import React from 'react';
import { FaBars } from 'react-icons/fa';
import { Button } from 'react-bootstrap';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import '../../componentsCss/Test.css';


const Hearder = () => {

  return (
    <nav id="entete" className="navbar  navbar-expand-sm">
        <div className="logo" style={{marginLeft: '0px', width: '40px', height: '70px'}}>
          <a href="/" className="navbar-brand mb-0 h1">
            <img src="https://yelodotred.s3-us-west-2.amazonaws.com/images/communities/wide_logos/8248/header/Inner_logo_%2818%29.png?1616191485" alt="3N Immo" srcSet="https://yelodotred.s3-us-west-2.amazonaws.com/images/communities/wide_logos/8248/header_highres/Inner_logo_%2818%29.png?1616191485 2x" />
          </a>
        </div>
        <button type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" className="navbar-toggler" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <svg xmlns="http://www.w3.org/2000/svg"  width={16} height={16} fill="currentColor" className="bi bi-list" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
          </svg>
          Menu
        </button>
        <div style={{marginLeft: '60px'}}>
          <form className="d-flex">
            <input type="text" className="form-control me-2" style={{width: '340px', height: '50px', borderTop: 'none', borderLeft: 'none', borderBottom: 'none'}} placeholder="Villa,Appartement,Bureau ou Terrain" /><input style={{width: '70px', border: 'none'}} type="text" className="form-control me-2" placeholder="ou?" /><a href><svg style={{marginTop: '25px', color: 'black'}} xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
              </svg></a>
          </form>
        </div>

        <div style={{marginTop: '17px'}} className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <a  style={{color: 'black'}} href="#" className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Menu
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a href="/" className="dropdown-item" style={{textAlign: 'justify'}}>Accueil</a></li>
                <li><hr className="dropdown-divider" /></li>
                <li><a href="/apropos" className="dropdown-item" style={{textAlign: 'justify'}}>a Propos</a></li>
                <li><hr className="dropdown-divider" /></li>
                <li><a href="/contact" className="dropdown-item" style={{textAlign: 'justify'}}>Contact</a></li>
              </ul>
            </li>
            <li className="nav-item" className="depot">
              <a href="seconnecter" className="nav-link" style={{height: '51px', color: '#d79c28'}}>
                +Déposer une annonce
              </a>
            </li>
            <li className="nav-item">
              <a href="#" className="nav-link">
            
              </a>
            </li>    
          </ul>
        </div>
      </nav>
  )
}
export default Hearder