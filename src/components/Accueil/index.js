import React, { Fragment } from 'react';
import { FaBars } from 'react-icons/fa';


const Accueil = () => {

    return(
        <Fragment>


            {/*Fin gestion du slide*/}
            
        <section id="hero__hero" className="hero__section--light">
        <div id="categorySlider" className="slider">
          <div className="slider__viewport">
            <ul className="slider__slides slide-list js-slide-list hero-carousel-slide-list" style={{left: '-400%'}}>
              <li className="slide-list__item js-slide-list-item">
                <div className="slide-list__content-area" style={{background: 'url(https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FftYEFawWyScYVrNw2Ng3F5io1lIkdI1_vue%20ar%20akwa.png)'}} />
              </li>
              <li className="slide-list__item js-slide-list-item" data-slide-index={0}>
                <div className="slide-list__content-area" style={{background: 'url(https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FQho6AdkpLg6tMT2x5cPTYn6n9gJMLkDouala-projet-infrastructure-Cameroun.jpg)'}} />
              </li>
              <li className="slide-list__item js-slide-list-item" data-slide-index={1}>
                <div className="slide-list__content-area" style={{background: 'url(https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FdOeHXd7MuLJC3icH3JkSeTfsuZur3LEchangeur-Joss-douala-autoroute-bonanjo.jpg)'}} />
              </li>
              <li className="slide-list__item js-slide-list-item" data-slide-index={2}>
                <div className="slide-list__content-area" style={{background: 'url(https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2F04JiMnjwgV9WBgg5wzmOaRTEgK18yBnautical.jpg)'}} />
              </li>
              <li className="slide-list__item js-slide-list-item" data-slide-index={3}>
                <div className="slide-list__content-area" style={{background: 'url(https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FftYEFawWyScYVrNw2Ng3F5io1lIkdI1_vue%20ar%20akwa.png)'}} />
              </li>
              <li className="slide-list__item js-slide-list-item">
                <div className="slide-list__content-area" style={{background: 'url(https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FQho6AdkpLg6tMT2x5cPTYn6n9gJMLkDouala-projet-infrastructure-Cameroun.jpg)'}} />
              </li>
            </ul>
          </div>
          <div className="slider__controls">
            <button type="button" data-direction="prev" className="js-arrow-button slider__button slider__button--prev">&lt;</button>
            <button type="button" data-direction="next " className="js-arrow-button slider__button slider__button--next">&gt;</button>
          </div>
        </div>
        <div className="hero__content">
          <div className="hero__content-container">
            <h1 className="hero__title">Le nouveau visage de l'immobilier!</h1>
            <h3 className="hero__subtitle">Agence <span style={{whiteSpace: 'nowrap'}}>immobilière digitale</span></h3>
            <div className="hero-search-container-flex">
              <div className="hero__search-bar-padding" style={{marginBottom: '25px'}}>
                <form  method="get" id="hero__hero__search-form" className="hero__search-bar--combined" action="/s?locale=%23%3CProc%3A0x007fc6d6d5fa30%40%2Fhome%2Fsharetribe%2Fsharetribe-prod%2Fapp%2Fview_utils%2Fpath_helpers.rb%3A15+%28lambda%29%3E">
                  <div className="hero__search-input-container--combined">
                    <button type="submit" className="hero__search-icon--combined">
                      <svg className="hero__search-icon-svg" viewBox="27 19 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                        <g id="icon_search" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd" transform="translate(38.000000, 30.000000) scale(-1, 1) translate(-38.000000, -30.000000) translate(28.000000, 20.000000)" strokeLinecap="round" strokeLinejoin="round">
                          <path d="M14.6666667,14.6666667 L19.3254458,19.3254458" id="line" stroke="#525961" strokeWidth={2} />
                          <circle id="Oval" stroke="#525961" strokeWidth={2} cx={8} cy={8} r={8} />
                        </g>
                      </svg>
                    </button>
                    <input type="search" name="q" className="hero__keyword-search-input--combined" placeholder="Villa, appartement, bureau ou terrain,Vehicule ?" />
                    <input type="search" name="lq" className="hero__location-search-input--combined" placeholder="Où ?" />
                    <div id="hero-loader" />
                    <div className="autocomplete-field" id="flightmap-hero-autocomplete-field-container" />
                  </div>
                  <button type="submit" className="hero__search-button hero__search-button--combined">Rechercher</button>
                  <input type="hidden" name="ls" />
                  <input type="hidden" name="lc" />
                  <input type="hidden" name="boundingbox" />
                  <input type="hidden" name="distance_max" />
                </form>
              </div>
              <div className="hero-custom-btns" />
            </div>
          </div>
        </div>
      </section>

      {/*Fin gestion du slide*/}



    {/*gestion du type de bien*/}

<section id="categories__categories_j5e0qh2r9" className="categories__section categories__section--blank" style={{}} data-src>
<div className="categories__content">
  <h1 className="categories__title">Types de biens</h1>
  <div className="categories__paragraph--markdown">
    <p>Catégories principales</p>
  </div>
  <div className="categories__categories">
    <div className="categories__category--3">
      <a href="/s?category=appartement&locale=%23%3CProc%3A0x007fc6d6c169d0%40%2Fhome%2Fsharetribe%2Fsharetribe-prod%2Fapp%2Fview_utils%2Fpath_helpers.rb%3A15+%28lambda%29%3E">
        <div className="categories__categories_j5e0qh2r9__categories__category-content categories__category-content lazyload" style={{display: 'flex', backgroundImage: 'url("https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2F7kvq0QFKaHERunD5dsvKtP9q5pP7Szcat%20appartement.jpg")'}}>
          <h3 className="categories__category-title">Appartement</h3>
        </div>
      </a>
    </div>
    <div className="categories__category--3">
      <a href="/s?category=villa&locale=%23%3CProc%3A0x007fc6d3d9f200%40%2Fhome%2Fsharetribe%2Fsharetribe-prod%2Fapp%2Fview_utils%2Fpath_helpers.rb%3A15+%28lambda%29%3E">
        <div className="categories__categories_j5e0qh2r9__categories__category-content categories__category-content lazyload" style={{display: 'flex', backgroundImage: 'url("https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FGgYC4rEVtcFwMON8nVmMRHW0bk0Rx2cat%20villa.jpg")'}}>
          <h3 className="categories__category-title">Villa</h3>
        </div>
      </a>
    </div>
    <div className="categories__category--3">
      <a href="/s?category=terrain&locale=%23%3CProc%3A0x007fc6d28a4288%40%2Fhome%2Fsharetribe%2Fsharetribe-prod%2Fapp%2Fview_utils%2Fpath_helpers.rb%3A15+%28lambda%29%3E">
        <div className="categories__categories_j5e0qh2r9__categories__category-content categories__category-content lazyload" style={{display: 'flex', backgroundImage: 'url("https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FDAkdJVoygRCgcj6WDKRmnOl8RkelMycat%20terrain.jpg")'}}>
          <h3 className="categories__category-title">Terrain</h3>
        </div>
      </a>
    </div>
  </div>
</div>
</section>
 {/*Fin gestion du type de bien*/}

 {/* gestion de notre agence*/}

<section id="info__single_info_without_background_and_cta" className="info__section--zebra lazyload" style={{}} data-src>
        <div className="info__content--single-column">
          <h1 className="info__title--single-column">Notre agence vous facilite l'achat, vente et location de propriétés en ligne</h1>
          <div className="info__paragraph--markdown" />
        </div>
      </section>
      {/*Fin gestion de notre agence*/}

            {/*gestion de se qui fait de nous les meilleurs*/}

      <section id="info__info_3lfhq16pj" className="info__section--background-image--dark lazyload" style={{display: 'flex', backgroundImage: 'url("https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FZV4Fim33x2AGdMXXcMiIscyUiIcx6Uhands-1063442_640.jpg")'}}>
        <div className="info__content--multi-column">
          <h1 className="info__title--multi-column">Ce qui fait de nous les meilleurs</h1>
          <div className="info__columns">
            <div className="info__column--three-columns">
              <div className="info__column-text-content">
                <div className="info__column-icon info__info_3lfhq16pj__info__column-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <g fill="none" stroke="none" strokeLinejoin="round" strokeMiterlimit={10}>
                      <path strokeLinecap="round" d="M8.5 8.5v5h9v-5M5.5 7L13 .5 20.5 7M16 .5h2.5V3" />
                      <path d="M.5 14.5h4v8h-4z" />
                      <path strokeLinecap="round" d="M4.5 21.062c10.5 3.5 7 3.5 19-2.5-1.062-1.062-1.902-1.313-3-1l-4.316 1.367" />
                      <path strokeLinecap="round" d="M4.5 15.5h3c2.353 0 4 2 4.5 2h3.5c1 0 1 2 0 2H10M11.5 9.5h3v4h-3z" />
                    </g>
                  </svg>
                </div>
                <h2 className="info__column-title">Choix de qualité</h2>
                <div className="info__column-paragraph--markdown">
                  <p>Nous préférons présélectionner des propriétés, vous proposez des biens de qualité plutôt qu'une grande quantité de biens médiocres</p>
                </div>
              </div>
            </div>
            <div className="info__column--three-columns">
              <div className="info__column-text-content">
                <div className="info__column-icon info__info_3lfhq16pj__info__column-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <g fill="none" stroke="none" strokeLinejoin="round" strokeMiterlimit={10}>
                      <path d="M9.355 2.066C9.636 1.137 10.509.5 11.5.5s1.864.637 2.145 1.566l.015.047.04-.03a2.28 2.28 0 0 1 2.677-.037 2.153 2.153 0 0 1 .793 2.498l-.019.046.051-.001c.991-.027 1.882.586 2.188 1.506a2.156 2.156 0 0 1-.862 2.475l-.043.028.043.028a2.156 2.156 0 0 1 .862 2.475c-.307.919-1.197 1.533-2.188 1.507l-.051-.002.018.047c.332.911.01 1.929-.793 2.497a2.28 2.28 0 0 1-2.677-.037l-.04-.03-.015.047c-.28.929-1.153 1.566-2.145 1.566s-1.863-.636-2.144-1.566l-.015-.047-.04.03a2.28 2.28 0 0 1-2.677.037 2.152 2.152 0 0 1-.793-2.497l.018-.047-.051.002c-.991.026-1.882-.588-2.188-1.507a2.157 2.157 0 0 1 .863-2.475l.043-.028-.043-.028a2.157 2.157 0 0 1-.863-2.475c.307-.92 1.197-1.533 2.188-1.506l.052.001-.018-.046a2.154 2.154 0 0 1 .793-2.498 2.279 2.279 0 0 1 2.677.037l.04.03.014-.047zM15.5 17v6.5l-4-4-4 4V17" />
                      <path d="M11.5 3.5l1.5 3h3L13.5 9l1 3.5-3-1.876-3 1.876 1-3.5L7 6.5h3z" />
                    </g>
                  </svg>
                </div>
                <h2 className="info__column-title">Les clients nous font confiance</h2>
                <div className="info__column-paragraph--markdown">
                  <p>Des milliers de visiteurs parcourent notre site tous les mois afin de dénicher la perle rare</p>
                </div>
              </div>
            </div>
            <div className="info__column--three-columns">
              <div className="info__column-text-content">
                <div className="info__column-icon info__info_3lfhq16pj__info__column-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <g fill="none" stroke="none" strokeLinejoin="round" strokeMiterlimit={10}>
                      <path strokeLinecap="round" d="M12 16.5c1.5 0 5-2 5-5 1.5 0 1.5-3 0-3L16.5 6c-4 0-4-1.5-4.5-2-.5.5-.5 2-4.5 2L7 8.5c-1.5 0-1.5 3 0 3 0 3 3.5 5 5 5z" />
                      <path strokeLinecap="round" d="M17.99 9.25l.51-3.75s.5-5-6.5-5-6.5 5-6.5 5l.512 3.75M9.457 15.514l-6.36 1.74C1.535 17.84.5 19.333.5 21v2.5h23V21c0-1.668-1.034-3.16-2.596-3.745l-6.34-1.755" />
                      <path strokeLinecap="round" d="M9 9.502c0-.553.447-.5 1-.5s1-.053 1 .5M13 9.502c0-.553.447-.5 1-.5s1-.053 1 .5" />
                      <path d="M12 19l-3 4.5h6zM10.5 16v1.5c0 .828.672 1.5 1.5 1.5s1.5-.672 1.5-1.5V16M17.5 20.5H21" />
                    </g>
                  </svg>
                </div>
                <h2 className="info__column-title">Une équipe d'expert</h2>
                <div className="info__column-paragraph--markdown">
                  <p>Notre équipe est formée d'agents immobiliers agrés et expérimentés qui sauront vous proposer les meilleures propriétés et vous guider afin de faire le meilleur choix</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
          {/*Fin gestion de se qui fait de nous les meilleurs*/}



            {/*gestion ville*/}

      <section className="locations__section" id="locations__locations_wdy5c3ja4">
        <div className="locations__content">
          <h1 className="locations__title">Villes</h1>
          <div className="locations__paragraph--markdown">
            <p>Destinations phares</p>
          </div>
          <div className="locations__locations">
            <div className="locations__location--3">
              <a href="www.3n-immo.com/dla">
                <div className="locations__locations_wdy5c3ja4__locations__location-content lazyload locations__location-content" style={{display: 'flex', backgroundImage: 'url("https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FVs5HtAS8qk2FgFamqW3wQ1b5icZGRTdla.jpg")'}}>
                  <h3 className="locations__location-title">Douala</h3>
                </div>
              </a>
            </div>
            <div className="locations__location--3">
              <a href="www.3n-immo.com/yde">
                <div className="locations__locations_wdy5c3ja4__locations__location-content lazyload locations__location-content" style={{display: 'flex', backgroundImage: 'url("https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FnLu9tTZNf4CbzCU9mGdv47nIxxD93Syde.jpg")'}}>
                  <h3 className="locations__location-title">Yaoundé</h3>
                </div>
              </a>
            </div>
            <div className="locations__location--3">
              <a href="www.3n-immo.com/kgi">
                <div className="locations__locations_wdy5c3ja4__locations__location-content lazyload locations__location-content" style={{display: 'flex', backgroundImage: 'url("https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2F3wtd0VeCzKi1quWaPPQ9ayT5FIg0Idkigali.jpg")'}}>
                  <h3 className="locations__location-title">Kigali</h3>
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>

       {/*Fin gestion ville*/}



         {/* gestion d'appartement meublé disponible*/}

      <section id="listing_carousel__listing_carousel_pfaijjzcw" className="listings__section listings__section--blank" style={{}} data-src>
        <div className="listings__content">
          <h1 className="listings__title">Appartements meublés disponibles</h1>
          <div className="listings__paragraph--markdown">
            <p>Sélection de propriétés premium</p>
          </div>
          <div className="listing_carousel__listings slick-initialized slick-slider">
            <div className="slick-list draggable">
              <div className="slick-track" style={{opacity: 1, width: '1140px', transform: 'translate3d(0px, 0px, 0px)'}}>
                <div className="listings__listing slick-slide slick-current slick-active" data-slick-index={0} aria-hidden="false" tabIndex={0} style={{width: '261px'}}>
                  <a href="/listings/27026" tabIndex={0}>
                    <div className="listings__listing-image lazyload" style={{display: 'block', backgroundImage: 'url("https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71399/big/2020122_1_193856.jpg?1616433402")'}} />
                    <h3 className="listings__listing-title">Appart meublé 3 chambres à louer</h3>
                  </a>
                  <div className="listings__listing-info">
                    <div className="listing_carousel__listing_carousel_pfaijjzcw__listings__price-container listings__price-container">
                      <span className="listings__price-amount">$30</span>
                      <span className="listings__price-unit">/ jour</span>
                    </div>
                    <a className="listing_carousel__listing_carousel_pfaijjzcw__listings__author-link listings__author-link" href="/samuel" tabIndex={0}>
                      <div className="listings__author-container">
                        <div className="listings__author-avatar-container">
                          <svg width={14} height={14} viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.445 5.212c0-.096.007-.19.012-.284.397.118.836.19 1.25.19 1.008 0 2.11-.46 2.785-1.108.602.796 1.31 1.174 2.202 1.174.193 0 .397-.022.61-.057l.002.085c0 2.336-1.538 4.236-3.43 4.236-1.892 0-3.43-1.9-3.43-4.236zM6.877.977c1.595 0 2.936 1.354 3.317 3.18-1.02.186-1.632-.148-2.244-1.223l-.44-.774-.412.79c-.324.624-1.464 1.192-2.39 1.192-.363 0-.772-.07-1.11-.186.436-1.724 1.74-2.98 3.28-2.98zm2.627 10.298v-1.89c1.074-.952 1.774-2.466 1.774-4.173C11.278 2.338 9.304 0 6.878 0c-2.43 0-4.404 2.338-4.404 5.212 0 1.66.66 3.14 1.686 4.094v1.974L0 13.046l.378.9 4.754-2.018V9.996c.535.275 1.125.43 1.745.43.585 0 1.144-.14 1.655-.387v1.894L13.637 14l.363-.906-4.496-1.82z" fill="#919599" fillRule="evenodd" />
                          </svg>
                        </div>
                        <span className="listings__author-name">SME Digital</span>
                      </div>
                    </a>
                  </div>
                </div>
                <div className="listings__listing slick-slide slick-active" data-slick-index={1} aria-hidden="false" tabIndex={0} style={{width: '261px'}}>
                  <a href="/listings/27247" tabIndex={0}>
                    <div className="listings__listing-image lazyload" style={{display: 'block', backgroundImage: 'url("https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/72038/big/v4.jpg?1617012539")'}} />
                    <h3 className="listings__listing-title">Villa à louer bastos Yaounde</h3>
                  </a>
                  <div className="listings__listing-info">
                    <div className="listing_carousel__listing_carousel_pfaijjzcw__listings__price-container listings__price-container">
                      <span className="listings__price-amount">$30 000 000</span>
                      <span className="listings__price-unit">/ mois</span>
                    </div>
                    <a className="listing_carousel__listing_carousel_pfaijjzcw__listings__author-link listings__author-link" href="/samuel" tabIndex={0}>
                      <div className="listings__author-container">
                        <div className="listings__author-avatar-container">
                          <svg width={14} height={14} viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.445 5.212c0-.096.007-.19.012-.284.397.118.836.19 1.25.19 1.008 0 2.11-.46 2.785-1.108.602.796 1.31 1.174 2.202 1.174.193 0 .397-.022.61-.057l.002.085c0 2.336-1.538 4.236-3.43 4.236-1.892 0-3.43-1.9-3.43-4.236zM6.877.977c1.595 0 2.936 1.354 3.317 3.18-1.02.186-1.632-.148-2.244-1.223l-.44-.774-.412.79c-.324.624-1.464 1.192-2.39 1.192-.363 0-.772-.07-1.11-.186.436-1.724 1.74-2.98 3.28-2.98zm2.627 10.298v-1.89c1.074-.952 1.774-2.466 1.774-4.173C11.278 2.338 9.304 0 6.878 0c-2.43 0-4.404 2.338-4.404 5.212 0 1.66.66 3.14 1.686 4.094v1.974L0 13.046l.378.9 4.754-2.018V9.996c.535.275 1.125.43 1.745.43.585 0 1.144-.14 1.655-.387v1.894L13.637 14l.363-.906-4.496-1.82z" fill="#919599" fillRule="evenodd" />
                          </svg>
                        </div>
                        <span className="listings__author-name">SME Digital</span>
                      </div>
                    </a>
                  </div>
                </div>
                <div className="listings__listing slick-slide slick-active" data-slick-index={2} aria-hidden="false" tabIndex={0} style={{width: '261px'}}>
                  <a href="/listings/27248" tabIndex={0}>
                    <div className="listings__listing-image lazyload" style={{display: 'block', backgroundImage: 'url("https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/72043/big/b4.jpg?1617012960")'}} />
                    <h3 className="listings__listing-title">Bureau à louer en centre ville Douala</h3>
                  </a>
                  <div className="listings__listing-info">
                    <div className="listing_carousel__listing_carousel_pfaijjzcw__listings__price-container listings__price-container">
                      <span className="listings__price-amount">$300 000</span>
                      <span className="listings__price-unit">/ mois</span>
                    </div>
                    <a className="listing_carousel__listing_carousel_pfaijjzcw__listings__author-link listings__author-link" href="/samuel" tabIndex={0}>
                      <div className="listings__author-container">
                        <div className="listings__author-avatar-container">
                          <svg width={14} height={14} viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.445 5.212c0-.096.007-.19.012-.284.397.118.836.19 1.25.19 1.008 0 2.11-.46 2.785-1.108.602.796 1.31 1.174 2.202 1.174.193 0 .397-.022.61-.057l.002.085c0 2.336-1.538 4.236-3.43 4.236-1.892 0-3.43-1.9-3.43-4.236zM6.877.977c1.595 0 2.936 1.354 3.317 3.18-1.02.186-1.632-.148-2.244-1.223l-.44-.774-.412.79c-.324.624-1.464 1.192-2.39 1.192-.363 0-.772-.07-1.11-.186.436-1.724 1.74-2.98 3.28-2.98zm2.627 10.298v-1.89c1.074-.952 1.774-2.466 1.774-4.173C11.278 2.338 9.304 0 6.878 0c-2.43 0-4.404 2.338-4.404 5.212 0 1.66.66 3.14 1.686 4.094v1.974L0 13.046l.378.9 4.754-2.018V9.996c.535.275 1.125.43 1.745.43.585 0 1.144-.14 1.655-.387v1.894L13.637 14l.363-.906-4.496-1.82z" fill="#919599" fillRule="evenodd" />
                          </svg>
                        </div>
                        <span className="listings__author-name">SME Digital</span>
                      </div>
                    </a>
                  </div>
                </div>
                <div className="listings__listing slick-slide slick-active" data-slick-index={3} aria-hidden="false" tabIndex={0} style={{width: '261px'}}>
                  <a href="/listings/27249" tabIndex={0}>
                    <div className="listings__listing-image lazyload" style={{display: 'block', backgroundImage: 'url("https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/72046/big/t1.jpg?1617013296")'}} />
                    <h3 className="listings__listing-title">Terrain à vendre à bonaberi</h3>
                  </a>
                  <div className="listings__listing-info">
                    <div className="listing_carousel__listing_carousel_pfaijjzcw__listings__price-container listings__price-container">
                      <span className="listings__price-amount">$3 000 000</span>
                      <span className="listings__price-unit">/ unité</span>
                    </div>
                    <a className="listing_carousel__listing_carousel_pfaijjzcw__listings__author-link listings__author-link" href="/samuel" tabIndex={0}>
                      <div className="listings__author-container">
                        <div className="listings__author-avatar-container">
                          <svg width={14} height={14} viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.445 5.212c0-.096.007-.19.012-.284.397.118.836.19 1.25.19 1.008 0 2.11-.46 2.785-1.108.602.796 1.31 1.174 2.202 1.174.193 0 .397-.022.61-.057l.002.085c0 2.336-1.538 4.236-3.43 4.236-1.892 0-3.43-1.9-3.43-4.236zM6.877.977c1.595 0 2.936 1.354 3.317 3.18-1.02.186-1.632-.148-2.244-1.223l-.44-.774-.412.79c-.324.624-1.464 1.192-2.39 1.192-.363 0-.772-.07-1.11-.186.436-1.724 1.74-2.98 3.28-2.98zm2.627 10.298v-1.89c1.074-.952 1.774-2.466 1.774-4.173C11.278 2.338 9.304 0 6.878 0c-2.43 0-4.404 2.338-4.404 5.212 0 1.66.66 3.14 1.686 4.094v1.974L0 13.046l.378.9 4.754-2.018V9.996c.535.275 1.125.43 1.745.43.585 0 1.144-.14 1.655-.387v1.894L13.637 14l.363-.906-4.496-1.82z" fill="#919599" fillRule="evenodd" />
                          </svg>
                        </div>
                        <span className="listings__author-name">SME Digital</span>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

       {/*Fin gestion d'appartement meublé disponible*/}



             {/*gestion contactez-nous*/}

      <section id="info__single_info_with_cta" className="info__section--background-image--dark lazyload" style={{display: 'flex', backgroundImage: 'url("https://rentals-temp.s3-us-west-2.amazonaws.com/landing%2Fimages%2FHbP5TRHxK2d6zIZCNZrYnzvzz10AMUkeys-1837155_1920.jpg")'}}>
        <div className="info__content--single-column">
          <h1 className="info__title--single-column">Gestion de patrimoine</h1>
          <div className="info__paragraph--markdown">
            <p>Vous désirez louer ou vendre votre bien ? Organisons un rendez-vous avec un de nos agents</p>
          </div>
          <a className="info__button info__single_info_with_cta__info__button" href="https://test.com">Contactez-nous!</a>
        </div>
      </section>

      {/*Fin gestion contactez-nous*/}

        {/*gestion icone chat*/}
      <FaBars id="iframe_fuguWidget" className="collapsed" />
        {/* Fin gestion icone chat*/}
</Fragment>


     

    )
}
export default Accueil