import React from 'react'
import '../../componentsCss/Seconnecter.css';


const SeConnecter = () =>{

    return(

        <article className="page-content" style={{}}>
        <div className="wrapper">
          <div className="login-form">
            <div className="login-centered-section">
              <div className="horizontal-nav">
                <div className="nav-tab">
                  <h1><a href="/fr/signup">S’inscrire</a></h1>
                </div>
                <div className="nav-tab login">
                  <h1><a href="/fr/login">Se connecter</a></h1>
                </div>
              </div>
              <div className="flex-fb-login-link" />
              <form action="/fr/sessions" className="flex-form-group" acceptCharset="UTF-8" method="post">
                <input name="utf8" type="hidden" defaultValue="✓" /><input type="hidden" name="authenticity_token" defaultValue="OWDve5DnqGBC2dhNfpnmj4gbQU83IQiJXeqc43uAcbzuDxbMxVJzZHBBfEjaSte47a07a0NOwOpeeRcGQNKotg==" />
                <label htmlFor="login">
                  Email ou nom d'utilisateur :
                </label>
                <input type="text" name="person[login]" id="main_person_login" className="text_field" autofocus="autofocus" />
                <label htmlFor="password">
                  Mot de passe :
                </label>
                <div className="pos-rel">
                  <input type="password" name="person[password]" id="main_person_password" className="text_field" />
                  <img id="show_password1_icon" onclick="showPassword('main_person_password','show_password1_icon','hide_password1_icon')" src="https://www.3n-immo.com/assets/show-password-b38c6f28f23fb47480f3cfbeae8f53d9d7a140f5edd7afc6e45b056b6a70dbd3.svg" alt="Show password" />
                  <img id="hide_password1_icon" className="hidepswrd" onclick="hidePassword('main_person_password','show_password1_icon','hide_password1_icon')" src="https://www.3n-immo.com/assets/hide-password-7376c8a554b2169a1c3f1c38babf8901bd52a022de9e49a6ea1b9786d0cd677f.svg" alt="Hide password" />
                </div>
                <div className="forgot-password-and-login">
                  <div className="reset-your-password">
                    Mot de passe oublié?
                    <a id="flex_password_forgotten_link" className="green_part_link" href="/fr/forgot_password">Réinitialiser mot de passe</a>
                  </div>
                  <button name="button" type="submit" className="login_button" id="main_log_in_button">Se connecter</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </article>

    )
}
export default SeConnecter