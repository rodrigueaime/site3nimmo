import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { FaBars } from 'react-icons/fa';
import Hearder from '../Header';
import Footer from '../Footer';
import Accueil from '../Accueil';
import Apropos from '../Apropos';
import CommentMarche from '../CommentMarche'
import PolitiqueConfidentialite from '../PolitiqueConfidentialite';
import CondictionUtilisation from '../CondictionUtilisation';
import Contact from '../Contact';
import Sinscrire from '../Sinscrire';
import SeConnecter from '../SeConnecter';
import '../../componentsCss/Accueil.css';
import ChatSms from '../ChatSms';
import DepotAnnonce from '../DepotAnnonce';
import TypeBien from '../TypeBien';
import TypeBienDisponible from '../TypebienDisponible';





function App() {
  return (
    <Router>
      
      <Hearder />
      <Switch>
        <Route exact path="/" component={Accueil} />
        <Route path="/apropos" component={Apropos} />
        <Route path="/commentcamarche" component={CommentMarche} />
        <Route path="/politiqueconfidentialite" component={PolitiqueConfidentialite} />
        <Route path="/condictionutilisation" component={CondictionUtilisation} />
        <Route path="/contact" component={Contact} />
        <Route path="/sinscrire" component={Sinscrire} />
        <Route path="/seconnecter" component={SeConnecter} />
        <Route path="/chat" component={ChatSms} />
        <Route path="/depotannonce" component={DepotAnnonce} />
        <Route path="/typebien" component={TypeBien} />
        <Route path="/typebiendisponible" component={TypeBienDisponible} />
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
