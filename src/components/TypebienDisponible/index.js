import React from 'react'
import BienDisponible from './BienDisponible'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const TypeBienDisponible = (props) =>{

    const { match } = props;

    return(


     <Router>
     <Switch> 
     <Route path={`${match.path}/biendisponible`} component={BienDisponible} />
     </Switch>
    </Router>

    )
}
export default TypeBienDisponible