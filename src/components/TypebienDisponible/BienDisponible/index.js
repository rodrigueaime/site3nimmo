import React from 'react'
import '../../../componentsCss/Biendisponible.css';

const BienDisponible = () =>{

    return(


        <article className="page-content" style={{}}>
        <div className="wrapper">
          <div className="row to_hide_image">
            <div className="col-8 listing-details-container">
              <div className="row">
                <div className="col-12">
                  <div className="flex-image-carousel" onclick="window.ST.closeCarouselOnClick(event)">
                    <div className="flex-carousel-1">
                      <div className="flex-carousel-2">
                        <button className="close_datepicker-btn" onclick="window.ST.hideFlexImageCarousel()" type="button">
                          <span className="close_datepicker-span">
                            <span className="close_datepicker-text">
                              Close
                            </span>
                            <i className="icon-remove" />
                          </span>
                        </button>
                        <div className="flex-carousel-3">
                          <div className="listing-image-carousel">
                            <div id="listing-image-frame">
                              <div className="listing-image-frame" style={{}}>
                                <div className="listing-image-frame-content" id="listing-image-link">
                                  <div className="too-narrow listing-image-vertical-centering">
                                    <img alt="Appart meublé 3 chambres à louer" className="listing-image too-narrow" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71399/big/2020122_1_193856.jpg?1616433402" />
                                  </div>
                                </div>
                              </div>
                              <div className="listing-image-frame" style={{display: 'none'}}>
                                <div className="listing-image-frame-content" id="listing-image-link">
                                  <div className="too-narrow listing-image-vertical-centering">
                                    <img alt="Appart meublé 3 chambres à louer" className="listing-image too-narrow" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71400/big/a12d2758196018a590a0baf_desktop-gallery-large.jpg?1616433399" />
                                  </div>
                                </div>
                              </div>
                              <div className="listing-image-frame" style={{display: 'none'}}>
                                <div className="listing-image-frame-content" id="listing-image-link">
                                  <div className="too-narrow listing-image-vertical-centering">
                                    <img alt="Appart meublé 3 chambres à louer" className="listing-image too-narrow" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71401/big/images.jpeg?1616433405" />
                                  </div>
                                </div>
                              </div>
                              <div className="listing-image-frame" style={{display: 'none'}}>
                                <div className="listing-image-frame-content" id="listing-image-link">
                                  <div className="too-narrow listing-image-vertical-centering">
                                    <img alt="Appart meublé 3 chambres à louer" className="listing-image too-narrow" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71402/big/a8b669f2435df6619d9824e_mobile-gallery-large.jpg?1616433407" />
                                  </div>
                                </div>
                              </div>
                            </div>
                            <a className="listing-image-navi listing-image-navi-left" id="listing-image-navi-left" href="/fr/listings/27026?image=71402&username=samuel">
                              <div className="listing-image-navi-arrow-container">
                                <div className="listing-image-arrow-icon-container left">
                                  <i className="icon-caret-left navigate-icon-fix listing-image-navi-arrow" />
                                </div>
                              </div>
                            </a>
                            <a className="listing-image-navi listing-image-navi-right" id="listing-image-navi-right" href="/fr/listings/27026?image=71400&username=samuel">
                              <div className="listing-image-navi-arrow-container">
                                <div className="listing-image-arrow-icon-container right">
                                  <i className="icon-caret-right navigate-icon-fix listing-image-navi-arrow" />
                                </div>
                              </div>
                            </a>
                          </div>
                          <div className="listing-image-thumbnail-stripe" id="thumbnail-stripe">
                            <div style={{position: 'absolute', left: '-2px', right: '-2px', width: '260px'}}>
                              <div className="listing-image-thumbnail-container selected">
                                <img className="listing-image-thumbnail" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71399/thumb/2020122_1_193856.jpg?1616433402" />
                                <div className="fade" />
                              </div>
                              <div className="listing-image-thumbnail-container">
                                <img className="listing-image-thumbnail" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71400/thumb/a12d2758196018a590a0baf_desktop-gallery-large.jpg?1616433399" />
                                <div className="fade" />
                              </div>
                              <div className="listing-image-thumbnail-container">
                                <img className="listing-image-thumbnail" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71401/thumb/images.jpeg?1616433405" />
                                <div className="fade" />
                              </div>
                              <div className="listing-image-thumbnail-container">
                                <img className="listing-image-thumbnail" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71402/thumb/a8b669f2435df6619d9824e_mobile-gallery-large.jpg?1616433407" />
                                <div className="fade" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="two-photos" id="two-photos">
                    <div className="first-image for-hover-effect" data-index={0} onclick="window.ST.showFlexImageCarousel(0)" onmouseout="window.ST.removeBlurEffect()" onmouseover="window.ST.blurRestImages(0)">
                      <img alt="Appart meublé 3 chambres à louer" className="listing-image ${aspectRatioClass}" id="listing_image_71399" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71399/original/2020122_1_193856.jpg?1616433402" />
                    </div>
                    <div className="second-img for-hover-effect" data-index={1} onclick="window.ST.showFlexImageCarousel(1)" onmouseout="window.ST.removeBlurEffect()" onmouseover="window.ST.blurRestImages(1)">
                      <img alt="Appart meublé 3 chambres à louer" className="listing-image ${aspectRatioClass}" id="listing_image_71400" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71400/original/a12d2758196018a590a0baf_desktop-gallery-large.jpg?1616433399" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mark-fav-listing">
                <div className="mark-favourite">
                  <label htmlFor="add_to_fav_27026" id="fav_icon_27026">
                    <div className="red-heart">
                      <img className="favourite-icon in_favorite" id="added_to_favourites_27026" src="https://www.3n-immo.com/assets/favourite-cc8b7c471ecf3427d3fb6046b5609400f6017b6006d42af08406f3270b393859.svg" alt="Favourite" />
                    </div>
                    <div className="grey-heart">
                      <img className="favourite-icon in_favorite" id="not_in_favourites_27026" src="https://www.3n-immo.com/assets/not_favourite-081dc589548042799e5849e7da3c06031ef3635ffc4439439bc6cb93bf8ac464.svg" alt="Not favourite" />
                    </div>
                  </label>
                  <input defaultChecked="checked" id="add_to_fav_27026" name="add_to_fav_27026" onchange="ST.fav_listings.updateFavouriteStatus(27026, 'HG7MNsSmX92YTDQkPhx2fQ')" style={{display: 'none'}} type="checkbox" />
                </div>
              </div>
              <button className="view-all-photos" onclick="window.ST.showFlexImageCarousel(0)">
                <span>
                  Voir les photos
                </span>
              </button>
            </div>
          </div>
          <div className="flex-listing-details-and-actions row">
            <div className="row to_hide_image">
              <div className="col-12">
                <div className="thumbnail-author-avatar">
                  <a href="/fr/samuel"><img className="listing-author-avatar-image" src="https://www.3n-immo.com/assets/profile_image/small/missing-cced9224e83e132981635bd0461eee0c9925a16869366487b3d6c77f7e8799e8.png" alt="Missing" /></a>
                </div>
              </div>
            </div>
            <div className="listing-all-details col-8" id="listing-all-details">
              <aside className="col-4">
                <div className="row">
                  <div className="col-12">
                    <div className="amount-and-listing-title">
                      <div className="listing-price">
                        <span className="listing-price-amount">
                          <span className="listing-price-amount" title="30,00" />
                          $ 30
                        </span>
                        <span className="listing-price-quantity">
                          par jour
                        </span>
                      </div>
                      <div className="listing-title-category-flex">
                        <h1 className="listing-title-heading" style={{wordBreak: 'break-word'}}>
                          <span className="listing-title-text">
                            Appart meublé 3 chambres à louer
                          </span>
                        </h1>
                        <div className="listing-category-host">
                          <span className="category-text" style={{display: 'flex'}}>
                            Appartement
                            <span className="profile-contact-seperator">
                              •
                            </span>
                          </span>
                          <span className="hosted-by" style={{display: 'flex'}}>
                            Publié par
                            <span className="hostname-text">
                              SME Digital
                            </span>
                          </span>
                          <span className="listing-author-contact-flex">
                            <span className="profile-contact-seperator">
                              •
                            </span>
                            <a className="listing-author-contact-link" href="/fr/listings/27026/contact" id="listing-contact">
                              <div className="content">
                                Contacter le vendeur
                              </div>
                            </a>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="listing-social">
                      <div className="listing-fb-like-button">
                        <div className="fb-like fb_iframe_widget" data-send="true" data-layout="button_count" data-width={200} data-show-faces="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&container_width=0&href=https%3A%2F%2Fwww.3n-immo.com%2Flistings%2F27026&layout=button_count&locale=fr_FR&sdk=joey&send=true&show_faces=false&width=200">
                          <span style={{verticalAlign: 'bottom', width: '180px', height: '28px'}}>
                            <iframe name="f3146529c0dc7f4" width="200px" height="1000px" data-testid="fb:like Facebook Social Plugin" title="fb:like Facebook Social Plugin" frameBorder={0} allowTransparency="true" allowFullScreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v3.2/plugins/like.php?app_id=&channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df191db69da47ed%26domain%3Dwww.3n-immo.com%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fwww.3n-immo.com%252Ff38da53f8ed3c8%26relation%3Dparent.parent&container_width=0&href=https%3A%2F%2Fwww.3n-immo.com%2Flistings%2F27026&layout=button_count&locale=fr_FR&sdk=joey&send=true&show_faces=false&width=200" style={{border: 'none', visibility: 'visible', width: '180px', height: '28px'}} className />
                          </span>
                        </div>
                      </div>
                      <div className="listing-tweet-button">
                        <iframe id="twitter-widget-0" scrolling="no" frameBorder={0} allowTransparency="true" allowFullScreen="true" className="twitter-share-button twitter-share-button-rendered twitter-tweet-button" style={{position: 'static', visibility: 'visible', width: '83px', height: '20px'}} title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.a53eecb4584348a2ad32ec2ae21f6eae.fr.html#dnt=false&id=twitter-widget-0&lang=fr&original_referer=https%3A%2F%2Fwww.3n-immo.com%2Flistings%2F27026&size=m&text=Appart%20meubl%C3%A9%203%20chambres%20%C3%A0%20louer&time=1637120985548&type=share&url=https%3A%2F%2Fwww.3n-immo.com%2Flistings%2F27026" />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="listing-description-flex listing-description-content">
                      <h2 className="flex-headings">
                        <span className="about-listing-text">
                          Description
                        </span>
                      </h2>
                      <p>3chb 3sdb</p>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 custom_field_value_flex">
                    <b>Equipements:</b>
                    TV, Wifi
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 custom_field_value_flex">
                    <b>Superficie (m2):</b>
                    55
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="product-flightmap leaflet-container leaflet-touch leaflet-fade-anim leaflet-grab leaflet-touch-drag leaflet-touch-zoom" id="map" tabIndex={0} style={{position: 'relative'}}>
                      <div className="leaflet-pane leaflet-map-pane" style={{transform: 'translate3d(0px, 0px, 0px)'}}>
                        <div className="leaflet-pane leaflet-tile-pane">
                          <div className="leaflet-gl-layer mapboxgl-map" style={{width: '782.4px', height: '420px', transform: 'translate3d(-65.2px, -35px, 0px)'}}>
                            <div className="mapboxgl-canary" style={{visibility: 'hidden'}} />
                            <div className="mapboxgl-canvas-container">
                              <canvas className="mapboxgl-canvas leaflet-image-layer leaflet-zoom-animated" tabIndex={0} aria-label="Map" width={703} height={377} style={{position: 'absolute', width: '782px', height: '420px', transform: 'translate3d(0px, 0px, 0px) scale(1)'}} />
                            </div>
                            <div className="mapboxgl-control-container">
                              <div className="mapboxgl-ctrl-top-left" />
                              <div className="mapboxgl-ctrl-top-right" />
                              <div className="mapboxgl-ctrl-bottom-left">
                                <div className="mapboxgl-ctrl" style={{display: 'none'}}><a className="mapboxgl-ctrl-logo" target="_blank" rel="noopener nofollow" href="https://www.mapbox.com/" aria-label="Mapbox logo" /></div>
                              </div>
                              <div className="mapboxgl-ctrl-bottom-right" />
                            </div>
                          </div>
                          <div className="leaflet-layer" style={{zIndex: 1, opacity: 1}}>
                            <div className="leaflet-tile-container leaflet-zoom-animated" style={{zIndex: 18, transform: 'translate3d(0px, 0px, 0px) scale(1)'}}>
                              <img alt="" role="presentation" src="http://a.tile.osm.org/8/134/124.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(96px, -115px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://b.tile.osm.org/8/135/124.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(352px, -115px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://b.tile.osm.org/8/134/125.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(96px, 141px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://c.tile.osm.org/8/135/125.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(352px, 141px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://c.tile.osm.org/8/133/124.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(-160px, -115px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://c.tile.osm.org/8/136/124.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(608px, -115px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://a.tile.osm.org/8/133/125.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(-160px, 141px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://a.tile.osm.org/8/136/125.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(608px, 141px, 0px)', opacity: 1}} />
                            </div>
                          </div>
                        </div>
                        <div className="leaflet-pane leaflet-shadow-pane">
                          <img src="https://unpkg.com/leaflet@1.0.3/dist/images/marker-shadow.png" className="leaflet-marker-shadow leaflet-zoom-animated" alt="" style={{marginLeft: '-12px', marginTop: '-41px', width: '41px', height: '41px', transform: 'translate3d(326px, 175px, 0px)'}} />
                        </div>
                        <div className="leaflet-pane leaflet-overlay-pane" />
                        <div className="leaflet-pane leaflet-marker-pane">
                          <img src="https://unpkg.com/leaflet@1.0.3/dist/images/marker-icon.png" className="leaflet-marker-icon leaflet-zoom-animated leaflet-interactive" tabIndex={0} style={{marginLeft: '-12px', marginTop: '-41px', width: '25px', height: '41px', transform: 'translate3d(326px, 175px, 0px)', zIndex: 175}} />
                        </div>
                        <div className="leaflet-pane leaflet-tooltip-pane" />
                        <div className="leaflet-pane leaflet-popup-pane" />
                        <div className="leaflet-proxy leaflet-zoom-animated" style={{transform: 'translate3d(34533.9px, 32034.4px, 0px) scale(128)'}} />
                      </div>
                      <div className="leaflet-control-container">
                        <div className="leaflet-top leaflet-left">
                          <div className="leaflet-control-zoom leaflet-bar leaflet-control">
                            <a className="leaflet-control-zoom-in" href="#" title="Zoom in" role="button" aria-label="Zoom in">+</a>
                            <a className="leaflet-control-zoom-out" href="#" title="Zoom out" role="button" aria-label="Zoom out">-</a>
                          </div>
                        </div>
                        <div className="leaflet-top leaflet-right" />
                        <div className="leaflet-bottom leaflet-left" />
                        <div className="leaflet-bottom leaflet-right">
                          <div className="leaflet-control-attribution leaflet-control">
                            <a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a> | © <a href="http://osm.org/copyright">OpenStreetMap</a> contributors
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="origin" />
                    <input defaultValue="Kado, Rue 1.235, Bonadoumbe, Douala I, Communauté urbaine de Douala, Wouri, Littoral, 4190, Cameroon" type="hidden" name="origin_loc[address]" id="origin_loc_address" />
                    <input defaultValue="undefined" type="hidden" name="origin_loc[google_address]" id="origin_loc_google_address" />
                    <input defaultValue="4.02634" type="hidden" name="origin_loc[latitude]" id="origin_loc_latitude" />
                    <input defaultValue="9.70042" type="hidden" name="origin_loc[longitude]" id="origin_loc_longitude" />
                  </div>
                </div>
                <div className="row-with-divider">
                  <div className="col-12">
                    <h2 className="flex-headings">
                      <span className="your-host">
                        Vendeur
                      </span>
                    </h2>
                    <div className="listing-author">
                      <div className="listing-author-avatar">
                        <a href="/fr/samuel">
                          <img className="listing-author-avatar-image" src="https://www.3n-immo.com/assets/profile_image/small/missing-cced9224e83e132981635bd0461eee0c9925a16869366487b3d6c77f7e8799e8.png" alt="Missing" />
                        </a>
                      </div>
                      <div className="listing-author-details">
                        <span className="listing-author-name-text">
                          <a id="listing-author-link" className="listing-author-name-link" title="SME Digital" href="/fr/samuel">SME Digital</a>
                        </span>
                        <div className="profile-about-me" />
                        <div className="listing-author-profile-contact">
                          <a href="/fr/samuel">Voir le profil</a>
                          <div className="listing-author-contact">
                            <span className="profile-contact-seperator">
                              •
                            </span>
                            <a className="listing-author-contact-link" href="/fr/listings/27026/contact" id="listing-contact">
                              <div className="content">
                                Contacter le vendeur
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="reviews-heading" style={{marginBottom: '10px', textDecoration: 'underline', display: 'inline-block'}}>
                  Notes et commentaires
                </div>
                <div className="review-count" style={{display: 'inline-block'}}>
                  ( 0 )
                </div>
                <div className="profile-testimonials-list" />
                <div className="row load-more-link" />
                <div className="modal ratings-modal">
                  <div className="modal-overlay ratings-modal-toggle" />
                  <div className="modal-wrapper mobile-modal modal-transition" style={{textAlign: 'center'}}>
                    <div className="modal-header">
                      <h2 className="modal-heading">
                        Notes et commentaires
                      </h2>
                    </div>
                    <div className="modal-body" style={{textAlign: 'left'}}>
                      <div className="modal-content" style={{height: '600px', overflow: 'auto'}} />
                    </div>
                  </div>
                </div>
              </aside>
            </div>
            <div className="book-or-listing-actions col-4" id="book-or-listing-actions">
              <div className="row-with-divider">
                <div className="col-12">
                  <div className="listing-message-links-flex" id="listing-message-links">
                    <div className="datepicker-popup-content" id="datepicker-popup-content">
                      <button className="close_datepicker-btn" onclick="window.ST.hidePopupForDatePicker()" type="button">
                        <span className="close_datepicker-span">
                          <span className="close_datepicker-text">
                            Close
                          </span>
                          <i className="icon-remove" />
                        </span>
                      </button>
                      <h1 className="booking-listing">
                        <span>
                          Appart meublé 3 chambres à louer
                        </span>
                      </h1>
                      <form id="booking-dates" action="/fr/transactions/new?listing_id=27026" acceptCharset="UTF-8" method="get" noValidate="novalidate">
                        <input name="utf8" type="hidden" defaultValue="✓" />
                        <div className="max-booking-days">
                          Nombre maximum de jours de réservation: 30
                        </div>
                        <div className="input-daterange input-group clearfix" data-dateformat="dd/mm/yyyy" data-locale="fr" id="datepicker">
                          <div className="datepicker-start-wrapper">
                            <label htmlFor="Du">Du</label>
                            <input autoComplete="off" className="input-sm form-control required" data-output="booking-start-output" id="start-on" name="start_on_datepicker" placeholder="mm/dd/yyyy" type="text" />
                            <input id="booking-start-output" name="start_on" type="hidden" />
                          </div>
                          <div className="datepicker-end-wrapper">
                            <label htmlFor="Au">Au</label>
                            <input autoComplete="off" className="input-sm form-control required" data-output="booking-end-output" id="end-on" name="end_on_datepicker" placeholder="mm/dd/yyyy" type="text" />
                            <input id="booking-end-output" name="end_on" type="hidden" />
                          </div>
                        </div>
                        <input type="hidden" name="n_day_night" id="n_day_night" defaultValue={1} />
                        <div className="listing-price-invoice-container">
                          <input id="total-with-addons" type="hidden" defaultValue={0} />
                          <input id="total-addons" type="hidden" defaultValue={0} />
                          <div className="listing-price-invoice">
                            <span className="listing-price-total">
                              Total
                            </span>
                            <span className="listing-price-amount-invoice listing-price-amount" amount={30}>
                              $ 30
                            </span>
                          </div>
                        </div>
                        <input type="hidden" name="listing_id" id="listing_id" defaultValue={27026} />
                        <input type="hidden" name="unit_type" id="unit_type" defaultValue="day" />
                        <div className="origin" />
                        <input defaultValue="Kado, Rue 1.235, Bonadoumbe, Douala I, Communauté urbaine de Douala, Wouri, Littoral, 4190, Cameroon" type="hidden" name="origin_loc[address]" id="origin_loc_address" />
                        <input defaultValue="undefined" type="hidden" name="origin_loc[google_address]" id="origin_loc_google_address" />
                        <input defaultValue="4.02634" type="hidden" name="origin_loc[latitude]" id="origin_loc_latitude" />
                        <input defaultValue="9.70042" type="hidden" name="origin_loc[longitude]" id="origin_loc_longitude" />
                        <button className="enabled-book-button">
                          <div className="content">
                            Location meublée
                          </div>
                        </button>
                      </form>
                      <div className="row">
                        <div className="col-12" />
                      </div>
                      <div className="listing-view-admin-links modern-theme">
                        <div className="listing-view-admin-link">
                          <a className="icon-with-text-container" href="/fr/listings/27026-appart-meuble-3-chambres-a-louer/edit">
                            <i className="icon-edit icon-part" />
                            <div className="text-part">Editer l'annonce</div>
                          </a>
                        </div>
                        <div className="listing-view-admin-link">
                          <a className="icon-with-text-container" data-method="put" data-remote="true" href="/fr/eddy/listings/27026-appart-meuble-3-chambres-a-louer/close">
                            <i className="icon-lock icon-part" />
                            <div className="text-part">Clore l'annonce</div>
                          </a>
                        </div>
                        <div className="lightbox" id="delete_listing_popup">
                          <div className="lightbox-content">
                            <a className="close lightbox-x" href="#" id="close_x">
                              <i className="icon-remove" />
                            </a>
                            <form action="/fr/listings/27026-appart-meuble-3-chambres-a-louer/listing_delete" method="get" name="delete-form" onsubmit="ST.deletelisting()">
                              <div className="row">
                                <div className="col-12 delete-text">
                                  <div>Il n’est pas possible de récupérer une annonce supprimée. Êtes-vous sûr de vouloir continuer ?</div>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-12 delete-buttons">
                                  <div className="inline-button-container">
                                    <button className="button save-btn" id="del-save-btn" type="submit">
                                      <i className="icon-trash icon-part" />
                                      <div className="text-part">Supprimer l’annonce</div>
                                    </button>
                                  </div>
                                  <div className="inline-button-container">
                                    <a className="cancel-button del-cancel-btn" href="#" id="close_x1">
                                      <div className="content">
                                        Cancel
                                      </div>
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <div className="listing-view-admin-link">
                          <span className="delete-popup-btn icon-with-text-container" onclick="ST['deleteListingPopupInit']()">
                            <i className="icon-trash icon-part" />
                            <div className="text-part">Supprimer l’annonce</div>
                          </span>
                        </div>
                        <div className="listing-view-admin-link">
                          <a data-method="put" href="/fr/eddy/listings/27026-appart-meuble-3-chambres-a-louer/move_to_top">
                            <div className="icon-with-text-container">
                              <i className="icon-star icon-part" />
                              <div className="text-part">Remonter en tête de liste</div>
                            </div>
                          </a>
                        </div>
                        <div className="listing-view-admin-link">
                          <a data-method="put" data-remote="true" id="add-to-updates-email" href="/fr/eddy/listings/27026-appart-meuble-3-chambres-a-louer/show_in_updates_email">
                            <div className="icon-with-text-container">
                              <i className="icon-envelope icon-part" />
                              <div className="text-part" data-action-error="Impossible de se connecter au serveur. Veuillez essayer de nouveau après avoir rafraîchi la page." data-action-loading="Chargement..." data-action-success="Cette annonce sera diffusée dans le prochain email d'actualité, automatiquement envoyé aux utilisateurs" id="add-to-updates-email-text">
                                Afficher dans la prochaine newsletter
                              </div>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="report-listing-container" style={{marginTop: '100px'}}>
                <button className="report-listing">
                  <div>
                    <span className="warn warning" />
                  </div>
                  <div>
                    <span style={{height: '30px'}} />
                  </div>
                  <span>
                    Signaler une annonce
                  </span>
                </button>
              </div>
              <div className="booking-panel">
                <div className="booking-panel-link">
                  <div className="price-of-listing">
                    <span className="listing-price-amount-flex">
                      $ 30
                    </span>
                    <span className="listing-price-quantity-flex">
                      par jour
                    </span>
                  </div>
                  <button className="perform-listing-actions-btn" onclick="window.ST.showPopupForDatePicker()" type="button">
                    <span id="request-to-book">
                      Location meublée
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="modal report-listing-modal">
            <div className="modal-overlay modal-toggle" />
            <div className="modal-wrapper modal-transition" style={{textAlign: 'center'}}>
              <div className="modal-header">
                <h2 className="modal-heading">
                  Signaler cette annonce
                </h2>
              </div>
              <div className="modal-body">
                <div className="modal-content">
                  <p>
                    Veuillez sélectionner vos raisons pour signaler cette annonce
                  </p>
                  <form className="report-listing-form">
                    <div className="report-reasons-row">
                      <div className="reason-1 report-reason">
                        <input id="vehicle1" name="reason1" type="checkbox" defaultValue="Bad Quality" />
                        <label htmlFor="reason1">
                          Mauvaise qualité
                        </label>
                      </div>
                      <div className="reason-2 report-reason">
                        <input id="vehicle2" name="reason2" type="checkbox" defaultValue="Fake Product" />
                        <label htmlFor="reason2">
                          Faux produit
                        </label>
                      </div>
                      <div className="reason-3 report-reason">
                        <input id="vehicle3" name="reason3" type="checkbox" defaultValue="Not Delivered" />
                        <label htmlFor="reason3">
                          Non livrés
                        </label>
                      </div>
                    </div>
                    <div className="report-reasons-row">
                      <div className="reason-4 report-reason">
                        <input id="vehicle1" name="reason4" type="checkbox" defaultValue="Bad Service" />
                        <label htmlFor="reason4">
                          Mauvais service
                        </label>
                      </div>
                      <div className="reason-2 report-reason">
                        <input id="vehicle5" name="reason5" type="checkbox" defaultValue="Not Mentioned Above" />
                        <label htmlFor="reason2">
                          Non mentionné ci-dessus
                        </label>
                      </div>
                    </div>
                    <textarea className="resport-desc" name="descreption" placeholder="Add description" style={{resize: 'none'}} defaultValue={""} />
                  </form>
                  <div className="modal-btns">
                    <button className="submit-report" onclick="window.ST.submitReportListingContent('fr','27026')">
                      Soumettre
                    </button>
                    <button className="modal-toggle">
                      proche
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>



    )
}
export default BienDisponible