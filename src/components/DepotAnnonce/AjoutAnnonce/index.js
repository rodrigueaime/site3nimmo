import React from 'react'
import '../../../componentsCss/Ajoutannonce.css'

const AjoutAnnonce = () =>{

    return(

        <article className="page-content" style={{}}>
        <div className="wrapper">
          <div className="new-listing-form" id="new_listing_form">
            <div className="centered-section main-category-section" style={{display: 'none'}}>
              <div id="selected-groups" />
              <div className="selected-group" name="category">
                <a className="select selected hidden" data-id={22334}>
                  <div className="link-text">Catégorie : Default category</div>
                </a>
                <a className="select selected" data-id={22422}>
                  <div className="link-text">Catégorie : Appartement</div>
                </a>
                <a className="select selected hidden" data-id={22423}>
                  <div className="link-text">Catégorie : Villa</div>
                </a>
                <a className="select selected hidden" data-id={22528}>
                  <div className="link-text">Catégorie : Bureau</div>
                </a>
                <a className="select selected hidden" data-id={22529}>
                  <div className="link-text">Catégorie : Terrain</div>
                </a>
                <a className="select selected hidden" data-id={24558}>
                  <div className="link-text">Catégorie : Vehicule</div>
                </a>
                <a className="select selected hidden" data-id={24680}>
                  <div className="link-text">Catégorie : Restaurant</div>
                </a>
              </div>
              <div className="selected-group" name="listing_shape">
                <a className="select selected hidden" data-id={19624}>
                  <div className="link-text">Type d'annonce : Location longue durée (non meublée)</div>
                </a>
                <a className="select selected" data-id={19625}>
                  <div className="link-text">Type d'annonce : Location court/moyenne durée (meublée)</div>
                </a>
                <a className="select selected hidden" data-id={19721}>
                  <div className="link-text">Type d'annonce : Vente</div>
                </a>
                <a className="select selected hidden" data-id={21740}>
                  <div className="link-text">Type d'annonce : Information</div>
                </a>
                <a className="select selected hidden" data-id={21741}>
                  <div className="link-text">Type d'annonce : Vente 2</div>
                </a>
              </div>
              <h2 className="listing-form-title" id="foo" />
              <div id="option-groups" />
              <div className="option-group" name="category">
                <a className="select option hidden" data-id={22334}>
                  <div className="link-text">Default category</div>
                </a>
                <a className="select option hidden" data-id={22422}>
                  <div className="link-text">Appartement</div>
                </a>
                <a className="select option hidden" data-id={22423}>
                  <div className="link-text">Villa</div>
                </a>
                <a className="select option hidden" data-id={22528}>
                  <div className="link-text">Bureau</div>
                </a>
                <a className="select option hidden" data-id={22529}>
                  <div className="link-text">Terrain</div>
                </a>
                <a className="select option hidden" data-id={24558}>
                  <div className="link-text">Vehicule</div>
                </a>
                <a className="select option hidden" data-id={24680}>
                  <div className="link-text">Restaurant</div>
                </a>
              </div>
              <div className="option-group" name="listing_shape">
                <a className="select option hidden" data-id={19624}>
                  <div className="link-text">Location longue durée (non meublée)</div>
                </a>
                <a className="select option hidden" data-id={19625}>
                  <div className="link-text">Location court/moyenne durée (meublée)</div>
                </a>
                <a className="select option hidden" data-id={19721}>
                  <div className="link-text">Vente</div>
                </a>
                <a className="select option hidden" data-id={21740}>
                  <div className="link-text">Information</div>
                </a>
                <a className="select option hidden" data-id={21741}>
                  <div className="link-text">Vente 2</div>
                </a>
              </div>
            </div>
            <div className="js-form-fields">
              <div className="listing-form-wrapper">
                <span className="mobile-back-btn" href="#" onclick="ST['backToCategorySelection']()">Retour à la sélection de catégorie</span>
                <div className="form-sidenav">
                  <span className="back-btn" href="#" onclick="ST['backToCategorySelection']()">Retour à la sélection de catégorie</span>
                  <div className="links-group">
                    <a href="#" className="form-sidenav-link selected-link" id="form-link-0" onclick="ST[&quot;selectForm&quot;](event,0,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                      La description
                    </a>
                    <a href="#" className="form-sidenav-link disabled" id="form-link-1" onclick="ST[&quot;selectForm&quot;](event,1,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                      Tarification
                    </a>
                    <a href="#" className="form-sidenav-link disabled" id="form-link-2" onclick="ST[&quot;selectForm&quot;](event,2,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                      Emplacement
                    </a>
                    <a href="#" className="form-sidenav-link disabled" id="form-link-3" onclick="ST[&quot;selectForm&quot;](event,3,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                      Information additionnelle
                    </a>
                    <a href="#" className="form-sidenav-link disabled" id="form-link-4" onclick="ST[&quot;selectForm&quot;](event,4,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                      Photos
                    </a>
                  </div>
                </div>
                <div className="listing-form-panel">
                  <form className="new_listing js-listing-form-ready" id="new_listing" encType="multipart/form-data" action="/fr/listings" acceptCharset="UTF-8" method="post" noValidate="novalidate">
                    <input name="utf8" type="hidden" defaultValue="✓" /><input type="hidden" name="authenticity_token" defaultValue="zfkRrfwwIMTs5AKHNlz/WmqjIeek34yL98fYXuOcd0e1SRATbA2Il7eFWQWy7BvJ2beJ/wfiUhnOLQUjLVrSrg==" />
                    <div className="form-section" id="form-panel-0">
                      <div className="section-heading">Ajoutez votre annonce</div>
                      <div className="form-group">
                        <label className="label-input" htmlFor="listing_title">Titre de l'annonce*</label>
                        <input className="title_text_field" maxLength={65} size={65} type="text" name="listing[title]" id="listing_title" />
                      </div>
                      <div className="form-group">
                        <label className="label-input" htmlFor="listing_description">Description détaillée</label>
                        <div className="info-text-container">
                          <div className="info-text-icon">
                            <i className="icon-info-sign" />
                          </div>
                          <div className="info-text-content">
                            <p>
                              Vous pouvez <a id="markdown-popup-link" href="#">formater votre description à l'aide de Markdown</a>. Si votre description contient des liens YouTube, les vidéos seront affichées sous la
                              description.
                            </p>
                          </div>
                        </div>
                        <textarea maxLength={5000} className="listing_description_textarea" name="listing[description]" id="listing_description" style={{overflow: 'hidden visible', overflowWrap: 'break-word', resize: 'horizontal'}} defaultValue={""} />
                      </div>
                      <div className="form-group">
                        <button className="button" onclick="ST['moveToNextStep'](event,0,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                          Suivant
                        </button>
                      </div>
                    </div>
                    <input type="hidden" name="listing_ordered_images" id="listing_ordered_images" />
                    <div className="form-section hidden" id="form-panel-1">
                      <input type="hidden" name="transaction_process" id="transaction_process" defaultValue="preauthorize" />
                      <div className="section-heading">
                        Combien ça coûte
                      </div>
                      <div className="price-container form-group">
                        <label htmlFor="listing_price">Prix</label>
                        <input className="price-field" maxLength={10} placeholder={0} defaultValue={0} size={10} type="text" name="listing[price]" id="listing_price" />
                        <div className="price-currency">
                          Fr
                        </div>
                        <div className="quantity-description">
                          par jour
                        </div>
                        <input type="hidden" name="listing[unit]" id="listing_unit" defaultValue="{&quot;unit_type&quot;:&quot;day&quot;,&quot;kind&quot;:&quot;time&quot;,&quot;quantity_selector&quot;:&quot;day&quot;}" />
                        <small> </small>
                      </div>
                      <div className="form-group" />
                      <div className="inventory-wrapper" style={{flexDirection: 'column'}}>
                        <label style={{display: 'inline-block'}}>Quantité</label>
                        <label className="custom-switch switch-for-flex">
                          <input name="listing[inventory_enabled]" type="hidden" defaultValue={0} />
                          <input onchange="window.ST.allowInventory({&quot;display&quot;:&quot;jour&quot;,&quot;value&quot;:&quot;{\&quot;unit_type\&quot;:\&quot;day\&quot;,\&quot;kind\&quot;:\&quot;time\&quot;,\&quot;quantity_selector\&quot;:\&quot;day\&quot;}&quot;,&quot;kind&quot;:&quot;time&quot;,&quot;selected&quot;:false})" type="checkbox" defaultValue={1} name="listing[inventory_enabled]" id="listing_inventory_enabled" />
                          <span className="slider round" />
                        </label>
                        <div className="input-inventory" style={{display: 'none'}}>
                          <label className="input" htmlFor="listing_available_quantity">Quantité disponible</label>
                          <input type="text" min={1} name="listing[available_quantity]" id="listing_available_quantity" />
                        </div>
                        <div className="minimum-quantity-to-buy" style={{display: 'none'}}>
                          <label className="input" htmlFor="listing_minimum_quantity_to_buy">Minimum Quantity buyer must Buy</label>
                          <input type="text" min={1} defaultValue={1} name="listing[minimum_quantity_to_buy]" id="listing_minimum_quantity_to_buy" />
                        </div>
                      </div>
                      <div className="form-group" id="date_time_container" style={{display: 'none'}}>
                        <div id="date_time_container">
                          <div className="choose_units">
                            <span>Choose pricing unit</span>
                            <select name="datetime_combination_pricing_unit" disabled="disabled">
                              <option value>Select</option>
                              <option value="Hour">Hour</option>
                              <option value="Day">Day</option>
                              <option value="Week">Week</option>
                              <option value="Month">Month</option>
                              <option value="Hour/Day">Hour/Day</option>
                              <option value="Day/Week">Day/Week</option>
                              <option value="Day/Month">Day/Month</option>
                              <option value="Hour/Day/Week">Hour/Day/Week</option>
                              <option value="Hour/Day/Month">Hour/Day/Month</option>
                              <option value="Day/Week/Month">Day/Week/Month</option>
                              <option value="Hour/Day/Week/Month">Hour/Day/Week/Month</option>
                            </select>
                          </div>
                          <div className="price_dropdown" style={{display: 'none'}} />
                        </div>
                      </div>
                      <div className="form-group" id="avail_calendar" style={{display: 'block'}}>
                        <label className="input" htmlFor="listing_min_booking_days">Nombre minimum de jours de réservation</label>
                        <input type="text" data-min={1} data-max={50} defaultValue={1} name="listing[min_booking_days]" id="listing_min_booking_days" />
                        <label className="input" htmlFor="listing_max_booking_days">Nombre maximum de jours de réservation</label>
                        <input type="text" name="listing[max_booking_days]" id="listing_max_booking_days" />
                        <div className="select-date-type">
                          <div className="custom_label">
                            Sélectionnez les dates non disponibles
                          </div>
                          <label className="date-switch">
                            <input name="listing[is_available]" type="hidden" defaultValue={0} /><input type="checkbox" defaultValue={1} name="listing[is_available]" id="listing_is_available" />
                            <span className="date-slider round" />
                          </label>
                          <div className="custom_label">
                            Sélectionnez les dates disponibles
                          </div>
                        </div>
                        <div id="avail_dates">
                          <div className="datepicker datepicker-inline">
                            <div className="datepicker-days" style={{display: 'block'}}>
                              <table className="table-condensed">
                                <thead>
                                  <tr>
                                    <th className="prev" style={{visibility: 'hidden'}}>«</th>
                                    <th colSpan={5} className="datepicker-switch">November 2021</th>
                                    <th className="next" style={{visibility: 'visible'}}>»</th>
                                  </tr>
                                  <tr>
                                    <th className="dow">Su</th>
                                    <th className="dow">Mo</th>
                                    <th className="dow">Tu</th>
                                    <th className="dow">We</th>
                                    <th className="dow">Th</th>
                                    <th className="dow">Fr</th>
                                    <th className="dow">Sa</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td className="old disabled day">31</td>
                                    <td className="disabled day">1</td>
                                    <td className="disabled day">2</td>
                                    <td className="disabled day">3</td>
                                    <td className="disabled day">4</td>
                                    <td className="disabled day">5</td>
                                    <td className="disabled day">6</td>
                                  </tr>
                                  <tr>
                                    <td className="disabled day">7</td>
                                    <td className="disabled day">8</td>
                                    <td className="disabled day">9</td>
                                    <td className="disabled day">10</td>
                                    <td className="disabled day">11</td>
                                    <td className="disabled day">12</td>
                                    <td className="disabled day">13</td>
                                  </tr>
                                  <tr>
                                    <td className="disabled day">14</td>
                                    <td className="disabled day">15</td>
                                    <td className="disabled day">16</td>
                                    <td className="day">17</td>
                                    <td className="day">18</td>
                                    <td className="day">19</td>
                                    <td className="day">20</td>
                                  </tr>
                                  <tr>
                                    <td className="day">21</td>
                                    <td className="day">22</td>
                                    <td className="day">23</td>
                                    <td className="day">24</td>
                                    <td className="day">25</td>
                                    <td className="day">26</td>
                                    <td className="day">27</td>
                                  </tr>
                                  <tr>
                                    <td className="day">28</td>
                                    <td className="day">29</td>
                                    <td className="day">30</td>
                                    <td className="new day">1</td>
                                    <td className="new day">2</td>
                                    <td className="new day">3</td>
                                    <td className="new day">4</td>
                                  </tr>
                                  <tr>
                                    <td className="new day">5</td>
                                    <td className="new day">6</td>
                                    <td className="new day">7</td>
                                    <td className="new day">8</td>
                                    <td className="new day">9</td>
                                    <td className="new day">10</td>
                                    <td className="new day">11</td>
                                  </tr>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <th colSpan={7} className="today" style={{display: 'none'}}>Today</th>
                                  </tr>
                                  <tr>
                                    <th colSpan={7} className="clear" style={{display: 'none'}}>Clear</th>
                                  </tr>
                                </tfoot>
                              </table>
                            </div>
                            <div className="datepicker-months" style={{display: 'none'}}>
                              <table className="table-condensed">
                                <thead>
                                  <tr>
                                    <th className="prev" style={{visibility: 'hidden'}}>«</th>
                                    <th colSpan={5} className="datepicker-switch">2021</th>
                                    <th className="next" style={{visibility: 'visible'}}>»</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td colSpan={7}>
                                      <span className="month disabled">Jan</span><span className="month disabled">Feb</span><span className="month disabled">Mar</span><span className="month disabled">Apr</span>
                                      <span className="month disabled">May</span><span className="month disabled">Jun</span><span className="month disabled">Jul</span><span className="month disabled">Aug</span>
                                      <span className="month disabled">Sep</span><span className="month disabled">Oct</span><span className="month">Nov</span><span className="month">Dec</span>
                                    </td>
                                  </tr>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <th colSpan={7} className="today" style={{display: 'none'}}>Today</th>
                                  </tr>
                                  <tr>
                                    <th colSpan={7} className="clear" style={{display: 'none'}}>Clear</th>
                                  </tr>
                                </tfoot>
                              </table>
                            </div>
                            <div className="datepicker-years" style={{display: 'none'}}>
                              <table className="table-condensed">
                                <thead>
                                  <tr>
                                    <th className="prev" style={{visibility: 'hidden'}}>«</th>
                                    <th colSpan={5} className="datepicker-switch">2020-2029</th>
                                    <th className="next" style={{visibility: 'visible'}}>»</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td colSpan={7}>
                                      <span className="year old disabled">2019</span><span className="year disabled">2020</span><span className="year">2021</span><span className="year">2022</span>
                                      <span className="year disabled">2023</span><span className="year disabled">2024</span><span className="year disabled">2025</span><span className="year disabled">2026</span>
                                      <span className="year disabled">2027</span><span className="year disabled">2028</span><span className="year disabled">2029</span><span className="year new disabled">2030</span>
                                    </td>
                                  </tr>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <th colSpan={7} className="today" style={{display: 'none'}}>Today</th>
                                  </tr>
                                  <tr>
                                    <th colSpan={7} className="clear" style={{display: 'none'}}>Clear</th>
                                  </tr>
                                </tfoot>
                              </table>
                            </div>
                          </div>
                        </div>
                        <input className="title_text_field" autoComplete="off" id="avail_dates_input" type="hidden" name="listing[blocked_dates]" defaultValue="[]" />
                      </div>
                      <div className="form-group">
                        <button className="button" onclick="ST['moveToNextStep'](event,1,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                          Suivant
                        </button>
                      </div>
                    </div>
                    <input type="hidden" name="listing_ordered_images" id="listing_ordered_images" />
                    <div className="form-section hidden" id="form-panel-2">
                      <div className="section-heading">Où est votre article</div>
                      <div className="form-group">
                        <label className="input" htmlFor="listing_origin">Lieu*</label>
                        <div className="location_text_field">
                          <input className="text_field required" onkeypress="hideLocationError()" defaultValue="Express Exchange PK 14, Road To PK 14, Logbessou II, Logbessou Pk.14, Douala III, Communauté urbaine de Douala, Wouri, Littoral, 4780 DLA, Cameroon" type="text" name="listing[origin]" id="listing_origin" />
                          <input defaultValue="Express Exchange PK 14, Road To PK 14, Logbessou II, Logbessou Pk.14, Douala III, Communauté urbaine de Douala, Wouri, Littoral, 4780 DLA, Cameroon" type="hidden" name="listing[origin_loc_attributes][address]" id="listing_origin_loc_attributes_address" />
                          <input defaultValue="Logbessou" type="hidden" name="listing[origin_loc_attributes][google_address]" id="listing_origin_loc_attributes_google_address" />
                          <input defaultValue="4.07884" type="hidden" name="listing[origin_loc_attributes][latitude]" id="listing_origin_loc_attributes_latitude" />
                          <input defaultValue="9.7937" type="hidden" name="listing[origin_loc_attributes][longitude]" id="listing_origin_loc_attributes_longitude" />
                          <img className="locate-me" id="locate-me-popup" onclick="ST.flightmaps.locationPermissionFlightmaps()" src="https://www.3n-immo.com/assets/gun-pointer-16a661881d78f30df4c60b92c988c447d312858df6e9557cc0ef8c4cfbc6b689.svg" alt="Gun pointer" />
                          <div className="loader" id="spinner" />
                        </div>
                        <div className="autocomplete-field" id="autocomplete-field-container" />
                        <p id="deny-location-message" />
                        <div className="jsmap listing-form-map leaflet-container leaflet-touch leaflet-fade-anim leaflet-grab leaflet-touch-drag leaflet-touch-zoom" id="map" tabIndex={0} style={{position: 'relative'}}>
                          <div className="leaflet-pane leaflet-map-pane" style={{transform: 'translate3d(0px, 0px, 0px)'}}>
                            <div className="leaflet-pane leaflet-tile-pane">
                              <div className="leaflet-gl-layer mapboxgl-map" style={{width: '0px', height: '0px', transform: 'translate3d(0px, 0px, 0px)'}}>
                                <div className="mapboxgl-canary" style={{visibility: 'hidden'}} />
                                <div className="mapboxgl-canvas-container">
                                  <canvas className="mapboxgl-canvas leaflet-image-layer leaflet-zoom-animated" tabIndex={0} aria-label="Map" width={359} height={269} style={{position: 'absolute', width: '400px', height: '300px', transform: 'translate3d(0px, 0px, 0px) scale(1)'}} />
                                </div>
                                <div className="mapboxgl-control-container">
                                  <div className="mapboxgl-ctrl-top-left" />
                                  <div className="mapboxgl-ctrl-top-right" />
                                  <div className="mapboxgl-ctrl-bottom-left">
                                    <div className="mapboxgl-ctrl" style={{display: 'none'}}>
                                      <a className="mapboxgl-ctrl-logo mapboxgl-compact" target="_blank" rel="noopener nofollow" href="https://www.mapbox.com/" aria-label="Mapbox logo" />
                                    </div>
                                  </div>
                                  <div className="mapboxgl-ctrl-bottom-right" />
                                </div>
                              </div>
                              <div className="leaflet-layer" style={{zIndex: 1, opacity: 1}}>
                                <div className="leaflet-tile-container leaflet-zoom-animated" style={{zIndex: 18, transform: 'translate3d(0px, 0px, 0px) scale(1)'}}>
                                  <img alt="" role="presentation" src="http://c.tile.osm.org/13/4318/4003.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(-220px, -27px, 0px)', opacity: 1}} />
                                </div>
                              </div>
                            </div>
                            <div className="leaflet-pane leaflet-shadow-pane">
                              <img src="https://unpkg.com/leaflet@1.0.3/dist/images/marker-shadow.png" className="leaflet-marker-shadow leaflet-zoom-animated" alt="" style={{marginLeft: '-12px', marginTop: '-41px', width: '41px', height: '41px', transform: 'translate3d(0px, 0px, 0px)'}} />
                            </div>
                            <div className="leaflet-pane leaflet-overlay-pane" />
                            <div className="leaflet-pane leaflet-marker-pane">
                              <img src="https://unpkg.com/leaflet@1.0.3/dist/images/marker-icon.png" className="leaflet-marker-icon leaflet-zoom-animated leaflet-interactive leaflet-marker-draggable" tabIndex={0} style={{marginLeft: '-12px', marginTop: '-41px', width: '25px', height: '41px', transform: 'translate3d(0px, 0px, 0px)', zIndex: 0}} />
                            </div>
                            <div className="leaflet-pane leaflet-tooltip-pane" />
                            <div className="leaflet-pane leaflet-popup-pane" />
                            <div className="leaflet-proxy leaflet-zoom-animated" style={{transform: 'translate3d(1.10563e6px, 1.02479e6px, 0px) scale(4096)'}} />
                          </div>
                          <div className="leaflet-control-container">
                            <div className="leaflet-top leaflet-left">
                              <div className="leaflet-control-zoom leaflet-bar leaflet-control">
                                <a className="leaflet-control-zoom-in" href="#" title="Zoom in" role="button" aria-label="Zoom in">+</a>
                                <a className="leaflet-control-zoom-out" href="#" title="Zoom out" role="button" aria-label="Zoom out">-</a>
                              </div>
                            </div>
                            <div className="leaflet-top leaflet-right" />
                            <div className="leaflet-bottom leaflet-left" />
                            <div className="leaflet-bottom leaflet-right">
                              <div className="leaflet-control-attribution leaflet-control">
                                <a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a> | © <a href="http://osm.org/copyright">OpenStreetMap</a> contributors
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <button className="button" onclick="ST['moveToNextStep'](event,2,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                          Suivant
                        </button>
                      </div>
                    </div>
                    <input type="hidden" name="listing_ordered_images" id="listing_ordered_images" />
                    <div className="form-section hidden" id="form-panel-3">
                      <div className="section-heading">
                        Information additionnelle
                      </div>
                      <div className="new_listing_form_field_container">
                        <div className="form-group">
                          <label className="input-label" htmlFor="custom_fields_7873">Toilettes*</label>
                          {/* Don't use HTML5 number input. It's a mess. */}
                          <input type="text" name="custom_fields[7873]" id="custom_fields_7873" defaultValue data-min={0} data-max={10} className="required number-no-decimals" />
                        </div>
                        <div className="form-group">
                          <label className="input-label" htmlFor="custom_fields_7843">Superficie (m2)*</label>
                          {/* Don't use HTML5 number input. It's a mess. */}
                          <input type="text" name="custom_fields[7843]" id="custom_fields_7843" defaultValue data-min={0} data-max={999999} className="required number-no-decimals" />
                        </div>
                        <div className="form-group">
                          <label className="input-label" htmlFor="custom_fields_7867">Equipements</label>
                          <input type="text" name="custom_fields[7867]" id="custom_fields_7867" defaultValue className />
                        </div>
                        <div className="form-group">
                          <label className="input-label" htmlFor="custom_fields_7870">Chambres*</label>
                          {/* Don't use HTML5 number input. It's a mess. */}
                          <input type="text" name="custom_fields[7870]" id="custom_fields_7870" defaultValue data-min={0} data-max={60} className="required number-no-decimals" />
                        </div>
                        <div className="form-group">
                          <label className="input-label" htmlFor="custom_fields_7871">Pièces*</label>
                          {/* Don't use HTML5 number input. It's a mess. */}
                          <input type="text" name="custom_fields[7871]" id="custom_fields_7871" defaultValue data-min={0} data-max={60} className="required number-no-decimals" />
                        </div>
                        <div className="form-group">
                          <label className="input-label" htmlFor="custom_fields_7872">Salle de bain*</label>
                          {/* Don't use HTML5 number input. It's a mess. */}
                          <input type="text" name="custom_fields[7872]" id="custom_fields_7872" defaultValue data-min={0} data-max={60} className="required number-no-decimals" />
                        </div>
                      </div>
                      <div className="form-group">
                        <button className="button" onclick="ST['moveToNextStep'](event,3,[{&quot;text&quot;:&quot;La description&quot;,&quot;kind&quot;:&quot;description&quot;},{&quot;text&quot;:&quot;Tarification&quot;,&quot;kind&quot;:&quot;price&quot;},{&quot;text&quot;:&quot;Emplacement&quot;,&quot;kind&quot;:&quot;location&quot;},{&quot;text&quot;:&quot;Information additionnelle&quot;,&quot;kind&quot;:&quot;custom&quot;},{&quot;text&quot;:&quot;Photos&quot;,&quot;kind&quot;:&quot;photos&quot;}])">
                          Suivant
                        </button>
                      </div>
                    </div>
                    <input type="hidden" name="listing_ordered_images" id="listing_ordered_images" />
                    <div className="form-section hidden" id="form-panel-4">
                      <div className="section-heading">
                        Ajoutez quelques photos
                      </div>
                      <label className="input" htmlFor="listing_image">Image</label>
                      <div className="info-text-container">
                        <div className="info-text-icon">
                          <i className="icon-info-sign" />
                        </div>
                        <div className="info-text-content">
                          <p>Pour un résultat idéal, ajoutez des images au format JPG, GIF ou PNG, de dimensions 660x440 pixels.</p>
                        </div>
                      </div>
                      <div className="flash-notifications js-listing-image-loading hidden" style={{display: 'none'}}>
                        <div className="flash-notice flash-notification">
                          <div className="flash-icon icon-circle-blank" />
                          <div className="flash-text">
                            Envoi de l image en cours...
                          </div>
                        </div>
                      </div>
                      <div className="flash-notifications js-listing-image-loading-done hidden" style={{display: 'none'}}>
                        <div className="flash-notice flash-notification">
                          <div className="flash-icon icon-ok" />
                          <div className="flash-text">
                            Toutes les images ont bien été envoyées ! Leur traitement va prendre quelques instants mais vous pouvez publier l'annonce et continuer sans crainte.
                          </div>
                        </div>
                      </div>
                      <div className="listing-images ui-sortable" id="image-uploader-container">
                        <div className="fileinput-button upload-image-placeholder" id="fileupload">
                          <div className="fileupload-text-container" style={{}}>
                            <div className="fileupload-centered-text">
                              <div className="fileupload-text">Choisir une image</div>
                              <div className="fileupload-small-text" />
                              <div className="fileupload-error-text" />
                            </div>
                          </div>
                          <input className="fileupload" multiple="multiple" type="file" style={{display: 'inline-block'}} />
                          <img className="fileupload-preview-image" style={{display: 'none'}} />
                          <div className="fileupload-preview-remove-image">
                            <i className="icon-remove icon-fix" />
                          </div>
                          <input className="listing-image-id" name="listing_images[][id]" type="hidden" />
                        </div>
                        <div className="fileinput-button upload-image-placeholder" id="fileupload">
                          <div className="fileupload-text-container" style={{}}>
                            <div className="fileupload-centered-text">
                              <div className="fileupload-text">Choisir une image</div>
                              <div className="fileupload-small-text" />
                              <div className="fileupload-error-text" />
                            </div>
                          </div>
                          <input className="fileupload" multiple="multiple" type="file" style={{display: 'inline-block'}} />
                          <img className="fileupload-preview-image" style={{display: 'none'}} />
                          <div className="fileupload-preview-remove-image">
                            <i className="icon-remove icon-fix" />
                          </div>
                          <input className="listing-image-id" name="listing_images[][id]" type="hidden" />
                        </div>
                        <div className="fileinput-button upload-image-placeholder" id="fileupload">
                          <div className="fileupload-text-container" style={{}}>
                            <div className="fileupload-centered-text">
                              <div className="fileupload-text">+ En ajouter plus</div>
                              <div className="fileupload-small-text" />
                              <div className="fileupload-error-text" />
                            </div>
                          </div>
                          <input className="fileupload" multiple="multiple" type="file" style={{display: 'inline-block'}} />
                          <img className="fileupload-preview-image" style={{display: 'none'}} />
                          <div className="fileupload-preview-remove-image">
                            <i className="icon-remove icon-fix" />
                          </div>
                          <input className="listing-image-id" name="listing_images[][id]" type="hidden" />
                        </div>
                      </div>
                      <input defaultValue={22422} type="hidden" name="listing[category_id]" id="listing_category_id" />
                      <input defaultValue={19625} type="hidden" name="listing[listing_shape_id]" id="listing_listing_shape_id" />
                      <div className="form-group">
                        <button name="button" type="submit" className="send_button">Publier l'annonce</button>
                      </div>
                    </div>
                    <input type="hidden" name="listing_ordered_images" id="listing_ordered_images" />
                  </form>
                  <div className="lightbox" id="help_valid_until">
                    <div className="lightbox-content">
                      <a className="close lightbox-x" href="#" id="close_x">
                        <i className="icon-remove" />
                      </a>
                      <h2>Date d'expiration</h2>
                      <p />
                      <p>&nbsp;</p>
                      <p />
                      <p>
                        &nbsp;&nbsp;Vous pouvez choisir combien de temps votre annonce restera en ligne. Après la date d'expiration, l'annonce sera close automatiquement. Les annonces closes ne sont pas visibles par les autres
                        utilisateurs. Vous pouvez, à tout moment, changer la date d'expiration en passant par le lien "Editer l'annonce". Vous pouvez également ré-ouvrir une annonce close en passant par le même menu.
                      </p>
                    </div>
                  </div>
                  <div className="lightbox" id="markdown-help-popup">
                    <div className="lightbox-content">
                      <a className="close lightbox-x" href="#" id="close_x">
                        <i className="icon-remove" />
                      </a>
                      <h2>Formatting help using Markdown</h2>
                      <p>
                        Markdown is a simple way to format text that looks great on any device. It doesn’t do anything fancy like change the font size, color, or type — just the essentials, using keyboard symbols you already
                        know.
                      </p>
                      <table className="markdown-help-table">
                        <tbody>
                          <tr>
                            <th>Formatting</th>
                            <th>Result</th>
                          </tr>
                          <tr>
                            <td>
                              # H1<br />
                              ## H2<br />
                              ### H3
                            </td>
                            <td>
                              <h1>H1</h1>
                              <br />
                              <h2>H2</h2>
                              <br />
                              <h3>H3</h3>
                            </td>
                          </tr>
                          <tr>
                            <td>*italic*</td>
                            <td>
                              <i>italic</i>
                            </td>
                          </tr>
                          <tr>
                            <td>**bold**</td>
                            <td>
                              <b>bold</b>
                            </td>
                          </tr>
                          <tr>
                            <td>_underline_</td>
                            <td>
                              <u>underline</u>
                            </td>
                          </tr>
                          <tr>
                            <td>~~strikethrough~~</td>
                            <td>
                              <strike>strikethrough</strike>
                            </td>
                          </tr>
                          <tr>
                            <td>[Inline link](https://www.example.com)</td>
                            <td>
                              <a href="https://www.example.com" target="_blank">Inline link</a>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              - List<br />
                              - List<br />
                              - List
                            </td>
                            <td>
                              <ul>
                                <li>
                                  List
                                  <br />
                                  <br />
                                </li>
                                <li>
                                  List
                                  <br />
                                  <br />
                                </li>
                                <li>
                                  List
                                  <br />
                                </li>
                              </ul>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              1. One
                              <br />
                              2. Two
                              <br />
                              3. Three
                            </td>
                            <td>
                              <ol>
                                <li>
                                  One
                                  <br />
                                  <br />
                                </li>
                                <li>
                                  Two
                                  <br />
                                  <br />
                                </li>
                                <li>
                                  Three
                                  <br />
                                  <br />
                                </li>
                              </ol>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>

    )
}
export default AjoutAnnonce