import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import AjoutAnnonce from './AjoutAnnonce';
import DepotAnnonce_categorie from './DepotAnnonce_Categorie';
import TypeAnnonce from './TypeAnnonce';


const DepotAnnonce = (props) =>{
  const { match } = props;

    return(

      <Router>
     <Switch> 
     <Route path={`${match.path}/depotannoncecategorie`} component={DepotAnnonce_categorie} />
     <Route path={`${match.path}/typeannonce`}  component={TypeAnnonce} />
     <Route path={`${match.path}/ajoutannonce`}  component={AjoutAnnonce} />
     </Switch>
    </Router>

        
    )
}
export default DepotAnnonce