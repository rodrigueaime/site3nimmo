import React from 'react';
import '../../../componentsCss/Depotannonce_categorie.css'

const DepotAnnonce_categorie = () =>{

    return(

        <article className="page-content" style={{}}>
        <div className="wrapper">
          <div className="flash-notifications">
            <div className="flash-notice flash-notification">
              <div className="flash-icon ss-check" />
              <div className="flash-text">Bienvenue, <a href="/fr/eddy">Eddy Harold</a> ! Voulez-vous aller dans <a href="/fr/admin/details/edit">votre panneau d'administration</a> ?</div>
            </div>
          </div>
          <div className="new-listing-form" id="new_listing_form">
            <div className="centered-section main-category-section">
              <div id="selected-groups" />
              <div className="selected-group" name="category">
                <a className="select selected hidden" data-id={22334}>
                  <div className="link-text">Catégorie : Default category</div>
                </a>
                <a className="select selected hidden" data-id={22422}>
                  <div className="link-text">Catégorie : Appartement</div>
                </a>
                <a className="select selected hidden" data-id={22423}>
                  <div className="link-text">Catégorie : Villa</div>
                </a>
                <a className="select selected hidden" data-id={22528}>
                  <div className="link-text">Catégorie : Bureau</div>
                </a>
                <a className="select selected hidden" data-id={22529}>
                  <div className="link-text">Catégorie : Terrain</div>
                </a>
                <a className="select selected hidden" data-id={24558}>
                  <div className="link-text">Catégorie : Vehicule</div>
                </a>
                <a className="select selected hidden" data-id={24680}>
                  <div className="link-text">Catégorie : Restaurant</div>
                </a>
              </div>
              <div className="selected-group" name="listing_shape">
                <a className="select selected hidden" data-id={19624}>
                  <div className="link-text">Type d'annonce : Location longue durée (non meublée)</div>
                </a>
                <a className="select selected hidden" data-id={19625}>
                  <div className="link-text">Type d'annonce : Location court/moyenne durée (meublée)</div>
                </a>
                <a className="select selected hidden" data-id={19721}>
                  <div className="link-text">Type d'annonce : Vente</div>
                </a>
                <a className="select selected hidden" data-id={21740}>
                  <div className="link-text">Type d'annonce : Information</div>
                </a>
                <a className="select selected hidden" data-id={21741}>
                  <div className="link-text">Type d'annonce : Vente 2</div>
                </a>
              </div>
              <h2 className="listing-form-title" id="foo">Choisir une catégorie</h2>
              <div id="option-groups" />
              <div className="option-group" name="category">
                <a className="select option" data-id={22334}>
                  <div className="link-text">Default category</div>
                </a>
                <a className="select option" data-id={22422}>
                  <div className="link-text">Appartement</div>
                </a>
                <a className="select option" data-id={22423}>
                  <div className="link-text">Villa</div>
                </a>
                <a className="select option" data-id={22528}>
                  <div className="link-text">Bureau</div>
                </a>
                <a className="select option" data-id={22529}>
                  <div className="link-text">Terrain</div>
                </a>
                <a className="select option" data-id={24558}>
                  <div className="link-text">Vehicule</div>
                </a>
                <a className="select option" data-id={24680}>
                  <div className="link-text">Restaurant</div>
                </a>
              </div>
              <div className="option-group" name="listing_shape">
                <a className="select option hidden" data-id={19624}>
                  <div className="link-text">Location longue durée (non meublée)</div>
                </a>
                <a className="select option hidden" data-id={19625}>
                  <div className="link-text">Location court/moyenne durée (meublée)</div>
                </a>
                <a className="select option hidden" data-id={19721}>
                  <div className="link-text">Vente</div>
                </a>
                <a className="select option hidden" data-id={21740}>
                  <div className="link-text">Information</div>
                </a>
                <a className="select option hidden" data-id={21741}>
                  <div className="link-text">Vente 2</div>
                </a>
              </div>
            </div>
            <div className="hidden js-form-fields" />
          </div>
        </div>
      </article>
    )
}
export default DepotAnnonce_categorie