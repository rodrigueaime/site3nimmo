import React from "react";

const NavBarAccueil = () =>{

    return(
          
                <div id="topbar-placeholder" className="topbar-placeholder">
                <div data-reactroot className="Topbar Topbar__topbar__1ODH7">
                <div className="Topbar__topbarMobileMenu__2K-8c MenuMobile MenuMobile__menuMobile___Ivzx Topbar__topbarMobileMenu__2K-8c" tabIndex={0}><div className="MenuMobile_overlay MenuMobile__overlay__2sZqq" style={{backgroundColor: 'rgb(215, 155, 40)'}} />
                <div className="MenuLabelMobile MenuMobile__menuLabelMobile__3uXwZ ">
                <span className="MenuMobile__menuLabelMobileIcon__14XBz" title="Menu">
                <svg width={18} height={12} viewBox="18 19 18 12" xmlns="http://www.w3.org/2000/svg">
                <g fill="#34495E" fillRule="evenodd" transform="translate(18 19)">
                <rect width={18} height={2} rx={1} /><rect y={5} width={18} height={2} rx={1} />
                <rect y={10} width={18} height={2} rx={1} /></g></svg></span>
                <div className="NotificationBadge__notificationBadge__he8-3 MenuMobile__notificationBadge__2RNEh">
                <span className="NotificationBadge__notificationBadgeCount__2upgE">2</span></div></div>
                <div className="OffScreenMenu MenuMobile__offScreenMenu__2fKaS"><div className="OffScreenMenu_scrollpane MenuMobile__scrollPane__2VvXd">
                <div className="OffScreenMenu_header _sideCustAvatar MenuMobile__offScreenHeader__KnANh"><div className="MenuMobile__avatarSpacer__2zKOW">
                <a href="/fr/eddy" className="Avatar__link__RlpRO"><div className="Avatar Avatar__textAvatar__4oTir" title="Eddy Harold D" style={{height: '44px', width: '44px'}}>ED</div></a></div>
                <a className="MenuMobile__offScreenHeaderNewListingButton__3ssaA AddNewListingButton AddNewListingButton__button__2H8yh" href="/fr/listings/new" title="Déposer une annonce"><span className="AddNewListingButton__backgroundContainer__4Nn3Z AddNewListingButton_background" style={{backgroundColor: 'rgb(215, 155, 40)'}} />
                <span className="AddNewListingButton__mobile__2JhHF AddNewListingButton_mobile" style={{color: 'rgb(215, 155, 40)'}}>+ Déposer une annonce</span>
                <span className="AddNewListingButton__desktop__17Luf AddNewListingButton_desktop">+ Déposer une annonce</span></a></div>
                <div className="OffScreenMenu_main MenuMobile__offScreenMain__1ruzb">
                <div className="MenuSection MenuMobile__menuSection__gD-As">
                <div className="MenuSection_title MenuMobile__menuSectionTitle__2HlQg">Menu</div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC">
                <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="https://www.3n-immo.com" style={{color: 'rgb(215, 155, 40)'}}>Accueil</a></div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC">
                <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/infos/about" style={{color: 'rgb(215, 155, 40)'}}>A propos</a></div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC">
                <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/user_feedbacks/new" style={{color: 'rgb(215, 155, 40)'}}>Nous contacter</a></div></div>
                <div className="MenuSection MenuMobile__menuSection__gD-As">
                <div className="MenuSection_title MenuMobile__menuSectionTitle__2HlQg">Utilisateur</div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC">
                <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/admin" style={{color: 'rgb(215, 155, 40)'}}>Panneau d'administration</a></div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC">
                <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/eddy/inbox" style={{color: 'rgb(215, 155, 40)'}}>{/* react-text: 33 */}Messagerie{/* /react-text */}<div className="NotificationBadge__notificationBadge__he8-3 Topbar__notificationBadge__3Ghei">
                <span className="NotificationBadge__notificationBadgeCount__2upgE Topbar__notificationBadgeCount__tOt-2">2</span></div></a></div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC">
                <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/eddy" style={{color: 'rgb(215, 155, 40)'}}>Profil</a></div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC">
                <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/eddy?show_closed=1" style={{color: 'rgb(215, 155, 40)'}}>Mes annonces</a></div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC">
                <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/eddy/settings" style={{color: 'rgb(215, 155, 40)'}}>Paramètres</a></div>
                <div className="MenuItem MenuItem__menuitem__3_-l_  MenuMobile__menuSectionMenuItem__2yMLC"><a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/logout" style={{color: 'rgb(215, 155, 40)'}}>Déconnexion</a></div></div></div><div><div><div className="OffScreenMenu_footer MenuMobile__offScreenFooter__3sjuV"><div className="MobileMenu_languages MenuMobile__languages__1Ekmy"><div className="MenuSection_title MenuMobile__menuSectionTitle__2HlQg"><span className="MenuMobile__menuSectionIcon__8bMU1"><svg width={16} height={16} viewBox="24 23 16 16" xmlns="http://www.w3.org/2000/svg"><g fill="none" fillRule="evenodd" stroke="#B2B2B2"><path d="M38.708 30.975a6.73 6.73 0 0 1-6.727 6.733c-3.715 0-6.69-3.135-6.69-6.854 0-3.621 2.841-6.412 6.42-6.556a6.67 6.67 0 0 1 6.997 6.677zM31.711 24.297c-3.5 3.792-3.5 8.739 0 13.405M32.295 24.298c3.5 3.791 3.5 8.736 0 13.403M26.166 34.195H37.87M26.403 27.195h11.12M25.292 30.695h13.38" /></g></svg></span>{/* react-text: 50 */}Langue{/* /react-text */}</div><div className="LanguagesMobile_languageList MenuMobile__languageList__DS1xm">
                <div className="LanguagesMobile_languageLink MenuMobile__languageLink__1XsC3"><a className="Link__link__3pNRT MenuItem_link LocaleCurrencyMobile active-locale-indicator activeMenuItem" href="javascript:void(0)" data-locale="fr" data-currency="nil" style={{color: 'rgb(74, 74, 74)'}}>Français</a></div><div className="LanguagesMobile_languageLink MenuMobile__languageLink__1XsC3"><a className="Link__link__3pNRT MenuItem_link LocaleCurrencyMobile active-locale-indicator deactiveMenuItem" href="javascript:void(0)" data-locale="en" data-currency="nil" style={{color: 'rgb(215, 155, 40)'}}>English</a></div></div></div></div></div>
                <a href className="MenuMobile__updateLocalebutton__jetyV update-locale-button-sm" style={{backgroundColor: 'rgb(215, 155, 40)'}}>Save</a></div></div></div></div><a className="Logo__logoNewTheme__ggzOm Logo Topbar__topbarLogo__3IOxU Topbar__topbarLogoPadding__1Pt44 Logo__logo__3sGgU" href="https://www.3n-immo.com" style={{color: 'rgb(215, 155, 40)'}}><img src="https://yelodotred.s3-us-west-2.amazonaws.com/images/communities/wide_logos/8248/header/Inner_logo_%2818%29.png?1616191485" alt="3N Immo" className="Logo__newlogoImage__q4g-U Logo__logoImage__3oOkB" srcSet="https://yelodotred.s3-us-west-2.amazonaws.com/images/communities/wide_logos/8248/header_highres/Inner_logo_%2818%29.png?1616191485  2x" /><span className="logo-border Logo__logoBorder__3fyP9" /></a><div className="Topbar__topbarMediumSpacer__29yXk" /><div className="Topbar__topbarMobileSearchPlaceholder__3bhIu" /><div className="Topbar__topbarMenuSpacer__gcKVN"><div className="MenuPriority MenuPriority__menuPriority__3y3Pu MenuPriority__noPriorityLinks__2I9oP">
                <div className="MenuPriority__priorityLinks__XgHdH" style={{position: 'absolute', top: '-2000px', left: '-2000px', width: '100%'}} /><div className="MenuPriority__hiddenLinks__1LcCU Menu  Menu__menu__1nYnK" tabIndex={0}><div className="MenuLabel Menu__menuLabel__17fat ">
                <span className="Menu__menuLabelIcon__3gpCG"><svg width={16} height={10} viewBox="18 19 18 12" xmlns="http://www.w3.org/2000/svg"><g fill="#34495E" fillRule="evenodd" transform="translate(18 19)"><rect width={18} height={2} rx={1} /><rect y={5} width={18} height={2} rx={1} /><rect y={10} width={18} height={2} rx={1} /></g></svg></span>{/* react-text: 70 */}Menu{/* /react-text */}</div><div className="MenuContent Menu__menuContent__3VW0B Menu__newThemeMenu__2hlvl"><div className="Menu__menuContentArrowBelow__2RPsL" style={{left: '235px'}} /><div className="Menu__menuContentArrowTop__2cSiD" style={{left: '235px'}} /><div className="Menu__menuScroll__2gWsw">{/* react-text: 75 */}{/* /react-text */}<div><div className="MenuItem MenuItem__menuitem__3_-l_  "><a className="MenuItem_link MenuItem__menuitemLink__2Eedg " href="https://www.3n-immo.com">Accueil</a></div><div className="MenuItem MenuItem__menuitem__3_-l_  "><a className="MenuItem_link MenuItem__menuitemLink__2Eedg " href="/fr/infos/about">A propos</a></div><div className="MenuItem MenuItem__menuitem__3_-l_  "><a className="MenuItem_link MenuItem__menuitemLink__2Eedg " href="/fr/user_feedbacks/new">Nous contacter</a></div></div>{/* react-text: 83 */}{/* /react-text */}</div>{/* react-text: 84 */}{/* /react-text */}</div></div></div></div><a className="Topbar__topbarListingButton__UlpUn AddNewThemeListingButton AddNewListingButton__themeButton__R-CLU AddNewListingButton__responsiveLayout__1JnL9" href="/fr/listings/new" title="Déposer une annonce"><span className="AddNewListingButton__mobile__2JhHF AddNewListingButton_mobile" style={{color: 'rgb(215, 155, 40)'}}>+ Déposer une annonce</span><span className="AddNewListingButton__desktopNewTheme__3D_T2 AddNewListingButton_desktop" style={{color: 'rgb(215, 155, 40)'}}>+ Déposer une annonce</span><span className="logo-border AddNewListingButton__logoBorder__37I2e" /></a><div className="topbar-messages-container-parent"><a className="Messages__messagesHeadingTopbarContainer__ikgns topbar-messages-container" href="/fr/eddy/inbox"><div className="Messages__messagesHeadingTopbar__3aU2u topbar-messages-icon"><svg version="1.1" width="20px" fill="#ffffff" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512" xmlSpace="preserve">
                            <g>
                              <g>
                                <g>
                                  <path d="M10.688,95.156C80.958,154.667,204.26,259.365,240.5,292.01c4.865,4.406,10.083,6.646,15.5,6.646
                          c5.406,0,10.615-2.219,15.469-6.604c36.271-32.677,159.573-137.385,229.844-196.896c4.375-3.698,5.042-10.198,1.5-14.719
                          C494.625,69.99,482.417,64,469.333,64H42.667c-13.083,0-25.292,5.99-33.479,16.438C5.646,84.958,6.313,91.458,10.688,95.156z" />
                                  <path d="M505.813,127.406c-3.781-1.76-8.229-1.146-11.375,1.542C416.51,195.01,317.052,279.688,285.76,307.885
                          c-17.563,15.854-41.938,15.854-59.542-0.021c-33.354-30.052-145.042-125-208.656-178.917c-3.167-2.688-7.625-3.281-11.375-1.542
                          C2.417,129.156,0,132.927,0,137.083v268.25C0,428.865,19.135,448,42.667,448h426.667C492.865,448,512,428.865,512,405.333
                          v-268.25C512,132.927,509.583,129.146,505.813,127.406z" />
                                </g>
                              </g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                            <g>
                            </g>
                          </svg></div>
                          <div className="Messages__messagesCountTopbar__1S9BN">
                          <span>2</span></div></a></div><div tabIndex={0} className="AvatarDropdown__avatarNewTheme__3Smzg AvatarDropdown _custAvatar Topbar__topbarAvatarDropdown__2fBjH AvatarDropdown__avatarDropdown__1Vjt6 AvatarDropdown__hasNotifications__2q9xK"><div className="AvatarDropdown__avatarWithNotificationsNew__b0Taz AvatarDropdown__avatarWithNotifications__1TdZT avatar-with-notifications"><div className="Avatar Avatar__textAvatar__4oTir" title="Eddy Harold D" style={{height: '100%', width: '100%'}}>ED</div></div>
                          <div className="AvatarDropdown__avatartArrow__2UgxZ" />
                          <div className="AvatarDropdown__avatarProfileDropdown__2gYjH"><div className="ProfileDropdown__rootArrowTop__3Vvtx" />
                          <div className="ProfileDropdown__rootArrowBelow__3YUUJ" />
                          <div className="ProfileDropdown__newProfileBox__1tyto ProfileDropdown__box__T_uM4">
                          <div className="ProfileDropdown__profileActions__34o4h">{/* react-text: 103 */}{/* /react-text */}<a href="/fr/eddy" className="ProfileDropdown__profileAction__1H9nm">
                          <div className="ProfileDropdown__profileActionIconWrapper__1v7N_" />
                          <div className="ProfileDropdown__profileActionLabel__2lW8j">Profil</div></a>
                          <a href="/fr/eddy/settings" className="ProfileDropdown__profileAction__1H9nm">
                          <div className="ProfileDropdown__profileActionIconWrapper__1v7N_" />
                          <div className="ProfileDropdown__profileActionLabel__2lW8j">Paramètres</div></a></div>
                          <div className="ProfileDropdown__basiclogoutArea__3IY-- ProfileDropdown__logoutArea__38Dcu">
                          <a href="/fr/admin" className="ProfileDropdown__adminLink__342Ug" style={{color: 'rgb(215, 155, 40)', backgroundColor: 'rgb(215, 155, 40)'}}>Panneau d'administration</a><a className="ProfileDropdown__logoutLink__15uxw" href="/fr/logout">Déconnexion</a></div></div></div>
                          <span className="logo-border AvatarDropdown__logoBorder__3ojKE" /></div><div className="Topbar__topbarMenu__hznJ1 Menu  Menu__menu__1nYnK" tabIndex={0}><div className="menu__label Menu__menuLabel__17fat Topbar__topbarLanguageMenuLabel__-0QYT">{/* react-text: 116 */}{/* /react-text */}<span className="Menu__menuLabelDropdownIconOpen__3NfNG Menu__menuLabelDropdownIcon__1QkrJ">
                          <svg height="18px" viewBox="0 0 480 480" width="18px" xmlns="http://www.w3.org/2000/svg"><path d="m240 0c-132.546875 0-240 107.453125-240 240s107.453125 240 240 240 240-107.453125 240-240c-.148438-132.484375-107.515625-239.851562-240-240zm207.566406 324.078125-68.253906 11.777344c7.8125-28.652344 12.03125-58.164063 12.558594-87.855469h71.929687c-.902343 26.117188-6.398437 51.871094-16.234375 76.078125zm-431.367187-76.078125h71.929687c.527344 29.691406 4.746094 59.203125 12.558594 87.855469l-68.253906-11.777344c-9.835938-24.207031-15.332032-49.960937-16.234375-76.078125zm16.234375-92.078125 68.253906-11.777344c-7.8125 28.652344-12.03125 58.164063-12.558594 87.855469h-71.929687c.902343-26.117188 6.398437-51.871094 16.234375-76.078125zm215.566406-27.472656c28.746094.367187 57.421875 2.984375 85.761719 7.832031l28.238281 4.871094c8.675781 29.523437 13.34375 60.078125 13.878906 90.847656h-127.878906zm88.488281-7.9375c-29.238281-4.996094-58.828125-7.695313-88.488281-8.0625v-96c45.863281 4.40625 85.703125 46.398437 108.28125 107.511719zm-104.488281-8.0625c-29.660156.367187-59.242188 3.066406-88.480469 8.0625l-19.800781 3.425781c22.578125-61.128906 62.417969-103.136719 108.28125-107.523438zm-85.753906 23.832031c28.335937-4.847656 57.007812-7.464844 85.753906-7.832031v103.550781h-127.878906c.535156-30.769531 5.203125-61.324219 13.878906-90.847656zm-42.125 111.71875h127.878906v103.550781c-28.746094-.367187-57.421875-2.984375-85.761719-7.832031l-28.238281-4.871094c-8.675781-29.523437-13.34375-60.078125-13.878906-90.847656zm39.390625 111.488281c29.238281 5.003907 58.824219 7.714844 88.488281 8.105469v96c-45.863281-4.410156-85.703125-46.402344-108.28125-107.515625zm104.488281 8.105469c29.660156-.390625 59.242188-3.101562 88.480469-8.105469l19.800781-3.425781c-22.578125 61.128906-62.417969 103.136719-108.28125 107.523438zm85.753906-23.875c-28.335937 4.847656-57.007812 7.464844-85.753906 7.832031v-103.550781h127.878906c-.535156 30.769531-5.203125 61.324219-13.878906 90.847656zm58.117188-111.71875c-.527344-29.691406-4.746094-59.203125-12.558594-87.855469l68.253906 11.777344c9.835938 24.207031 15.332032 49.960937 16.234375 76.078125zm47.601562-93.710938-65.425781-11.289062c-11.761719-38.371094-33.765625-72.808594-63.648437-99.601562 55.878906 18.648437 102.21875 58.457031 129.074218 110.890624zm-269.871094-110.890624c-29.882812 26.792968-51.886718 61.230468-63.648437 99.601562l-65.425781 11.289062c26.855468-52.433593 73.195312-92.242187 129.074218-110.890624zm-129.074218 314.3125 65.425781 11.289062c11.761719 38.371094 33.765625 72.808594 63.648437 99.601562-55.878906-18.648437-102.21875-58.457031-129.074218-110.890624zm269.871094 110.890624c29.882812-26.792968 51.886718-61.230468 63.648437-99.601562l65.425781-11.289062c-26.855468 52.433593-73.195312 92.242187-129.074218 110.890624zm0 0" /></svg></span><span className="Menu__menuLabelDropdownIconClosed__3ja5m Menu__menuLabelDropdownIcon__1QkrJ"><svg height="18px" viewBox="0 0 480 480" width="18px" xmlns="http://www.w3.org/2000/svg" >
                         <path d="m240 0c-132.546875 0-240 107.453125-240 240s107.453125 240 240 240 240-107.453125 240-240c-.148438-132.484375-107.515625-239.851562-240-240zm207.566406 324.078125-68.253906 11.777344c7.8125-28.652344 12.03125-58.164063 12.558594-87.855469h71.929687c-.902343 26.117188-6.398437 51.871094-16.234375 76.078125zm-431.367187-76.078125h71.929687c.527344 29.691406 4.746094 59.203125 12.558594 87.855469l-68.253906-11.777344c-9.835938-24.207031-15.332032-49.960937-16.234375-76.078125zm16.234375-92.078125 68.253906-11.777344c-7.8125 28.652344-12.03125 58.164063-12.558594 87.855469h-71.929687c.902343-26.117188 6.398437-51.871094 16.234375-76.078125zm215.566406-27.472656c28.746094.367187 57.421875 2.984375 85.761719 7.832031l28.238281 4.871094c8.675781 29.523437 13.34375 60.078125 13.878906 90.847656h-127.878906zm88.488281-7.9375c-29.238281-4.996094-58.828125-7.695313-88.488281-8.0625v-96c45.863281 4.40625 85.703125 46.398437 108.28125 107.511719zm-104.488281-8.0625c-29.660156.367187-59.242188 3.066406-88.480469 8.0625l-19.800781 3.425781c22.578125-61.128906 62.417969-103.136719 108.28125-107.523438zm-85.753906 23.832031c28.335937-4.847656 57.007812-7.464844 85.753906-7.832031v103.550781h-127.878906c.535156-30.769531 5.203125-61.324219 13.878906-90.847656zm-42.125 111.71875h127.878906v103.550781c-28.746094-.367187-57.421875-2.984375-85.761719-7.832031l-28.238281-4.871094c-8.675781-29.523437-13.34375-60.078125-13.878906-90.847656zm39.390625 111.488281c29.238281 5.003907 58.824219 7.714844 88.488281 8.105469v96c-45.863281-4.410156-85.703125-46.402344-108.28125-107.515625zm104.488281 8.105469c29.660156-.390625 59.242188-3.101562 88.480469-8.105469l19.800781-3.425781c-22.578125 61.128906-62.417969 103.136719-108.28125 107.523438zm85.753906-23.875c-28.335937 4.847656-57.007812 7.464844-85.753906 7.832031v-103.550781h127.878906c-.535156 30.769531-5.203125 61.324219-13.878906 90.847656zm58.117188-111.71875c-.527344-29.691406-4.746094-59.203125-12.558594-87.855469l68.253906 11.777344c9.835938 24.207031 15.332032 49.960937 16.234375 76.078125zm47.601562-93.710938-65.425781-11.289062c-11.761719-38.371094-33.765625-72.808594-63.648437-99.601562 55.878906 18.648437 102.21875 58.457031 129.074218 110.890624zm-269.871094-110.890624c-29.882812 26.792968-51.886718 61.230468-63.648437 99.601562l-65.425781 11.289062c26.855468-52.433593 73.195312-92.242187 129.074218-110.890624zm-129.074218 314.3125 65.425781 11.289062c11.761719 38.371094 33.765625 72.808594 63.648437 99.601562-55.878906-18.648437-102.21875-58.457031-129.074218-110.890624zm269.871094 110.890624c29.882812-26.792968 51.886718-61.230468 63.648437-99.601562l65.425781-11.289062c-26.855468 52.433593-73.195312 92.242187-129.074218 110.890624zm0 0" /></svg></span></div><div className="MenuContent Menu__menuContent__3VW0B Menu__localeLabelTopbar__36KWL locale-currency-container"><div className="Menu__menuContentArrowBelow__2RPsL" style={{left: '235px'}} />
<div className="Menu__menuContentArrowTop__2cSiD" style={{left: '235px'}} /><div className="Menu__menuScroll__2gWsw"><div className="Menu__localeContainerHeading__Y0QZX">Languages</div><div><div className="MenuItem MenuItem__menuitem__3_-l_  "><span className="MenuItem__activeIndicator__2AB6n active-locale-indicator" style={{backgroundColor: 'rgb(215, 155, 40)'}} /><a className="MenuItem_link LocaleCurrency MenuItem__menuitemLink__2Eedg  undefined activeMenuItem" href="javascript:void(0)" data-locale="fr" data-currency="nil">Français</a></div><div className="MenuItem MenuItem__menuitem__3_-l_  "><a className="MenuItem_link LocaleCurrency MenuItem__menuitemLink__2Eedg  undefined deactiveMenuItem" href="javascript:void(0)" data-locale="en" data-currency="nil">English</a></div></div>{/* react-text: 130 */}{/* /react-text */}</div><a href="/change_locale?locale=fr&currency=XAF&redirect_uri=" className="Menu__updateLocalebutton__LNljI update-locale-button-bs" style={{backgroundColor: 'rgb(215, 155, 40)'}}>Save</a></div></div></div></div>

           

    )
}
export default NavBarAccueil