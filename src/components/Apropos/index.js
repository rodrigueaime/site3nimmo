import React from 'react';
import '../../componentsCss/Apropos.css';

const Apropos = () =>{

    return(

        <article className="page-content" style={{}}>
        <div className="wrapper">
          <div className="flash-notifications">
            
          </div>
          <div className="left-navi">
            <a className="selected left-navi-link" href="/fr/infos/about" title="A propos">
              <div className="left-navi-links-modern">
                <div className="icon-info-sign left-navi-link-icon" />
                <div className="left-navi-link-text">A propos</div>
              </div>
            </a>
            <a className="left-navi-link" href="/fr/infos/how_to_use" title="Comment ça marche">
              <div className="left-navi-links-modern">
                <div className="icon-book left-navi-link-icon" />
                <div className="left-navi-link-text">Comment ça marche</div>
              </div>
            </a>
            <a className="left-navi-link" href="/fr/infos/privacy" title="Politique de Confidentialité">
              <div className="left-navi-links-modern">
                <div className="icon-lock left-navi-link-icon" />
                <div className="left-navi-link-text">Politique de Confidentialité</div>
              </div>
            </a>
            <a className="left-navi-link" href="/fr/infos/terms" title="Conditions d'utilisation">
              <div className="left-navi-links-modern">
                <div className="icon-file-alt left-navi-link-icon" />
                <div className="left-navi-link-text">Conditions d'utilisation</div>
              </div>
            </a>
          </div>
          <div className="left-navi-dropdown toggle with-borders hidden-tablet" data-toggle=".left-navi-menu">
            <div className="icon-info-sign icon-with-text" />
            <div className="text-with-icon">A propos</div>
            <i className="dropdown-icon" />
          </div>
          <div className="left-navi-menu toggle-menu hidden">
            <div className="menu-text">Menu</div>
            <a href="/fr/infos/about" id="about_left_navi_link" title="A propos">
              <div className="icon-info-sign icon-with-text" />
              <div className="text-with-icon">A propos</div>
            </a>
            <a href="/fr/infos/how_to_use" id="how_to_use_left_navi_link" title="Comment ça marche">
              <div className="icon-book icon-with-text" />
              <div className="text-with-icon">Comment ça marche</div>
            </a>
            <a href="/fr/infos/privacy" id="privacy_left_navi_link" title="Politique de Confidentialité">
              <div className="icon-lock icon-with-text" />
              <div className="text-with-icon">Politique de Confidentialité</div>
            </a>
            <a href="/fr/infos/terms" id="terms_left_navi_link" title="Conditions d'utilisation">
              <div className="icon-file-alt icon-with-text" />
              <div className="text-with-icon">Conditions d'utilisation</div>
            </a>
          </div>
          <div className="left-navi-section about-section">
            <div data-mercury="full" id="about_page_content">
              <h2><br /></h2>
              <h2>
                <font style={{verticalAlign: 'inherit'}}>
                  <font style={{verticalAlign: 'inherit'}}>
                    <font style={{verticalAlign: 'inherit'}}>
                      <font style={{verticalAlign: 'inherit'}}>
                        <font style={{verticalAlign: 'inherit'}}><font style={{verticalAlign: 'inherit'}}>3N Immo est une agence immobilière digitale basée à Douala.</font></font>
                      </font>
                    </font>
                  </font>
                </font>
              </h2>
              <div><br /></div>
              <div>
                <font style={{verticalAlign: 'inherit'}}>
                  <font style={{verticalAlign: 'inherit'}}>
                    <font style={{verticalAlign: 'inherit'}}>
                      <font style={{verticalAlign: 'inherit'}}>
                        <font style={{verticalAlign: 'inherit'}}><font style={{verticalAlign: 'inherit'}}>Sur notre site, vous retrouverez des biens de prestige à louer (meublé et non meublé) ou disponible à l'achat.</font></font>
                      </font>
                    </font>
                  </font>
                </font>
              </div>
              <div><br /></div>
              <div>
                <font style={{verticalAlign: 'inherit'}}>
                  <font style={{verticalAlign: 'inherit'}}>
                    <font style={{verticalAlign: 'inherit'}}>
                      <font style={{verticalAlign: 'inherit'}}>
                        <font style={{verticalAlign: 'inherit'}}><font style={{verticalAlign: 'inherit'}}>Si vous pouvez bien vouloir vendre, contactez-nous au... ou par mail sur contact@3n-immo.com</font></font>
                      </font>
                    </font>
                  </font>
                </font>
              </div>
            </div>
          </div>
        </div>
      </article>

    )
}
export default Apropos