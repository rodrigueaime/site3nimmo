import React from 'react';
import '../../componentsCss/Footer.css';

const Footer = () => {

    return (

        <footer className="footer__container--logo">
        <div className="footer__content">
          <div className="footer__links-container--center">
            <div className="footer__logo-container">
              <div className="footer__logo-image">
                <img src="https://yelodotred.s3-us-west-2.amazonaws.com/images/communities/wide_logos/8248/header_highres/Inner_logo_%2818%29.png?1616191485" alt="Inner logo %2818%29" />
              </div>
            </div>
            <ul className="footer__link-list" />
            <div className="footer__social-media">
              <a className="footer__social-media-link" href="https://www.facebook.com/3nimmo" rel="noreferrer" target="_blank">
                <svg className="footer__social-media-icon footer__facebook-icon" version="1.1" viewBox="0 0 12 22" xmlnsXlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                  <path d="M7.78896556,22 L7.78896556,11.9648254 L11.3270218,11.9648254 L11.8567155,8.05386965 L7.78896556,8.05386965 L7.78896556,5.55689722 C7.78896556,4.42458426 8.11918279,3.65297638 9.8247272,3.65297638 L12,3.65200544 L12,0.15408855 C11.623687,0.106512375 10.3324948,0 8.83030221,0 C5.69405446,0 3.54703063,1.82255587 3.54703063,5.16968541 L3.54703063,8.05386965 L0,8.05386965 L0,11.9648254 L3.54703063,11.9648254 L3.54703063,22 L7.78896556,22 Z" fillRule="evenodd" id="Facebook" stroke="none" />
                </svg>
              </a>
              <a className="footer__social-media-link" href="https://www.instagram/com/3nimmo" rel="noreferrer" target="_blank">
                <svg className="footer__social-media-icon footer__instagram-icon" viewBox="397 291 22 21" xmlns="http://www.w3.org/2000/svg">
                  <path d="M411.714 301.43c0 1.887-1.54 3.427-3.428 3.427-1.89 0-3.43-1.54-3.43-3.428 0-1.89 1.54-3.43 3.43-3.43 1.888 0 3.428 1.54 3.428 3.43zm1.85 0c0-2.92-2.36-5.278-5.278-5.278-2.92 0-5.277 2.357-5.277 5.277 0 2.918 2.356 5.275 5.276 5.275s5.277-2.357 5.277-5.276zm1.445-5.493c0-.683-.55-1.232-1.233-1.232s-1.232.55-1.232 1.232c0 .684.55 1.233 1.232 1.233.683 0 1.232-.55 1.232-1.233zm-6.724-2.946c1.5 0 4.714-.12 6.067.416.468.188.817.415 1.178.777.363.362.59.71.778 1.18.536 1.35.415 4.566.415 6.066s.12 4.713-.415 6.066c-.187.468-.415.816-.777 1.178-.36.362-.71.59-1.177.777-1.353.537-4.567.416-6.067.416s-4.715.12-6.067-.415c-.47-.187-.818-.414-1.18-.776-.36-.362-.59-.71-.777-1.178-.535-1.353-.415-4.567-.415-6.067s-.12-4.716.415-6.068c.188-.47.416-.817.777-1.18.362-.36.71-.588 1.18-.776 1.35-.535 4.566-.415 6.066-.415zm10.285 8.44c0-1.42.015-2.827-.066-4.247-.08-1.647-.455-3.107-1.66-4.312-1.206-1.205-2.665-1.58-4.313-1.66-1.418-.08-2.825-.067-4.244-.067-1.42 0-2.826-.014-4.246.067-1.647.08-3.107.455-4.312 1.66-1.206 1.206-1.58 2.666-1.66 4.313-.08 1.42-.068 2.826-.068 4.246 0 1.418-.013 2.824.067 4.244.08 1.647.455 3.107 1.66 4.313 1.206 1.205 2.666 1.58 4.313 1.66 1.42.08 2.826.067 4.246.067 1.42 0 2.826.014 4.245-.067 1.65-.08 3.108-.455 4.314-1.66 1.205-1.206 1.58-2.666 1.66-4.313.08-1.42.067-2.826.067-4.245z" fillRule="evenodd" id="Instagram" stroke="none" />
                </svg>
              </a>
              <br />
            </div>
          </div>
          <hr className="footer__separator" />
          <div className="app-link-container">
            <div className="app-links">
              <div className="download-text footer__link">
                TÉLÉCHARGEZ NOTRE APP
              </div>
              <div className="download-img">
                <a className="android-img" href="https://play.google.com/store/apps/3nimmo">
                  <img src="https://www.3n-immo.com/assets/play-store-03b34849f047255796e6d10c0323ac555aac5917444bcf3ef60bb6777c7cabea.png" alt="Play store" />
                </a>
                <a className="android-img" href="https://apps.apple.com/3nimmo">
                  <img src="https://www.3n-immo.com/assets/app-store-baa4dc98309656c673f42bb953ffea22b8e3b2390a16f07bb25a2dc313c9e15d.png" alt="App store" />
                </a>
              </div>
            </div>
          </div>
          <div className="footer__copyrights--markdown">
            <p>3N IMMO - All Rights Reserved - 2021</p>
          </div>
        </div>
      </footer>

    )
}

export default Footer