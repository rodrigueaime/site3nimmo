import React from 'react';

const DemandeRenseignement = () =>{

    return(

        <div>
        <div className="modal tourModal">
          <div className="modal-overlay modal-toggle" />
          <div className="modal-wrapper modal-transition" style={{textAlign: 'center'}}>
            <div className="modal-header">
              <h2 className="modal-heading" style={{color: '#d79b28'}}>
                Greetings!!!
              </h2>
            </div>
            <div className="modal-body">
              <div className="modal-content">
                <p>
                  Welcome to
                  <span style={{fontWeight: 'bold'}}>
                    3N Immo
                  </span>
                  , Want to take a tour?
                </p>
                <img src="https://image.freepik.com/free-vector/navigation-concept-illustration_114360-956.jpg" style={{width: '200px'}} />
                <div className="modal-btns">
                  <button className="tour">
                    Take me to the tour
                  </button>
                  <button className="modal-toggle">
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="fab-toggler" id="chat-widgets">
          <div id="fugu-widget" style={{backgroundColor: '#d79b28'}}>
            <div id="unread-count" />
            <img src="https://www.3n-immo.com/assets/chat-icon-83f1ce3d6db6254adde99a5330ca2eeeb4ef8d069a85373941727f34c299a509.svg" alt="Chat icon" />
          </div>
          <div id="support-widget">
            <div className="fugu-text">Support</div>
            <div className="fugu-icon" style={{backgroundColor: '#d79b28'}}>
              <img src="https://www.3n-immo.com/assets/chat-icon-83f1ce3d6db6254adde99a5330ca2eeeb4ef8d069a85373941727f34c299a509.svg" alt="Chat icon" />
            </div>
          </div>
          <div id="agent-widget">
            <div className="fugu-text">Chat Widget</div>
            <div className="fugu-icon" style={{backgroundColor: '#d79b28'}}>
              <img src="https://www.3n-immo.com/assets/chat-icon-83f1ce3d6db6254adde99a5330ca2eeeb4ef8d069a85373941727f34c299a509.svg" alt="Chat icon" />
            </div>
          </div>
        </div>
        <div id="sidewinder-wrapper">
          <noscript>
            &lt;div class="noscript-padding"&gt;&lt;/div&gt;
          </noscript>
          <div id="OnboardingTopBar-react-component-b8ccd3b1-6ba8-4dc4-ae1e-9920f1afa628">
            <div className="OnboardingTopBar__topbarContainer__sUy7z" data-reactroot data-reactid={1} data-react-checksum={-551466948}>
              <div className="OnboardingTopBar__topbar__ZHXM7" data-reactid={2}>
                <a className="OnboardingTopBar__progressLabel__9Us73" href="/fr/admin/getting_started_guide" data-reactid={3}>
                  {/* react-text: 4 */}Avancement{/* /react-text */}
                  <span className="OnboardingTopBar__progressLabelPercentage__3zatt" data-reactid={5}>85 %</span>
                </a>
                <div className="OnboardingTopBar__progressBarBackground__lPrWa" data-reactid={6}><div className="OnboardingTopBar__progressBar__1MH4x" style={{width: '85.71428571428571%'}} data-reactid={7} /></div>
                <div className="OnboardingTopBar__nextContainer__2NDuk" data-reactid={8}>
                  <div className="OnboardingTopBar__nextLabel__3Apk5" data-reactid={9}>Prochaine étape</div>
                  <a href="/fr/admin/getting_started_guide/payment" className="OnboardingTopBar__nextButton__3al06" data-reactid={10}><span data-reactid={11}>Configurer les paiements</span></a>
                </div>
              </div>
            </div>
          </div>
          <div id="topbar-container">
            <div className="Topbar Topbar__topbar__1ODH7" data-reactroot data-reactid={1} data-react-checksum={-785977952}>
              <div className="Topbar__topbarMobileMenu__2K-8c MenuMobile MenuMobile__menuMobile___Ivzx Topbar__topbarMobileMenu__2K-8c" tabIndex={0} data-reactid={2}>
                <div style={{backgroundColor: '#d79b28'}} className="MenuMobile_overlay MenuMobile__overlay__2sZqq" data-reactid={3} />
                <div className="MenuLabelMobile MenuMobile__menuLabelMobile__3uXwZ" id="go-back-btn" data-reactid={4}>
                  <span data-reactid={5}>
                    {/*?xml version="1.0" encoding="iso-8859-1"?*/}
                    {/* Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  */}
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="18px" height="12px" viewBox="0 0 370.814 370.814" style={{enableBackground: 'new 0 0 370.814 370.814'}} xmlSpace="preserve">
                      <g>
                        <g>
                          <polygon points="292.92,24.848 268.781,0 77.895,185.401 268.781,370.814 292.92,345.961 127.638,185.401 		" />
                        </g>
                      </g>
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                    </svg>
                  </span>
                </div>
                <div className="OffScreenMenu MenuMobile__offScreenMenu__2fKaS" data-reactid={6}>
                  <div className="OffScreenMenu_scrollpane MenuMobile__scrollPane__2VvXd" data-reactid={7}>
                    <div className="OffScreenMenu_header _sideCustAvatar MenuMobile__offScreenHeader__KnANh" data-reactid={8}>
                      <div className="MenuMobile__avatarSpacer__2zKOW" data-reactid={9} style={{backgroundColor: 'rgb(215, 155, 40)', borderRadius: '50%', boxShadow: 'rgba(0, 0, 0, 0.2) 0px 2px 5px'}}>
                        <a href="/fr/eddy" className="Avatar__link__RlpRO" data-reactid={10}><div className="Avatar Avatar__textAvatar__4oTir" style={{height: '44px', width: '44px'}} title="Eddy Harold D" data-reactid={11}>ED</div></a>
                      </div>
                      <a className="MenuMobile__offScreenHeaderNewListingButton__3ssaA AddNewListingButton AddNewListingButton__button__2H8yh" href="/fr/listings/new" title="Déposer une annonce" data-reactid={12}>
                        <span className="AddNewListingButton__backgroundContainer__4Nn3Z AddNewListingButton_background" style={{backgroundColor: '#d79b28'}} data-reactid={13} />
                        <span className="AddNewListingButton__mobile__2JhHF AddNewListingButton_mobile" style={{color: '#d79b28'}} data-reactid={14}>+ Déposer une annonce</span>
                        <span className="AddNewListingButton__desktop__17Luf AddNewListingButton_desktop" data-reactid={15}>+ Déposer une annonce</span>
                      </a>
                    </div>
                    <div className="OffScreenMenu_main MenuMobile__offScreenMain__1ruzb" data-reactid={16}>
                      <div className="MenuSection MenuMobile__menuSection__gD-As" data-reactid={17}>
                        <div className="MenuSection_title MenuMobile__menuSectionTitle__2HlQg" data-reactid={18}>Menu</div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={19}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="https://www.3n-immo.com" style={{color: '#d79b28'}} data-reactid={20}>Accueil</a>
                        </div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={21}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/infos/about" style={{color: '#d79b28'}} data-reactid={22}>A propos</a>
                        </div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={23}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/user_feedbacks/new" style={{color: '#d79b28'}} data-reactid={24}>Nous contacter</a>
                        </div>
                      </div>
                      <div className="MenuSection MenuMobile__menuSection__gD-As" data-reactid={25}>
                        <div className="MenuSection_title MenuMobile__menuSectionTitle__2HlQg" data-reactid={26}>Utilisateur</div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={27}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/admin" style={{color: '#d79b28'}} data-reactid={28}>Panneau d'administration</a>
                        </div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={29}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/eddy/inbox" style={{color: '#d79b28'}} data-reactid={30}>
                            {/* react-text: 31 */}Messagerie{/* /react-text */}
                            <div className="NotificationBadge__notificationBadge__he8-3 Topbar__notificationBadge__3Ghei" data-reactid={32}>
                              <span className="NotificationBadge__notificationBadgeCount__2upgE Topbar__notificationBadgeCount__tOt-2" data-reactid={33}>2</span>
                            </div>
                          </a>
                        </div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={34}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/eddy" style={{color: '#d79b28'}} data-reactid={35}>Profil</a>
                        </div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={36}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/eddy?show_closed=1" style={{color: '#d79b28'}} data-reactid={37}>Mes annonces</a>
                        </div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={38}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/eddy/settings" style={{color: '#d79b28'}} data-reactid={39}>Paramètres</a>
                        </div>
                        <div className="MenuItem MenuItem__menuitem__3_-l_ MenuMobile__menuSectionMenuItem__2yMLC" data-reactid={40}>
                          <a className="MenuItem_link MenuItem__menuitemLink__2Eedg MenuMobile__menuSectionMenuItemLink__GfyW6" href="/fr/logout" style={{color: '#d79b28'}} data-reactid={41}>Déconnexion</a>
                        </div>
                      </div>
                    </div>
                    <div data-reactid={42}>
                      <div data-reactid={43}>
                        <div className="OffScreenMenu_footer MenuMobile__offScreenFooter__3sjuV" data-reactid={44}>
                          <div className="MobileMenu_languages MenuMobile__languages__1Ekmy" data-reactid={45}>
                            <div className="MenuSection_title MenuMobile__menuSectionTitle__2HlQg" data-reactid={46}>
                              <span className="MenuMobile__menuSectionIcon__8bMU1" data-reactid={47}>
                                <svg width={16} height={16} viewBox="24 23 16 16" xmlns="http://www.w3.org/2000/svg">
                                  <g fill="none" fillRule="evenodd" stroke="#B2B2B2">
                                    <path d="M38.708 30.975a6.73 6.73 0 0 1-6.727 6.733c-3.715 0-6.69-3.135-6.69-6.854 0-3.621 2.841-6.412 6.42-6.556a6.67 6.67 0 0 1 6.997 6.677zM31.711 24.297c-3.5 3.792-3.5 8.739 0 13.405M32.295 24.298c3.5 3.791 3.5 8.736 0 13.403M26.166 34.195H37.87M26.403 27.195h11.12M25.292 30.695h13.38" />
                                  </g>
                                </svg>
                              </span>
                              {/* react-text: 48 */}Langue{/* /react-text */}
                            </div>
                            <div className="LanguagesMobile_languageList MenuMobile__languageList__DS1xm" data-reactid={49}>
                              <div className="LanguagesMobile_languageLink MenuMobile__languageLink__1XsC3" data-reactid={50}>
                                <a className="Link__link__3pNRT MenuItem_link LocaleCurrencyMobile active-locale-indicator activeMenuItem" href="javascript:void(0)" style={{color: '#4a4a4a'}} data-locale="fr" data-currency="nil" data-reactid={51}>
                                  Français
                                </a>
                              </div>
                              <div className="LanguagesMobile_languageLink MenuMobile__languageLink__1XsC3" data-reactid={52}>
                                <a className="Link__link__3pNRT MenuItem_link LocaleCurrencyMobile active-locale-indicator deactiveMenuItem" href="javascript:void(0)" style={{color: '#d79b28'}} data-locale="en" data-currency="nil" data-reactid={53}>
                                  English
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <a href style={{backgroundColor: '#d79b28'}} className="MenuMobile__updateLocalebutton__jetyV update-locale-button-sm" data-reactid={54}>Save</a>
                    </div>
                  </div>
                </div>
              </div>
              <a className="Logo__logoNewTheme__ggzOm Logo Topbar__topbarLogo__3IOxU Topbar__topbarLogoPadding__1Pt44 Logo__logo__3sGgU" href="https://www.3n-immo.com" style={{color: '#d79b28'}} data-reactid={55}>
                <img src="https://yelodotred.s3-us-west-2.amazonaws.com/images/communities/wide_logos/8248/header/Inner_logo_%2818%29.png?1616191485" alt="3N Immo" className="Logo__newlogoImage__q4g-U Logo__logoImage__3oOkB" srcSet="https://yelodotred.s3-us-west-2.amazonaws.com/images/communities/wide_logos/8248/header_highres/Inner_logo_%2818%29.png?1616191485 2x" data-reactid={56} />
                <span className="logo-border Logo__logoBorder__3fyP9" data-reactid={57} />
              </a>
              <div className="Topbar__topbarMediumSpacer__29yXk" data-reactid={58} />
              <div className="SearchBar__root__2hIPj SearchBar__rootNewTheme__3tTg9" data-reactid={59}>
                <button className="SearchBar__mobileToggle__3pjye" data-reactid={60}>
                  <div data-reactid={61}>
                    <svg width={17} height={17} viewBox="336 14 17 17" xmlns="http://www.w3.org/2000/svg">
                      <g opacity=".7" fill="none" fillRule="evenodd" transform="matrix(-1 0 0 1 352 15)" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5">
                        <path d="M11 11l3.494 3.494" />
                        <circle cx={6} cy={6} r={6} />
                      </g>
                    </svg>
                  </div>
                  <span className="SearchBar__mobileToggleArrow__25aBI" style={{borderBottomColor: 'transparent'}} data-reactid={62} />
                </button>
                <form style={{backgroundColor: 'transparent'}} className="SearchBar__form__27PQI SearchBar__newThemeForm__2d6LK" data-reactid={63}>
                  <input type="search" className="SearchBar__keywordInput__2HTav" placeholder="Villa, appartement, bureau ou terrain,Vehicule ?" data-reactid={64} />
                  <input type="search" id="searchBar__location" className="SearchBar__locationInput__3g__8" placeholder="Où ?" autoComplete="off" data-reactid={65} />
                  <div className="SearchBar__topbarAutocompleteField__36wRN" id="topbar-autocomplete-field-container" data-reactid={66} />
                  <button type="submit" className="SearchBar__searchButtonNewTheme__2G2Ui SearchBar__searchButton__1Ck2b" style={{backgroundColor: 'transparent'}} data-reactid={67}>
                    <svg width={17} height={17} viewBox="336 14 17 17" xmlns="http://www.w3.org/2000/svg">
                      <g opacity=".7" fill="none" fillRule="evenodd" transform="matrix(-1 0 0 1 352 15)" strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5">
                        <path d="M11 11l3.494 3.494" />
                        <circle cx={6} cy={6} r={6} />
                      </g>
                    </svg>
                  </button>
                  <div className="searchCloseBtn SearchBar__searchCloseBtn__3w7qs" style={{display: 'none'}} data-reactid={68}>X</div>
                  <span className="SearchBar__focusContainer__2uI0-" style={{display: 'none'}} data-reactid={69} />
                </form>
              </div>
              <div className="Topbar__topbarMenuSpacer__gcKVN" data-reactid={70}>
                <div className="MenuPriority MenuPriority__menuPriority__3y3Pu MenuPriority__noPriorityLinks__2I9oP" data-reactid={71}>
                  <div className="MenuPriority__priorityLinks__XgHdH" style={{position: 'absolute', top: '-2000px', left: '-2000px', width: '100%'}} data-reactid={72} />
                  <div className="MenuPriority__hiddenLinks__1LcCU Menu Menu__menu__1nYnK" tabIndex={0} data-reactid={75}>
                    <div className="MenuLabel Menu__menuLabel__17fat" data-reactid={76}>
                      <span className="Menu__menuLabelIcon__3gpCG" data-reactid={77}>
                        <svg width={16} height={10} viewBox="18 19 18 12" xmlns="http://www.w3.org/2000/svg">
                          <g fill="#34495E" fillRule="evenodd" transform="translate(18 19)">
                            <rect width={18} height={2} rx={1} />
                            <rect y={5} width={18} height={2} rx={1} />
                            <rect y={10} width={18} height={2} rx={1} />
                          </g>
                        </svg>
                      </span>
                      {/* react-text: 78 */}Menu{/* /react-text */}
                    </div>
                    <div className="MenuContent Menu__menuContent__3VW0B Menu__newThemeMenu__2hlvl" data-reactid={79}>
                      <div className="Menu__menuContentArrowBelow__2RPsL" style={{left: '235px'}} data-reactid={80} />
                      <div className="Menu__menuContentArrowTop__2cSiD" style={{left: '235px'}} data-reactid={81} />
                      <div className="Menu__menuScroll__2gWsw" data-reactid={82}>
                        {/* react-text: 83 */}{/* /react-text */}
                        <div data-reactid={84}>
                          <div className="MenuItem MenuItem__menuitem__3_-l_" data-reactid={85}><a className="MenuItem_link MenuItem__menuitemLink__2Eedg" href="https://www.3n-immo.com" data-reactid={86}>Accueil</a></div>
                          <div className="MenuItem MenuItem__menuitem__3_-l_" data-reactid={87}><a className="MenuItem_link MenuItem__menuitemLink__2Eedg" href="/fr/infos/about" data-reactid={88}>A propos</a></div>
                          <div className="MenuItem MenuItem__menuitem__3_-l_" data-reactid={89}><a className="MenuItem_link MenuItem__menuitemLink__2Eedg" href="/fr/user_feedbacks/new" data-reactid={90}>Nous contacter</a></div>
                        </div>
                        {/* react-text: 91 */}{/* /react-text */}
                      </div>
                      {/* react-text: 92 */}{/* /react-text */}
                    </div>
                  </div>
                </div>
              </div>
              <a className="Topbar__topbarListingButton__UlpUn AddNewThemeListingButton AddNewListingButton__themeButton__R-CLU AddNewListingButton__responsiveLayout__1JnL9" href="/fr/listings/new" title="Déposer une annonce" data-reactid={93}>
                <span className="AddNewListingButton__mobile__2JhHF AddNewListingButton_mobile" style={{color: '#d79b28'}} data-reactid={94}>+ Déposer une annonce</span>
                <span className="AddNewListingButton__desktopNewTheme__3D_T2 AddNewListingButton_desktop" style={{color: '#d79b28'}} data-reactid={95}>+ Déposer une annonce</span>
                <span className="logo-border AddNewListingButton__logoBorder__37I2e" data-reactid={96} />
              </a>
              <div className="topbar-messages-container-parent" data-reactid={97}>
                <a className="Messages__messagesHeadingTopbarContainer__ikgns topbar-messages-container" href="/fr/eddy/inbox" data-reactid={98}>
                  <div className="Messages__messagesHeadingTopbar__3aU2u topbar-messages-icon" data-reactid={99}>
                    <svg version="1.1" width="20px" fill="#ffffff" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512" xmlSpace="preserve">
                      <g>
                        <g>
                          <g>
                            <path d="M10.688,95.156C80.958,154.667,204.26,259.365,240.5,292.01c4.865,4.406,10.083,6.646,15.5,6.646
				c5.406,0,10.615-2.219,15.469-6.604c36.271-32.677,159.573-137.385,229.844-196.896c4.375-3.698,5.042-10.198,1.5-14.719
				C494.625,69.99,482.417,64,469.333,64H42.667c-13.083,0-25.292,5.99-33.479,16.438C5.646,84.958,6.313,91.458,10.688,95.156z" />
                            <path d="M505.813,127.406c-3.781-1.76-8.229-1.146-11.375,1.542C416.51,195.01,317.052,279.688,285.76,307.885
				c-17.563,15.854-41.938,15.854-59.542-0.021c-33.354-30.052-145.042-125-208.656-178.917c-3.167-2.688-7.625-3.281-11.375-1.542
				C2.417,129.156,0,132.927,0,137.083v268.25C0,428.865,19.135,448,42.667,448h426.667C492.865,448,512,428.865,512,405.333
				v-268.25C512,132.927,509.583,129.146,505.813,127.406z" />
                          </g>
                        </g>
                      </g>
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                      <g />
                    </svg>
                  </div>
                  <div className="Messages__messagesCountTopbar__1S9BN" data-reactid={100}><span data-reactid={101}>2</span></div>
                </a>
              </div>
              <div tabIndex={0} className="AvatarDropdown__avatarNewTheme__3Smzg AvatarDropdown _custAvatar Topbar__topbarAvatarDropdown__2fBjH AvatarDropdown__avatarDropdown__1Vjt6 AvatarDropdown__hasNotifications__2q9xK" data-reactid={102}>
                <div className="AvatarDropdown__avatarWithNotificationsNew__b0Taz AvatarDropdown__avatarWithNotifications__1TdZT avatar-with-notifications" data-reactid={103} style={{borderRadius: '50%', boxShadow: 'rgba(0, 0, 0, 0.2) 0px 2px 5px'}}>
                  <div className="Avatar Avatar__textAvatar__4oTir" style={{height: '100%', width: '100%'}} title="Eddy Harold D" data-reactid={104}>ED</div>
                </div>
                <div className="AvatarDropdown__avatartArrow__2UgxZ" data-reactid={105} />
                <div className="AvatarDropdown__avatarProfileDropdown__2gYjH" data-reactid={106}>
                  <div className="ProfileDropdown__rootArrowTop__3Vvtx" data-reactid={107} />
                  <div className="ProfileDropdown__rootArrowBelow__3YUUJ" data-reactid={108} />
                  <div className="ProfileDropdown__newProfileBox__1tyto ProfileDropdown__box__T_uM4" data-reactid={109}>
                    <div className="ProfileDropdown__profileActions__34o4h" data-reactid={110}>
                      {/* react-text: 111 */}{/* /react-text */}
                      <a href="/fr/eddy" className="ProfileDropdown__profileAction__1H9nm" data-reactid={112}>
                        <div className="ProfileDropdown__profileActionIconWrapper__1v7N_" data-reactid={113} />
                        <div className="ProfileDropdown__profileActionLabel__2lW8j" data-reactid={114}>Profil</div>
                      </a>
                      <a href="/fr/eddy/settings" className="ProfileDropdown__profileAction__1H9nm" data-reactid={115}>
                        <div className="ProfileDropdown__profileActionIconWrapper__1v7N_" data-reactid={116} />
                        <div className="ProfileDropdown__profileActionLabel__2lW8j" data-reactid={117}>Paramètres</div>
                      </a>
                    </div>
                    <div className="ProfileDropdown__basiclogoutArea__3IY-- ProfileDropdown__logoutArea__38Dcu" data-reactid={118}>
                      <a style={{color: '#d79b28', backgroundColor: '#d79b28'}} href="/fr/admin" className="ProfileDropdown__adminLink__342Ug" data-reactid={119}>Panneau d'administration</a>
                      <a className="ProfileDropdown__logoutLink__15uxw" href="/fr/logout" data-reactid={120}>Déconnexion</a>
                    </div>
                  </div>
                </div>
                <span className="logo-border AvatarDropdown__logoBorder__3ojKE" data-reactid={121} />
              </div>
              <div className="Topbar__topbarMenu__hznJ1 Menu Menu__menu__1nYnK" tabIndex={0} data-reactid={122}>
                <div className="menu__label Menu__menuLabel__17fat Topbar__topbarLanguageMenuLabel__-0QYT" data-reactid={123}>
                  {/* react-text: 124 */}{/* /react-text */}
                  <span className="Menu__menuLabelDropdownIconOpen__3NfNG Menu__menuLabelDropdownIcon__1QkrJ" data-reactid={125}>
                    <svg height="18px" viewBox="0 0 480 480" width="18px" xmlns="http://www.w3.org/2000/svg">
                      <path d="m240 0c-132.546875 0-240 107.453125-240 240s107.453125 240 240 240 240-107.453125 240-240c-.148438-132.484375-107.515625-239.851562-240-240zm207.566406 324.078125-68.253906 11.777344c7.8125-28.652344 12.03125-58.164063 12.558594-87.855469h71.929687c-.902343 26.117188-6.398437 51.871094-16.234375 76.078125zm-431.367187-76.078125h71.929687c.527344 29.691406 4.746094 59.203125 12.558594 87.855469l-68.253906-11.777344c-9.835938-24.207031-15.332032-49.960937-16.234375-76.078125zm16.234375-92.078125 68.253906-11.777344c-7.8125 28.652344-12.03125 58.164063-12.558594 87.855469h-71.929687c.902343-26.117188 6.398437-51.871094 16.234375-76.078125zm215.566406-27.472656c28.746094.367187 57.421875 2.984375 85.761719 7.832031l28.238281 4.871094c8.675781 29.523437 13.34375 60.078125 13.878906 90.847656h-127.878906zm88.488281-7.9375c-29.238281-4.996094-58.828125-7.695313-88.488281-8.0625v-96c45.863281 4.40625 85.703125 46.398437 108.28125 107.511719zm-104.488281-8.0625c-29.660156.367187-59.242188 3.066406-88.480469 8.0625l-19.800781 3.425781c22.578125-61.128906 62.417969-103.136719 108.28125-107.523438zm-85.753906 23.832031c28.335937-4.847656 57.007812-7.464844 85.753906-7.832031v103.550781h-127.878906c.535156-30.769531 5.203125-61.324219 13.878906-90.847656zm-42.125 111.71875h127.878906v103.550781c-28.746094-.367187-57.421875-2.984375-85.761719-7.832031l-28.238281-4.871094c-8.675781-29.523437-13.34375-60.078125-13.878906-90.847656zm39.390625 111.488281c29.238281 5.003907 58.824219 7.714844 88.488281 8.105469v96c-45.863281-4.410156-85.703125-46.402344-108.28125-107.515625zm104.488281 8.105469c29.660156-.390625 59.242188-3.101562 88.480469-8.105469l19.800781-3.425781c-22.578125 61.128906-62.417969 103.136719-108.28125 107.523438zm85.753906-23.875c-28.335937 4.847656-57.007812 7.464844-85.753906 7.832031v-103.550781h127.878906c-.535156 30.769531-5.203125 61.324219-13.878906 90.847656zm58.117188-111.71875c-.527344-29.691406-4.746094-59.203125-12.558594-87.855469l68.253906 11.777344c9.835938 24.207031 15.332032 49.960937 16.234375 76.078125zm47.601562-93.710938-65.425781-11.289062c-11.761719-38.371094-33.765625-72.808594-63.648437-99.601562 55.878906 18.648437 102.21875 58.457031 129.074218 110.890624zm-269.871094-110.890624c-29.882812 26.792968-51.886718 61.230468-63.648437 99.601562l-65.425781 11.289062c26.855468-52.433593 73.195312-92.242187 129.074218-110.890624zm-129.074218 314.3125 65.425781 11.289062c11.761719 38.371094 33.765625 72.808594 63.648437 99.601562-55.878906-18.648437-102.21875-58.457031-129.074218-110.890624zm269.871094 110.890624c29.882812-26.792968 51.886718-61.230468 63.648437-99.601562l65.425781-11.289062c-26.855468 52.433593-73.195312 92.242187-129.074218 110.890624zm0 0" />
                    </svg>
                  </span>
                  <span className="Menu__menuLabelDropdownIconClosed__3ja5m Menu__menuLabelDropdownIcon__1QkrJ" data-reactid={126}>
                    <svg height="18px" viewBox="0 0 480 480" width="18px" xmlns="http://www.w3.org/2000/svg">
                      <path d="m240 0c-132.546875 0-240 107.453125-240 240s107.453125 240 240 240 240-107.453125 240-240c-.148438-132.484375-107.515625-239.851562-240-240zm207.566406 324.078125-68.253906 11.777344c7.8125-28.652344 12.03125-58.164063 12.558594-87.855469h71.929687c-.902343 26.117188-6.398437 51.871094-16.234375 76.078125zm-431.367187-76.078125h71.929687c.527344 29.691406 4.746094 59.203125 12.558594 87.855469l-68.253906-11.777344c-9.835938-24.207031-15.332032-49.960937-16.234375-76.078125zm16.234375-92.078125 68.253906-11.777344c-7.8125 28.652344-12.03125 58.164063-12.558594 87.855469h-71.929687c.902343-26.117188 6.398437-51.871094 16.234375-76.078125zm215.566406-27.472656c28.746094.367187 57.421875 2.984375 85.761719 7.832031l28.238281 4.871094c8.675781 29.523437 13.34375 60.078125 13.878906 90.847656h-127.878906zm88.488281-7.9375c-29.238281-4.996094-58.828125-7.695313-88.488281-8.0625v-96c45.863281 4.40625 85.703125 46.398437 108.28125 107.511719zm-104.488281-8.0625c-29.660156.367187-59.242188 3.066406-88.480469 8.0625l-19.800781 3.425781c22.578125-61.128906 62.417969-103.136719 108.28125-107.523438zm-85.753906 23.832031c28.335937-4.847656 57.007812-7.464844 85.753906-7.832031v103.550781h-127.878906c.535156-30.769531 5.203125-61.324219 13.878906-90.847656zm-42.125 111.71875h127.878906v103.550781c-28.746094-.367187-57.421875-2.984375-85.761719-7.832031l-28.238281-4.871094c-8.675781-29.523437-13.34375-60.078125-13.878906-90.847656zm39.390625 111.488281c29.238281 5.003907 58.824219 7.714844 88.488281 8.105469v96c-45.863281-4.410156-85.703125-46.402344-108.28125-107.515625zm104.488281 8.105469c29.660156-.390625 59.242188-3.101562 88.480469-8.105469l19.800781-3.425781c-22.578125 61.128906-62.417969 103.136719-108.28125 107.523438zm85.753906-23.875c-28.335937 4.847656-57.007812 7.464844-85.753906 7.832031v-103.550781h127.878906c-.535156 30.769531-5.203125 61.324219-13.878906 90.847656zm58.117188-111.71875c-.527344-29.691406-4.746094-59.203125-12.558594-87.855469l68.253906 11.777344c9.835938 24.207031 15.332032 49.960937 16.234375 76.078125zm47.601562-93.710938-65.425781-11.289062c-11.761719-38.371094-33.765625-72.808594-63.648437-99.601562 55.878906 18.648437 102.21875 58.457031 129.074218 110.890624zm-269.871094-110.890624c-29.882812 26.792968-51.886718 61.230468-63.648437 99.601562l-65.425781 11.289062c26.855468-52.433593 73.195312-92.242187 129.074218-110.890624zm-129.074218 314.3125 65.425781 11.289062c11.761719 38.371094 33.765625 72.808594 63.648437 99.601562-55.878906-18.648437-102.21875-58.457031-129.074218-110.890624zm269.871094 110.890624c29.882812-26.792968 51.886718-61.230468 63.648437-99.601562l65.425781-11.289062c-26.855468 52.433593-73.195312 92.242187-129.074218 110.890624zm0 0" />
                    </svg>
                  </span>
                </div>
                <div className="MenuContent Menu__menuContent__3VW0B Menu__localeLabelTopbar__36KWL locale-currency-container" data-reactid={127}>
                  <div className="Menu__menuContentArrowBelow__2RPsL" style={{left: '235px'}} data-reactid={128} />
                  <div className="Menu__menuContentArrowTop__2cSiD" style={{left: '235px'}} data-reactid={129} />
                  <div className="Menu__menuScroll__2gWsw" data-reactid={130}>
                    <div className="Menu__localeContainerHeading__Y0QZX" data-reactid={131}>Languages</div>
                    <div data-reactid={132}>
                      <div className="MenuItem MenuItem__menuitem__3_-l_" data-reactid={133}>
                        <span className="MenuItem__activeIndicator__2AB6n active-locale-indicator" style={{backgroundColor: '#d79b28'}} data-reactid={134} />
                        <a className="MenuItem_link LocaleCurrency MenuItem__menuitemLink__2Eedg undefined activeMenuItem" href="javascript:void(0)" data-locale="fr" data-currency="nil" data-reactid={135}>Français</a>
                      </div>
                      <div className="MenuItem MenuItem__menuitem__3_-l_" data-reactid={136}>
                        <a className="MenuItem_link LocaleCurrency MenuItem__menuitemLink__2Eedg undefined deactiveMenuItem" href="javascript:void(0)" data-locale="en" data-currency="nil" data-reactid={137}>English</a>
                      </div>
                    </div>
                    {/* react-text: 138 */}{/* /react-text */}
                  </div>
                  <a href="/change_locale?locale=fr&currency=XAF&redirect_uri=eddy/inbox?type=enquiry" className="Menu__updateLocalebutton__LNljI update-locale-button-bs" style={{backgroundColor: '#d79b28'}} data-reactid={139}>
                    Save
                  </a>
                </div>
              </div>
            </div>
          </div>
          <section className="marketplace-lander" />
          <article className="page-content" style={{}}>
            <div className="wrapper">
              <div className="conversation-types">
                <div className="coversation-types-tab">
                  <a className="conversation-type-link" href=" /fr/eddy/inbox/?type=all-conversations" value="{&quot;type&quot;:&quot;Commandes&quot;,&quot;href&quot;:&quot;/fr/eddy/inbox/?type=all-conversations&quot;}">
                    <h1 className="conversation-type-title">
                      Commandes
                    </h1>
                  </a>
                  <a className="conversation-type-link" href=" /fr/eddy/inbox/?type=enquiry" value="{&quot;type&quot;:&quot;Demande de renseignement&quot;,&quot;href&quot;:&quot;/fr/eddy/inbox/?type=enquiry&quot;}">
                    <h1 className="conversation-type-title selected">
                      Demande de renseignement
                    </h1>
                  </a>
                </div>
              </div>
              <div className="undo-mobile-wrapper-margin" id="conversations">
                <div className="conversation-row conversation-unread row-with-divider without-margin">
                  <div className="col-3">
                    <div className="conversation-details-container">
                      <a href="/fr/kevink">
                        <img className="conversation-avatar" src="https://www.3n-immo.com/assets/profile_image/thumb/missing-2103b8402da301387627aaea4ce27451dedbd0f6aa0cf7850759a0a8b368375e.png" alt="Missing" />
                      </a>
                      <div className="conversation-details">
                        <div className="conversation-other-party-name">
                          <a href="/fr/kevink">Kevin K</a>
                        </div>
                        <div className="conversation-last-message-at">
                          il y a 2 jours
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-9">
                    <a className="conversation-title-link-unread" href="/fr/eddy/transactions/9636">top </a>
                    <div className="conversation-context">Description <a href="/fr/listings/33266">tttccddnj,</a></div>
                  </div>
                </div>
                <div id="pageless-loader" style={{display: 'none', textAlign: 'center', width: '100%'}}>
                  <div className="msg" style={{color: '#e9e9e9', fontSize: '2em'}}>Chargement de la suite des messages</div>
                  <img src="https://s3.amazonaws.com/sharetribe/assets/ajax-loader-grey.gif" alt="loading more results" style={{margin: '10px auto'}} />
                </div>
              </div>
            </div>
          </article>
          <footer className="footer__container--logo">
            <div className="footer__content">
              <div className="footer__links-container--center">
                <div className="footer__logo-container">
                  <div className="footer__logo-image">
                    <img src="https://yelodotred.s3-us-west-2.amazonaws.com/images/communities/wide_logos/8248/header_highres/Inner_logo_%2818%29.png?1616191485" alt="Inner logo %2818%29" />
                  </div>
                </div>
                <div className="footer__social-media">
                  <a className="footer__social-media-link" href="https://www.facebook.com/3nimmo" rel="noreferrer" target="_blank">
                    <svg className="footer__social-media-icon footer__facebook-icon" version="1.1" viewBox="0 0 12 22" xmlnsXlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                      <path d="M7.78896556,22 L7.78896556,11.9648254 L11.3270218,11.9648254 L11.8567155,8.05386965 L7.78896556,8.05386965 L7.78896556,5.55689722 C7.78896556,4.42458426 8.11918279,3.65297638 9.8247272,3.65297638 L12,3.65200544 L12,0.15408855 C11.623687,0.106512375 10.3324948,0 8.83030221,0 C5.69405446,0 3.54703063,1.82255587 3.54703063,5.16968541 L3.54703063,8.05386965 L0,8.05386965 L0,11.9648254 L3.54703063,11.9648254 L3.54703063,22 L7.78896556,22 Z" fillRule="evenodd" id="Facebook" stroke="none" />
                    </svg>
                  </a>
                  <a className="footer__social-media-link" href="https://www.instagram/com/3nimmo" rel="noreferrer" target="_blank">
                    <svg className="footer__social-media-icon footer__instagram-icon" viewBox="397 291 22 21" xmlns="http://www.w3.org/2000/svg">
                      <path d="M411.714 301.43c0 1.887-1.54 3.427-3.428 3.427-1.89 0-3.43-1.54-3.43-3.428 0-1.89 1.54-3.43 3.43-3.43 1.888 0 3.428 1.54 3.428 3.43zm1.85 0c0-2.92-2.36-5.278-5.278-5.278-2.92 0-5.277 2.357-5.277 5.277 0 2.918 2.356 5.275 5.276 5.275s5.277-2.357 5.277-5.276zm1.445-5.493c0-.683-.55-1.232-1.233-1.232s-1.232.55-1.232 1.232c0 .684.55 1.233 1.232 1.233.683 0 1.232-.55 1.232-1.233zm-6.724-2.946c1.5 0 4.714-.12 6.067.416.468.188.817.415 1.178.777.363.362.59.71.778 1.18.536 1.35.415 4.566.415 6.066s.12 4.713-.415 6.066c-.187.468-.415.816-.777 1.178-.36.362-.71.59-1.177.777-1.353.537-4.567.416-6.067.416s-4.715.12-6.067-.415c-.47-.187-.818-.414-1.18-.776-.36-.362-.59-.71-.777-1.178-.535-1.353-.415-4.567-.415-6.067s-.12-4.716.415-6.068c.188-.47.416-.817.777-1.18.362-.36.71-.588 1.18-.776 1.35-.535 4.566-.415 6.066-.415zm10.285 8.44c0-1.42.015-2.827-.066-4.247-.08-1.647-.455-3.107-1.66-4.312-1.206-1.205-2.665-1.58-4.313-1.66-1.418-.08-2.825-.067-4.244-.067-1.42 0-2.826-.014-4.246.067-1.647.08-3.107.455-4.312 1.66-1.206 1.206-1.58 2.666-1.66 4.313-.08 1.42-.068 2.826-.068 4.246 0 1.418-.013 2.824.067 4.244.08 1.647.455 3.107 1.66 4.313 1.206 1.205 2.666 1.58 4.313 1.66 1.42.08 2.826.067 4.246.067 1.42 0 2.826.014 4.245-.067 1.65-.08 3.108-.455 4.314-1.66 1.205-1.206 1.58-2.666 1.66-4.313.08-1.42.067-2.826.067-4.245z" fillRule="evenodd" id="Instagram" stroke="none" />
                    </svg>
                  </a>
                  <br />
                </div>
              </div>
              <hr className="footer__separator" />
              <div className="app-link-container">
                <div className="app-links">
                  <div className="download-text footer__link">
                    TÉLÉCHARGEZ NOTRE APP
                  </div>
                  <div className="download-img">
                    <a className="android-img" href="https://play.google.com/store/apps/3nimmo">
                      <img src="https://www.3n-immo.com/assets/play-store-03b34849f047255796e6d10c0323ac555aac5917444bcf3ef60bb6777c7cabea.png" alt="Play store" />
                    </a>
                    <a className="android-img" href="https://apps.apple.com/3nimmo">
                      <img src="https://www.3n-immo.com/assets/app-store-baa4dc98309656c673f42bb953ffea22b8e3b2390a16f07bb25a2dc313c9e15d.png" alt="App store" />
                    </a>
                  </div>
                </div>
              </div>
              <div className="footer__copyrights--markdown">
                <p>3N IMMO - All Rights Reserved - 2021</p>
              </div>
            </div>
          </footer>
          <div id="fb-root" className="fb_reset">
            <div style={{position: 'absolute', top: '-10000px', width: '0px', height: '0px'}}><div /></div>
          </div>
          <noscript>
            &lt;div class="noscript"&gt;
            &lt;div class="wrapper"&gt;
            &lt;h2&gt;Javascript est désactivé dans votre navigateur&lt;/h2&gt;
            &lt;p&gt;3N Immo ne fonctionne pas correctement sans JavaScript. Essayez d&amp;#39;activer javascript à partir des préférences de votre navigateur puis rechargez cette page.&lt;/p&gt;
            &lt;/div&gt;
            &lt;/div&gt;
          </noscript>
        </div>
        <iframe id="iframe_fuguWidgetContent" allow="microphone; camera; autoplay" allowFullScreen className="collapsed" src="https://widget.hippochat.io/widget/" style={{borderRadius: '6px'}} />
        <iframe id="iframe_fuguWidget" className="collapsed" />
      </div>
    )
}
export default DemandeRenseignement