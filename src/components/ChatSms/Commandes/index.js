import React from 'react';
import '../../../componentsCss/Commandes.css';


const Commandes = () =>{

        return(

            <article className="page-content" style={{}}>
            <div className="wrapper">
              <div className="conversation-types">
                <div className="coversation-types-tab">
                  <a className="conversation-type-link" href=" /fr/eddy/inbox/?type=all-conversations" value="{&quot;type&quot;:&quot;Commandes&quot;,&quot;href&quot;:&quot;/fr/eddy/inbox/?type=all-conversations&quot;}">
                    <h1 className="conversation-type-title selected">
                      Commandes
                    </h1>
                  </a>
                  <a className="conversation-type-link" href=" /fr/eddy/inbox/?type=enquiry" value="{&quot;type&quot;:&quot;Demande de renseignement&quot;,&quot;href&quot;:&quot;/fr/eddy/inbox/?type=enquiry&quot;}">
                    <h1 className="conversation-type-title">
                      Demande de renseignement
                    </h1>
                  </a>
                </div>
              </div>
              <div className="conversation-sub-types">
                <div className="coversation-sub-types-tab">
                  <a className="conversation-type-link" href=" /fr/eddy/inbox/?type=all-conversations" value="{&quot;type&quot;:&quot;Tous&quot;,&quot;href&quot;:&quot;/fr/eddy/inbox/?type=all-conversations&quot;}">
                    <p className="conversation-type-title selected">
                      Tous
                    </p>
                  </a>
                  <a className="conversation-type-link" href=" /fr/eddy/inbox/?type=hostings" value="{&quot;type&quot;:&quot;Hébergement&quot;,&quot;href&quot;:&quot;/fr/eddy/inbox/?type=hostings&quot;}">
                    <p className="conversation-type-title">
                      Hébergement
                    </p>
                  </a>
                  <a className="conversation-type-link" href=" /fr/eddy/inbox/?type=bookings" value="{&quot;type&quot;:&quot;Réservations&quot;,&quot;href&quot;:&quot;/fr/eddy/inbox/?type=bookings&quot;}">
                    <p className="conversation-type-title">
                      Réservations
                    </p>
                  </a>
                </div>
              </div>
              <div className="undo-mobile-wrapper-margin" id="conversations">
                <div className="conversation-row row-with-divider without-margin">
                  <div className="hostings-tag">
                    Hosting
                  </div>
                  <div className="col-3">
                    <div className="conversation-details-container">
                      <a href="/fr/kevink"><img className="conversation-avatar" src="https://www.3n-immo.com/assets/profile_image/thumb/missing-2103b8402da301387627aaea4ce27451dedbd0f6aa0cf7850759a0a8b368375e.png" alt="Missing" /> </a>
                      <div className="conversation-details">
                        <div className="conversation-other-party-name">
                          <a href="/fr/kevink">Kevin K</a>
                        </div>
                        <div className="conversation-last-message-at">
                          il y a 1 jour
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-9">
                    <a className="conversation-title-link" href="/fr/eddy/transactions/9635">test message </a>
                    <div className="conversation-context">Description <a href="/fr/listings/33266">tttccddnj,</a></div>
                  </div>
                </div>
                <div className="conversation-row row-with-divider without-margin">
                  <div className="hostings-tag">
                    Hosting
                  </div>
                  <div className="col-3">
                    <div className="conversation-details-container">
                      <a href="/fr/kevink"><img className="conversation-avatar" src="https://www.3n-immo.com/assets/profile_image/thumb/missing-2103b8402da301387627aaea4ce27451dedbd0f6aa0cf7850759a0a8b368375e.png" alt="Missing" /> </a>
                      <div className="conversation-details">
                        <div className="conversation-other-party-name">
                          <a href="/fr/kevink">Kevin K</a>
                        </div>
                        <div className="conversation-last-message-at">
                          il y a 1 jour
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-9">
                    <a className="conversation-title-link" href="/fr/eddy/transactions/9634">ok </a>
                    <div className="conversation-context">Description <a href="/fr/listings/33233">hhh</a></div>
                  </div>
                </div>
                <div className="conversation-row conversation-unread row-with-divider without-margin">
                  <div className="hostings-tag">
                    Hosting
                  </div>
                  <div className="col-3">
                    <div className="conversation-details-container">
                      <a href="/fr/kevink"><img className="conversation-avatar" src="https://www.3n-immo.com/assets/profile_image/thumb/missing-2103b8402da301387627aaea4ce27451dedbd0f6aa0cf7850759a0a8b368375e.png" alt="Missing" /> </a>
                      <div className="conversation-details">
                        <div className="conversation-other-party-name">
                          <a href="/fr/kevink">Kevin K</a>
                        </div>
                        <div className="conversation-last-message-at">
                          il y a 2 jours
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-9">
                    <a className="conversation-title-link-unread" href="/fr/eddy/transactions/9631">pour l'achat </a>
                    <div className="conversation-context">Description <a href="/fr/listings/33266">tttccddnj,</a></div>
                  </div>
                </div>
                <div id="pageless-loader" style={{display: 'none', textAlign: 'center', width: '100%'}}>
                  <div className="msg" style={{color: '#e9e9e9', fontSize: '2em'}}>Chargement de la suite des messages</div>
                  <img src="https://s3.amazonaws.com/sharetribe/assets/ajax-loader-grey.gif" alt="loading more results" style={{margin: '10px auto'}} />
                </div>
              </div>
            </div>
          </article>

        )

}
export default Commandes