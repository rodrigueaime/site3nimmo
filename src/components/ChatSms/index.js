import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Commandes from './Commandes';
import DemandeRenseignement from './DemandeRenseignement';

const ChatSms = (props) => {

    const { match } = props;

    return (
        <Router>
            <Switch>
                <Route path={`${match.path}/commandes`} component={Commandes} />
                <Route path={`${match.path}/demanderenseignement`} component={DemandeRenseignement} />
            </Switch>
        </Router>
    )
}
export default ChatSms