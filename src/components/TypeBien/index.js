import React from 'react'
import Appartement from './Appartement'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const TypeBien = (props) =>{

    const { match } = props;

    return(


        <Router>
     <Switch> 
     <Route path={`${match.path}/appartement`} component={Appartement} />
     </Switch>
    </Router>

    )
}
export default TypeBien