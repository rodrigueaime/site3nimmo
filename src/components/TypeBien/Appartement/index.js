import React from 'react';
import '../../../componentsCss/Appartement.css';

const Appartement = () =>{
    return(


        <form id="homepage-filters" method="get">
        <input type="hidden" name="category" id="category" defaultValue="appartement" />
        <input type="hidden" name="locale" id="locale" defaultValue="fr" />
        <input type="hidden" name="view" id="view" defaultValue="list" />
        <section className="marketplace-lander">
          <div className="coverimage">
            <figure className="fluidratio" style={{height: '100%'}}>
              <div className="category-banner">
                <a href>
                  <img className="banner-image" src="https://rentals-temp.s3-us-west-2.amazonaws.com/category_banner%2Fimages%2Fcat%20appartement.jpg" />
                </a>
              </div>
            </figure>
          </div>
        </section>
        <article className="no-cover-photo page-content" style={{}}>
          <div className="wrapper">
            <div className="home-toolbar">
              <div className="home-toolbar-show-filters-button-container">
                <div className="home-toolbar-show-filters-button" id="home-toolbar-show-filters">
                  Filtre
                </div>
              </div>
              <div className="filters-enabled home-toolbar-button-group">
                <a className="home-toolbar-button-group-button grid" title="Grille" href="/fr/s?category=appartement&sort_by=4&view=grid">
                  <i className="icon-th-large icon-fix home-button-group-icon" />
                  <span className="home-toolbar-button-text">
                    Grille
                  </span>
                </a>
                <a className="home-toolbar-button-group-button selected list" title="Liste" href="/fr/s?category=appartement&sort_by=4&view=list">
                  <i className="icon-reorder icon-fix home-button-group-icon" />
                  <span className="home-toolbar-button-text">
                    Liste
                  </span>
                </a>
                <a className="home-toolbar-button-group-button map" title="Carte" href="/fr/s?category=appartement&sort_by=4&view=map">
                  <i className="icon-map-marker icon-fix home-button-group-icon" />
                  <span className="home-toolbar-button-text">
                    Carte
                  </span>
                </a>
              </div>
              <div className="home-toolbar-filters home-toolbar-filters-mobile-hidden" id="home-toolbar-filters">
                <div className="toggle-container home-toolbar-toggle-container">
                  <div className="toggle with-borders" data-toggle=".home-toolbar-share-type-menu" id="home_toolbar-select-share-type">
                    <div className="toggle-header-container">
                      <div className="toggle-header">
                        Tous les types d'annonces
                      </div>
                    </div>
                  </div>
                  <div className="hidden home-toolbar-share-type-menu toggle-menu">
                    <a href="/fr/s?category=appartement&sort_by=4&transaction_type=all">Tous les types d'annonces</a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=appartement&sort_by=4&transaction_type=renting-out-without-online-payment">
                      <span className="for-left-border" />
                      Location longue durée (non meublée)
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=appartement&sort_by=4&transaction_type=renting-out-with-online-payment">
                      <span className="for-left-border" />
                      Location court/moyenne durée (meublée)
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=appartement&sort_by=4&transaction_type=vente">
                      <span className="for-left-border" />
                      Vente
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=appartement&sort_by=4&transaction_type=information">
                      <span className="for-left-border" />
                      Information
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=appartement&sort_by=4&transaction_type=vente-2">
                      <span className="for-left-border" />
                      Vente 2
                    </a>
                  </div>
                </div>
                <div className="toggle-container home-toolbar-toggle-container">
                  <div className="toggle with-borders" data-toggle="#home-toolbar-categories-menu">
                    <div className="toggle-header-container">
                      <div className="toggle-header">
                        Appartement
                      </div>
                    </div>
                  </div>
                  <div className="toggle-menu hidden" id="home-toolbar-categories-menu">
                    <a href="/fr/s?category=all&sort_by=4">Toutes les catégories</a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=default-category&sort_by=4">
                      <span className="for-left-border" />
                      Default category
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=appartement&sort_by=4">
                      <span className="for-left-border" />
                      Appartement
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=villa&sort_by=4">
                      <span className="for-left-border" />
                      Villa
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=bureau&sort_by=4">
                      <span className="for-left-border" />
                      Bureau
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=terrain&sort_by=4">
                      <span className="for-left-border" />
                      Terrain
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=vehicule&sort_by=4">
                      <span className="for-left-border" />
                      Vehicule
                    </a>
                    <a className="toggle-menu-subitem" href="/fr/s?category=restaurant&sort_by=4">
                      <span className="for-left-border" />
                      Restaurant
                    </a>
                  </div>
                </div>
                {/* Filters */}
                <div id="filters">
                  <div className="toggle-container home-toolbar-toggle-container price-filter">
                    <div className="toggle with-borders" data-toggle="#home-toolbar-0-menu">
                      <div className="toggle-header-container">
                        <div className="toggle-header">
                          Superficie (m2)
                        </div>
                      </div>
                    </div>
                    <div className="toggle-menu hidden" id="home-toolbar-0-menu">
                      <div className="custom-filter-options">
                        <div className="num-range">
                          <span className="num-range-text">
                            <span>
                              Range
                            </span>
                          </span>
                          <div className="num-min-max">
                            <input className="min-num-limit" id="nf_min_range-slider-7843" max={999999.0} min={0.0} name="nf_min_7843" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,999999.0],[&quot;#nf_min_range-slider-7843&quot;,&quot;#nf_max_range-slider-7843&quot;],'#nf_min_range-slider-7843','#range-slider-7843')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={0} step={1} type="number" />
                            <span className="num-seperator">
                              -
                            </span>
                            <input className="max-num-limit" id="nf_max_range-slider-7843" max={999999.0} min={0.0} name="nf_max_7843" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,999999.0],[&quot;#nf_min_range-slider-7843&quot;,&quot;#nf_max_range-slider-7843&quot;],'#nf_max_range-slider-7843','#range-slider-7843')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={999999} step={1} type="number" />
                          </div>
                        </div>
                        <div className="range-slider noUi-target noUi-ltr noUi-horizontal noUi-background" id="range-slider-7843">
                          <div className="noUi-base">
                            <div className="noUi-origin noUi-connect" data-style="left" style={{left: '0%'}}><div className="noUi-handle noUi-handle-lower" /></div>
                            <div className="noUi-origin noUi-background" data-style="left" style={{left: '100%'}}><div className="noUi-handle noUi-handle-upper" /></div>
                          </div>
                        </div>
                      </div>
                      <div className="filter-action">
                        <button className="clear-filter-button" id="clear-button" onclick="window.ST.clearNumberRangeFilter(`#range-slider-7843`)" type="submit">
                          Effacer
                        </button>
                        <div className="apply-cancel-btn">
                          <button className="cancel-filter-button" onclick="window.ST.cancelNumberRangeFilter(`#range-slider-7843`)" type="button">
                            Annuler
                          </button>
                          <button className="apply-filter-button" type="submit">
                            Appliquer
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="toggle-container home-toolbar-toggle-container price-filter">
                    <div className="toggle with-borders" data-toggle="#home-toolbar-1-menu">
                      <div className="toggle-header-container">
                        <div className="toggle-header">
                          Chambres
                        </div>
                      </div>
                    </div>
                    <div className="toggle-menu hidden" id="home-toolbar-1-menu">
                      <div className="custom-filter-options">
                        <div className="num-range">
                          <span className="num-range-text">
                            <span>
                              Range
                            </span>
                          </span>
                          <div className="num-min-max">
                            <input className="min-num-limit" id="nf_min_range-slider-7870" max={60.0} min={0.0} name="nf_min_7870" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,60.0],[&quot;#nf_min_range-slider-7870&quot;,&quot;#nf_max_range-slider-7870&quot;],'#nf_min_range-slider-7870','#range-slider-7870')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={0} step={1} type="number" />
                            <span className="num-seperator">
                              -
                            </span>
                            <input className="max-num-limit" id="nf_max_range-slider-7870" max={60.0} min={0.0} name="nf_max_7870" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,60.0],[&quot;#nf_min_range-slider-7870&quot;,&quot;#nf_max_range-slider-7870&quot;],'#nf_max_range-slider-7870','#range-slider-7870')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={60} step={1} type="number" />
                          </div>
                        </div>
                        <div className="range-slider noUi-target noUi-ltr noUi-horizontal noUi-background" id="range-slider-7870">
                          <div className="noUi-base">
                            <div className="noUi-origin noUi-connect" data-style="left" style={{left: '0%'}}><div className="noUi-handle noUi-handle-lower" /></div>
                            <div className="noUi-origin noUi-background" data-style="left" style={{left: '100%'}}><div className="noUi-handle noUi-handle-upper" /></div>
                          </div>
                        </div>
                      </div>
                      <div className="filter-action">
                        <button className="clear-filter-button" id="clear-button" onclick="window.ST.clearNumberRangeFilter(`#range-slider-7870`)" type="submit">
                          Effacer
                        </button>
                        <div className="apply-cancel-btn">
                          <button className="cancel-filter-button" onclick="window.ST.cancelNumberRangeFilter(`#range-slider-7870`)" type="button">
                            Annuler
                          </button>
                          <button className="apply-filter-button" type="submit">
                            Appliquer
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="toggle-container home-toolbar-toggle-container price-filter">
                    <div className="toggle with-borders" data-toggle="#home-toolbar-2-menu">
                      <div className="toggle-header-container">
                        <div className="toggle-header">
                          Pièces
                        </div>
                      </div>
                    </div>
                    <div className="toggle-menu hidden" id="home-toolbar-2-menu">
                      <div className="custom-filter-options">
                        <div className="num-range">
                          <span className="num-range-text">
                            <span>
                              Range
                            </span>
                          </span>
                          <div className="num-min-max">
                            <input className="min-num-limit" id="nf_min_range-slider-7871" max={60.0} min={0.0} name="nf_min_7871" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,60.0],[&quot;#nf_min_range-slider-7871&quot;,&quot;#nf_max_range-slider-7871&quot;],'#nf_min_range-slider-7871','#range-slider-7871')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={0} step={1} type="number" />
                            <span className="num-seperator">
                              -
                            </span>
                            <input className="max-num-limit" id="nf_max_range-slider-7871" max={60.0} min={0.0} name="nf_max_7871" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,60.0],[&quot;#nf_min_range-slider-7871&quot;,&quot;#nf_max_range-slider-7871&quot;],'#nf_max_range-slider-7871','#range-slider-7871')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={60} step={1} type="number" />
                          </div>
                        </div>
                        <div className="range-slider noUi-target noUi-ltr noUi-horizontal noUi-background" id="range-slider-7871">
                          <div className="noUi-base">
                            <div className="noUi-origin noUi-connect" data-style="left" style={{left: '0%'}}><div className="noUi-handle noUi-handle-lower" /></div>
                            <div className="noUi-origin noUi-background" data-style="left" style={{left: '100%'}}><div className="noUi-handle noUi-handle-upper" /></div>
                          </div>
                        </div>
                      </div>
                      <div className="filter-action">
                        <button className="clear-filter-button" id="clear-button" onclick="window.ST.clearNumberRangeFilter(`#range-slider-7871`)" type="submit">
                          Effacer
                        </button>
                        <div className="apply-cancel-btn">
                          <button className="cancel-filter-button" onclick="window.ST.cancelNumberRangeFilter(`#range-slider-7871`)" type="button">
                            Annuler
                          </button>
                          <button className="apply-filter-button" type="submit">
                            Appliquer
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="toggle-container home-toolbar-toggle-container price-filter">
                    <div className="toggle with-borders" data-toggle="#home-toolbar-3-menu">
                      <div className="toggle-header-container">
                        <div className="toggle-header">
                          Salle de bain
                        </div>
                      </div>
                    </div>
                    <div className="toggle-menu hidden" id="home-toolbar-3-menu">
                      <div className="custom-filter-options">
                        <div className="num-range">
                          <span className="num-range-text">
                            <span>
                              Range
                            </span>
                          </span>
                          <div className="num-min-max">
                            <input className="min-num-limit" id="nf_min_range-slider-7872" max={60.0} min={0.0} name="nf_min_7872" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,60.0],[&quot;#nf_min_range-slider-7872&quot;,&quot;#nf_max_range-slider-7872&quot;],'#nf_min_range-slider-7872','#range-slider-7872')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={0} step={1} type="number" />
                            <span className="num-seperator">
                              -
                            </span>
                            <input className="max-num-limit" id="nf_max_range-slider-7872" max={60.0} min={0.0} name="nf_max_7872" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,60.0],[&quot;#nf_min_range-slider-7872&quot;,&quot;#nf_max_range-slider-7872&quot;],'#nf_max_range-slider-7872','#range-slider-7872')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={60} step={1} type="number" />
                          </div>
                        </div>
                        <div className="range-slider noUi-target noUi-ltr noUi-horizontal noUi-background" id="range-slider-7872">
                          <div className="noUi-base">
                            <div className="noUi-origin noUi-connect" data-style="left" style={{left: '0%'}}><div className="noUi-handle noUi-handle-lower" /></div>
                            <div className="noUi-origin noUi-background" data-style="left" style={{left: '100%'}}><div className="noUi-handle noUi-handle-upper" /></div>
                          </div>
                        </div>
                      </div>
                      <div className="filter-action">
                        <button className="clear-filter-button" id="clear-button" onclick="window.ST.clearNumberRangeFilter(`#range-slider-7872`)" type="submit">
                          Effacer
                        </button>
                        <div className="apply-cancel-btn">
                          <button className="cancel-filter-button" onclick="window.ST.cancelNumberRangeFilter(`#range-slider-7872`)" type="button">
                            Annuler
                          </button>
                          <button className="apply-filter-button" type="submit">
                            Appliquer
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="toggle-container home-toolbar-toggle-container price-filter">
                    <div className="toggle with-borders" data-toggle="#home-toolbar-4-menu">
                      <div className="toggle-header-container">
                        <div className="toggle-header">
                          Toilettes
                        </div>
                      </div>
                    </div>
                    <div className="toggle-menu hidden" id="home-toolbar-4-menu">
                      <div className="custom-filter-options">
                        <div className="num-range">
                          <span className="num-range-text">
                            <span>
                              Range
                            </span>
                          </span>
                          <div className="num-min-max">
                            <input className="min-num-limit" id="nf_min_range-slider-7873" max={10.0} min={0.0} name="nf_min_7873" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,10.0],[&quot;#nf_min_range-slider-7873&quot;,&quot;#nf_max_range-slider-7873&quot;],'#nf_min_range-slider-7873','#range-slider-7873')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={0} step={1} type="number" />
                            <span className="num-seperator">
                              -
                            </span>
                            <input className="max-num-limit" id="nf_max_range-slider-7873" max={10.0} min={0.0} name="nf_max_7873" oninput="window.ST.numberFilters.onInputHandler(event,[0.0,10.0],[&quot;#nf_min_range-slider-7873&quot;,&quot;#nf_max_range-slider-7873&quot;],'#nf_max_range-slider-7873','#range-slider-7873')" onkeydown="window.ST.numberFilters.onKeyDownHandler(event)" placeholder={10} step={1} type="number" />
                          </div>
                        </div>
                        <div className="range-slider noUi-target noUi-ltr noUi-horizontal noUi-background" id="range-slider-7873">
                          <div className="noUi-base">
                            <div className="noUi-origin noUi-connect" data-style="left" style={{left: '0%'}}><div className="noUi-handle noUi-handle-lower" /></div>
                            <div className="noUi-origin noUi-background" data-style="left" style={{left: '100%'}}><div className="noUi-handle noUi-handle-upper" /></div>
                          </div>
                        </div>
                      </div>
                      <div className="filter-action">
                        <button className="clear-filter-button" id="clear-button" onclick="window.ST.clearNumberRangeFilter(`#range-slider-7873`)" type="submit">
                          Effacer
                        </button>
                        <div className="apply-cancel-btn">
                          <button className="cancel-filter-button" onclick="window.ST.cancelNumberRangeFilter(`#range-slider-7873`)" type="button">
                            Annuler
                          </button>
                          <button className="apply-filter-button" type="submit">
                            Appliquer
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className>
              <div className="row listing-sections">
                <div className="col-7">
                  <div className="home-listings">
                    <div className="home-list-item flex-theme">
                      <a className="home-list-image-container-desktop" href="/fr/listings/33266-tttccddnj">
                        <img alt="Vente: tttccddnj," className="home-list-image" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/90181/small_3x2/Capture3.PNG?1636721357" />
                      </a>
                      <a className="home-list-image-container-mobile" href="/fr/listings/33266-tttccddnj">
                        <img alt="Vente: tttccddnj," className="home-list-image" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/90181/thumb/Capture3.PNG?1636721357" />
                      </a>
                      <div className="home-list-details-right">
                        <div className="home-list-price">
                          <div className="home-list-item-price-value">
                            Fr 22
                          </div>
                          <div className="home-list-price-quantity" title=" par unité">
                            par unité
                          </div>
                        </div>
                      </div>
                      <div className="home-list-details-with-image">
                        <h2 className="home-list-title">
                          <a href="/fr/listings/33266-tttccddnj">tttccddnj, </a>
                        </h2>
                        <div className="home-list-price-mobile home-list-price-mobile-with-listing-image">
                          <div className="home-list-price-value-mobile">
                            Fr 22
                          </div>
                          <div className="home-list-price-quantity" title=" par unité">
                            par unité
                          </div>
                        </div>
                      </div>
                      <div className="home-list-author home-list-author-with-listing-image">
                        <div className="home-list-avatar">
                          <a className="home-fluid-thumbnail-grid-author-avatar-image" href="/fr/eddy">
                            <img src="https://www.3n-immo.com/assets/profile_image/thumb/missing-2103b8402da301387627aaea4ce27451dedbd0f6aa0cf7850759a0a8b368375e.png" alt="Missing" />
                          </a>
                        </div>
                        <div className="home-list-author-details">
                          <a className="home-list-author-name" href="/fr/eddy">Eddy Harold D </a>
                          <div className="home-list-author-reviews">
                            <div className="card-review-info">
                              <div />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="home-list-item flex-theme">
                      <a className="home-list-image-container-desktop" href="/fr/listings/33233-hhh">
                        <img alt="Location longue durée (non meublée): hhh" className="home-list-image" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/90055/small_3x2/Capture3.PNG?1636554302" />
                      </a>
                      <a className="home-list-image-container-mobile" href="/fr/listings/33233-hhh">
                        <img alt="Location longue durée (non meublée): hhh" className="home-list-image" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/90055/thumb/Capture3.PNG?1636554302" />
                      </a>
                      <div className="home-list-details-right">
                        <div className="home-list-price">
                          <div className="home-list-item-price-value">
                            Fr 5 555
                          </div>
                          <div className="home-list-price-quantity" title=" par mois">
                            par mois
                          </div>
                        </div>
                      </div>
                      <div className="home-list-details-with-image">
                        <h2 className="home-list-title">
                          <a href="/fr/listings/33233-hhh">hhh </a>
                        </h2>
                        <div className="home-list-price-mobile home-list-price-mobile-with-listing-image">
                          <div className="home-list-price-value-mobile">
                            Fr 5 555
                          </div>
                          <div className="home-list-price-quantity" title=" par mois">
                            par mois
                          </div>
                        </div>
                      </div>
                      <div className="home-list-author home-list-author-with-listing-image">
                        <div className="home-list-avatar">
                          <a className="home-fluid-thumbnail-grid-author-avatar-image" href="/fr/eddy">
                            <img src="https://www.3n-immo.com/assets/profile_image/thumb/missing-2103b8402da301387627aaea4ce27451dedbd0f6aa0cf7850759a0a8b368375e.png" alt="Missing" />
                          </a>
                        </div>
                        <div className="home-list-author-details">
                          <a className="home-list-author-name" href="/fr/eddy">Eddy Harold D </a>
                          <div className="home-list-author-reviews">
                            <div className="card-review-info">
                              <div />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="home-list-item flex-theme">
                      <div className="mark-favourite">
                        <label htmlFor="add_to_fav_32447" id="fav_icon_32447">
                          <div className="red-heart">
                            <img className="favourite-icon not_favorite" id="added_to_favourites_32447" src="https://www.3n-immo.com/assets/favourite-cc8b7c471ecf3427d3fb6046b5609400f6017b6006d42af08406f3270b393859.svg" alt="Favourite" />
                          </div>
                          <div className="grey-heart">
                            <img className="favourite-icon not_favorite" id="not_in_favourites_32447" src="https://www.3n-immo.com/assets/not_favourite-081dc589548042799e5849e7da3c06031ef3635ffc4439439bc6cb93bf8ac464.svg" alt="Not favourite" />
                          </div>
                        </label>
                        <input id="add_to_fav_32447" name="add_to_fav_32447" onchange="ST.fav_listings.updateFavouriteStatus(32447, 'HG7MNsSmX92YTDQkPhx2fQ')" style={{display: 'none'}} type="checkbox" />
                      </div>
                      <a className="home-list-image-container-desktop" href="/fr/listings/32447-appartements-meubles">
                        <img alt="Location court/moyenne durée (meublée): Appartements meublés" className="home-list-image" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/87617/small_3x2/flyer_offre_bonamoussadi.jpg?1631265723" />
                      </a>
                      <a className="home-list-image-container-mobile" href="/fr/listings/32447-appartements-meubles">
                        <img alt="Location court/moyenne durée (meublée): Appartements meublés" className="home-list-image" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/87617/thumb/flyer_offre_bonamoussadi.jpg?1631265723" />
                      </a>
                      <div className="home-list-details-right">
                        <div className="home-list-price">
                          <div className="home-list-item-price-value">
                            $ 73
                          </div>
                          <div className="home-list-price-quantity" title=" par jour">
                            par jour
                          </div>
                        </div>
                      </div>
                      <div className="home-list-details-with-image">
                        <h2 className="home-list-title">
                          <a href="/fr/listings/32447-appartements-meubles">Appartements meublés </a>
                        </h2>
                        <div className="home-list-price-mobile home-list-price-mobile-with-listing-image">
                          <div className="home-list-price-value-mobile">
                            $ 73
                          </div>
                          <div className="home-list-price-quantity" title=" par jour">
                            par jour
                          </div>
                        </div>
                      </div>
                      <div className="home-list-author home-list-author-with-listing-image">
                        <div className="home-list-avatar">
                          <a className="home-fluid-thumbnail-grid-author-avatar-image" href="/fr/renee_noguem08">
                            <img src="https://yelodotred.s3-us-west-2.amazonaws.com/images/people/images/9O6WoNER6OSqeuFZE7c3wA/thumb/0ADD4DCE-1A0A-4576-A538-5FD5677F5081.jpeg?1622131137" alt="0add4dce 1a0a 4576 a538 5fd5677f5081" />
                          </a>
                        </div>
                        <div className="home-list-author-details">
                          <a className="home-list-author-name" href="/fr/renee_noguem08">Renée N </a>
                          <div className="home-list-author-reviews">
                            <div className="card-review-info">
                              <div />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="home-list-item flex-theme">
                      <div className="mark-favourite">
                        <label htmlFor="add_to_fav_27026" id="fav_icon_27026">
                          <div className="red-heart">
                            <img className="favourite-icon in_favorite" id="added_to_favourites_27026" src="https://www.3n-immo.com/assets/favourite-cc8b7c471ecf3427d3fb6046b5609400f6017b6006d42af08406f3270b393859.svg" alt="Favourite" />
                          </div>
                          <div className="grey-heart">
                            <img className="favourite-icon in_favorite" id="not_in_favourites_27026" src="https://www.3n-immo.com/assets/not_favourite-081dc589548042799e5849e7da3c06031ef3635ffc4439439bc6cb93bf8ac464.svg" alt="Not favourite" />
                          </div>
                        </label>
                        <input defaultChecked="checked" id="add_to_fav_27026" name="add_to_fav_27026" onchange="ST.fav_listings.updateFavouriteStatus(27026, 'HG7MNsSmX92YTDQkPhx2fQ')" style={{display: 'none'}} type="checkbox" />
                      </div>
                      <a className="home-list-image-container-desktop" href="/fr/listings/27026-appart-meuble-3-chambres-a-louer">
                        <img alt="Location court/moyenne durée (meublée): Appart meublé 3 chambres à louer" className="home-list-image" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71399/small_3x2/2020122_1_193856.jpg?1616433402" />
                      </a>
                      <a className="home-list-image-container-mobile" href="/fr/listings/27026-appart-meuble-3-chambres-a-louer">
                        <img alt="Location court/moyenne durée (meublée): Appart meublé 3 chambres à louer" className="home-list-image" src="https://yelodotred.s3-us-west-2.amazonaws.com/images/listing_images/images/71399/thumb/2020122_1_193856.jpg?1616433402" />
                      </a>
                      <div className="home-list-details-right">
                        <div className="home-list-price">
                          <div className="home-list-item-price-value">
                            $ 30
                          </div>
                          <div className="home-list-price-quantity" title=" par jour">
                            par jour
                          </div>
                        </div>
                      </div>
                      <div className="home-list-details-with-image">
                        <h2 className="home-list-title">
                          <a href="/fr/listings/27026-appart-meuble-3-chambres-a-louer">Appart meublé 3 chambres à louer </a>
                        </h2>
                        <div className="home-list-price-mobile home-list-price-mobile-with-listing-image">
                          <div className="home-list-price-value-mobile">
                            $ 30
                          </div>
                          <div className="home-list-price-quantity" title=" par jour">
                            par jour
                          </div>
                        </div>
                      </div>
                      <div className="home-list-author home-list-author-with-listing-image">
                        <div className="home-list-avatar">
                          <a className="home-fluid-thumbnail-grid-author-avatar-image" href="/fr/samuel">
                            <img src="https://www.3n-immo.com/assets/profile_image/thumb/missing-2103b8402da301387627aaea4ce27451dedbd0f6aa0cf7850759a0a8b368375e.png" alt="Missing" />
                          </a>
                        </div>
                        <div className="home-list-author-details">
                          <a className="home-list-author-name" href="/fr/samuel">SME Digital </a>
                          <div className="home-list-author-reviews">
                            <div className="card-review-info">
                              <div />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="pageless-loader" style={{display: 'none', textAlign: 'center', width: '100%'}}>
                      <div className="msg" style={{color: '#e9e9e9', fontSize: '2em'}}>Chargement de plus de contenu</div>
                      <img src="https://s3.amazonaws.com/sharetribe/assets/ajax-loader-grey.gif" alt="loading more results" style={{margin: '10px auto'}} />
                    </div>
                  </div>
                  <div className="home-loading-more" />
                </div>
                <div className="col-5">
                  <div className="home-map">
                    <div className="map leaflet-container leaflet-touch leaflet-fade-anim leaflet-grab leaflet-touch-drag leaflet-touch-zoom" id="map" tabIndex={0} style={{position: 'relative'}}>
                      <div className="leaflet-pane leaflet-map-pane" style={{transform: 'translate3d(0px, 0px, 0px)'}}>
                        <div className="leaflet-pane leaflet-tile-pane">
                          <div className="leaflet-gl-layer mapboxgl-map" style={{width: '574.8px', height: '732px', transform: 'translate3d(-47.9px, -61px, 0px)'}}>
                            <div className="mapboxgl-canary" style={{visibility: 'hidden'}} />
                            <div className="mapboxgl-canvas-container">
                              <canvas className="mapboxgl-canvas leaflet-image-layer leaflet-zoom-animated" tabIndex={0} aria-label="Map" width={516} height={658} style={{position: 'absolute', width: '574px', height: '732px', transform: 'translate3d(0px, 0px, 0px) scale(1)'}} />
                            </div>
                            <div className="mapboxgl-control-container">
                              <div className="mapboxgl-ctrl-top-left" />
                              <div className="mapboxgl-ctrl-top-right" />
                              <div className="mapboxgl-ctrl-bottom-left">
                                <div className="mapboxgl-ctrl" style={{display: 'none'}}><a className="mapboxgl-ctrl-logo" target="_blank" rel="noopener nofollow" href="https://www.mapbox.com/" aria-label="Mapbox logo" /></div>
                              </div>
                              <div className="mapboxgl-ctrl-bottom-right" />
                            </div>
                          </div>
                          <div className="leaflet-layer" style={{zIndex: 1, opacity: 1}}>
                            <div className="leaflet-tile-container leaflet-zoom-animated" style={{zIndex: 18, transform: 'translate3d(0px, 0px, 0px) scale(1)'}}>
                              <img alt="" role="presentation" src="http://b.tile.osm.org/12/2158/2001.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(9px, 105px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://a.tile.osm.org/12/2158/2000.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(9px, -151px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://a.tile.osm.org/12/2157/2001.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(-247px, 105px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://c.tile.osm.org/12/2159/2001.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(265px, 105px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://c.tile.osm.org/12/2158/2002.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(9px, 361px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://c.tile.osm.org/12/2157/2000.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(-247px, -151px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://b.tile.osm.org/12/2159/2000.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(265px, -151px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://b.tile.osm.org/12/2157/2002.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(-247px, 361px, 0px)', opacity: 1}} />
                              <img alt="" role="presentation" src="http://a.tile.osm.org/12/2159/2002.png" className="leaflet-tile leaflet-tile-loaded" style={{width: '256px', height: '256px', transform: 'translate3d(265px, 361px, 0px)', opacity: 1}} />
                            </div>
                          </div>
                        </div>
                        <div className="leaflet-pane leaflet-shadow-pane">
                          <img src="https://unpkg.com/leaflet@1.0.3/dist/images/marker-shadow.png" className="leaflet-marker-shadow leaflet-zoom-animated" alt="" style={{marginLeft: '-12px', marginTop: '-41px', width: '41px', height: '41px', opacity: 1, transform: 'translate3d(104px, 400px, 0px)'}} />
                          <img src="https://unpkg.com/leaflet@1.0.3/dist/images/marker-shadow.png" className="leaflet-marker-shadow leaflet-zoom-animated" alt="" style={{marginLeft: '-12px', marginTop: '-41px', width: '41px', height: '41px', opacity: 1, transform: 'translate3d(216px, 210px, 0px)'}} />
                        </div>
                        <div className="leaflet-pane leaflet-overlay-pane" />
                        <div className="leaflet-pane leaflet-marker-pane">
                          <img src="https://unpkg.com/leaflet@1.0.3/dist/images/marker-icon.png" className="leaflet-marker-icon leaflet-zoom-animated leaflet-interactive" title={27026} tabIndex={0} style={{marginLeft: '-12px', marginTop: '-41px', width: '25px', height: '41px', opacity: 1, transform: 'translate3d(104px, 400px, 0px)', zIndex: 400}} />
                          <img src="https://unpkg.com/leaflet@1.0.3/dist/images/marker-icon.png" className="leaflet-marker-icon leaflet-zoom-animated leaflet-interactive" title={32447} tabIndex={0} style={{marginLeft: '-12px', marginTop: '-41px', width: '25px', height: '41px', opacity: 1, transform: 'translate3d(216px, 210px, 0px)', zIndex: 210}} />
                          <div className="leaflet-marker-icon marker-cluster marker-cluster-small leaflet-zoom-animated leaflet-interactive" tabIndex={0} style={{marginLeft: '-20px', marginTop: '-20px', width: '40px', height: '40px', transform: 'translate3d(375px, 246px, 0px)', zIndex: 246, opacity: 1}}>
                            <div><span>2</span></div>
                          </div>
                        </div>
                        <div className="leaflet-pane leaflet-tooltip-pane" />
                        <div className="leaflet-pane leaflet-popup-pane" />
                        <div className="leaflet-proxy leaflet-zoom-animated" style={{transform: 'translate3d(552678px, 512456px, 0px) scale(2048)'}} />
                      </div>
                      <div className="leaflet-control-container">
                        <div className="leaflet-top leaflet-left">
                          <div className="leaflet-control-zoom leaflet-bar leaflet-control">
                            <a className="leaflet-control-zoom-in" href="#" title="Zoom in" role="button" aria-label="Zoom in">+</a>
                            <a className="leaflet-control-zoom-out" href="#" title="Zoom out" role="button" aria-label="Zoom out">-</a>
                          </div>
                        </div>
                        <div className="leaflet-top leaflet-right" />
                        <div className="leaflet-bottom leaflet-left" />
                        <div className="leaflet-bottom leaflet-right">
                          <div className="leaflet-control-attribution leaflet-control">
                            <a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a> | © <a href="http://osm.org/copyright">OpenStreetMap</a> contributors
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </form>

    )
}
export default Appartement