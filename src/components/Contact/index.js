import React from 'react';
import '../../componentsCss/Contact.css';

const Contact = () =>{

    return(

        <article className="page-content" style={{}}>
        <div className="wrapper">
          <div className="new-feedback-form centered-section">
            <form className="new_feedback" id="new_feedback" action="/fr/user_feedbacks" acceptCharset="UTF-8" method="post" noValidate="novalidate">
              <input name="utf8" type="hidden" defaultValue="✓" /><input type="hidden" name="authenticity_token" defaultValue="c5mg/dSGZ2bWLg25DAZ/dg0pXaY6cA2C9XmAKE/yKpOk9llKgTO8YuS2qbyo1U5BaJ8ngk4fxeH26gvNdKDzmQ==" />
              <label htmlFor="feedback_email">Votre adresse email (pour vous joindre)</label>
              <input type="text" name="feedback[email]" id="feedback_email" />
              <label className="unwanted_text_field" htmlFor="feedback_title">You should not see this field, if CSS is working.</label>
              <input className="unwanted_text_field" id="error_feedback_unwanted_title" type="text" name="feedback[title]" />
              <label htmlFor="feedback_content">Votre message pour l'équipe 3N Immo</label>
              <textarea name="feedback[content]" id="feedback_content" defaultValue={""} />
              <div className="recaptcha-error">
                <div className="g-recaptcha" data-callback="googleRecaptchaCallback" data-sitekey="6Lfe6JcaAAAAAIwF1uF38h_Spop7QzqCpe6H5uKy">
                  <div style={{width: '304px', height: '78px'}}>
                    <div>
                      <iframe title="reCAPTCHA" src="https://www.google.com/recaptcha/api2/anchor?ar=1&k=6Lfe6JcaAAAAAIwF1uF38h_Spop7QzqCpe6H5uKy&co=aHR0cHM6Ly93d3cuM24taW1tby5jb206NDQz&hl=fr&v=yZguKF1TiDm6F3yJWVhmOKQ9&size=normal&cb=wb4fa1vmmiaw" width={304} height={78} role="presentation" name="a-75hf1w03rir" frameBorder={0} scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox" />
                    </div>
                    <textarea id="g-recaptcha-response" name="g-recaptcha-response" className="g-recaptcha-response" style={{width: '250px', height: '40px', border: '1px solid rgb(193, 193, 193)', margin: '10px 25px', padding: '0px', resize: 'none', display: 'none'}} defaultValue={""} />
                  </div>
                  <iframe style={{display: 'none'}} />
                </div>
                <span>Please verify that you are not a robot</span>
              </div>
              <input defaultValue="https://www.3n-immo.com/fr/login" type="hidden" name="feedback[url]" id="feedback_url" />
              <button name="button" type="submit">Envoyer votre message</button>
            </form>
          </div>
        </div>
      </article>
    )

    
}
export default Contact