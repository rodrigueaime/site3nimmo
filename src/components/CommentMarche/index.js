import react from 'react';
import '../../componentsCss/Commentmarche.css';


const CommentMarche = () =>{

    return(

        <article className="page-content" style={{}}>
        <div className="wrapper">
          <div className="left-navi">
            <a className="left-navi-link" href="/fr/infos/about" title="A propos">
              <div className="left-navi-links-modern">
                <div className="icon-info-sign left-navi-link-icon" />
                <div className="left-navi-link-text">A propos</div>
              </div>
            </a>
            <a className="selected left-navi-link" href="/fr/infos/how_to_use" title="Comment ça marche">
              <div className="left-navi-links-modern">
                <div className="icon-book left-navi-link-icon" />
                <div className="left-navi-link-text">Comment ça marche</div>
              </div>
            </a>
            <a className="left-navi-link" href="/fr/infos/privacy" title="Politique de Confidentialité">
              <div className="left-navi-links-modern">
                <div className="icon-lock left-navi-link-icon" />
                <div className="left-navi-link-text">Politique de Confidentialité</div>
              </div>
            </a>
            <a className="left-navi-link" href="/fr/infos/terms" title="Conditions d'utilisation">
              <div className="left-navi-links-modern">
                <div className="icon-file-alt left-navi-link-icon" />
                <div className="left-navi-link-text">Conditions d'utilisation</div>
              </div>
            </a>
          </div>
          <div className="left-navi-dropdown toggle with-borders hidden-tablet" data-toggle=".left-navi-menu">
            <div className="icon-book icon-with-text" />
            <div className="text-with-icon">Comment ça marche</div>
            <i className="dropdown-icon" />
          </div>
          <div className="left-navi-menu toggle-menu hidden">
            <div className="menu-text">Menu</div>
            <a href="/fr/infos/about" id="about_left_navi_link" title="A propos">
              <div className="icon-info-sign icon-with-text" />
              <div className="text-with-icon">A propos</div>
            </a>
            <a href="/fr/infos/how_to_use" id="how_to_use_left_navi_link" title="Comment ça marche">
              <div className="icon-book icon-with-text" />
              <div className="text-with-icon">Comment ça marche</div>
            </a>
            <a href="/fr/infos/privacy" id="privacy_left_navi_link" title="Politique de Confidentialité">
              <div className="icon-lock icon-with-text" />
              <div className="text-with-icon">Politique de Confidentialité</div>
            </a>
            <a href="/fr/infos/terms" id="terms_left_navi_link" title="Conditions d'utilisation">
              <div className="icon-file-alt icon-with-text" />
              <div className="text-with-icon">Conditions d'utilisation</div>
            </a>
          </div>
          <div className="left-navi-section about-section">
            <div data-mercury="full" id="how_to_use_page_content">
              <h1>3N Immo<br /></h1><div>est une agence immobilière en ligne.</div><div><br /></div><div>Les biens sont mis à jour en permanence et retirés lorsqu'ils ne sont plus disponibles</div>
            </div>
          </div>
        </div>
      </article>

    )
}
export default CommentMarche